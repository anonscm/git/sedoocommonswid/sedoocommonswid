package fr.sedoo.commons.client.component;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.resources.client.CssResource;

import fr.sedoo.commons.client.image.CommonBundle;

public class DefaultCssResourceProvider implements CssResourceProvider {

	@Override
	public List<CssResource> getCssResources() {
		List<CssResource> resources = new ArrayList<CssResource>();
		resources.add(CommonBundle.INSTANCE.css());
		return resources;
	}

}
