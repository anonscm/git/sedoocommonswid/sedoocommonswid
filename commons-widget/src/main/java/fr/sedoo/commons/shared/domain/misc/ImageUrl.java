package fr.sedoo.commons.shared.domain.misc;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.user.client.rpc.IsSerializable;

import fr.sedoo.commons.client.widget.editing.ValidationAlert;
import fr.sedoo.commons.shared.domain.HasIdentifier;
import fr.sedoo.commons.shared.domain.IsUrl;

public class ImageUrl implements HasIdentifier, IsUrl, IsSerializable {

	private String link;
	private String label;
	private Long id;

	public void setLink(String link) {
		this.link = link;
	}

	@Override
	public String getLink() {
		return link;
	}

	@Override
	public String getLabel() {
		return label;
	}

	@Override
	public String getIdentifier() {
		return "" + getId();
	}

	@Override
	public List<ValidationAlert> validate() {
		return new ArrayList<ValidationAlert>();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
