package fr.sedoo.commons.client.news.widget;

import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.core.client.GWT;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;

import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.news.bundle.NewsMessages;
import fr.sedoo.commons.client.news.ui.MessageEditViewImpl;
import fr.sedoo.commons.client.style.SedooStyleBundle;
import fr.sedoo.commons.client.util.DateUtil;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.shared.domain.Message;
import fr.sedoo.commons.shared.domain.message.TitledMessage;

public class DefaultTitledMessageRenderer implements TitledMessageRenderer {

	private static int DEFAULT_LENGTH = 200;

	public void render(Context context, final TitledMessage message, final SafeHtmlBuilder sb, final String language) {

		render(message, DEFAULT_LENGTH, sb, language);
	}

	public void render(TitledMessage message, int length, SafeHtmlBuilder sb, String language) {
		if (message == null) {
			return;
		}
		sb.appendHtmlConstant("<div class=\"" + SedooStyleBundle.INSTANCE.sedooStyle().newsBlock() + "\">");
		sb.appendHtmlConstant("<a>");

		if (message.getImageType().compareToIgnoreCase(Message.NO_IMAGE) != 0) {
			String url = StringUtil.trimToEmpty(message.getUrl());
			if (message.getImageType().compareToIgnoreCase(Message.INTERNAL_IMAGE) == 0) {
				url = GWT.getModuleBaseURL() + MessageEditViewImpl.getStoredFileServicePath() + "ILLUSTRATION_OF_" + message.getUuid();
			}
			sb.appendHtmlConstant("<div class=\"" + SedooStyleBundle.INSTANCE.sedooStyle().newsImageContainer() + "\">" + "<img src=\"" + url + "\" class=\""
					+ SedooStyleBundle.INSTANCE.sedooStyle().newsImage() + "\"/>" + "</div>");
		}
		sb.appendHtmlConstant("<div class=\"" + SedooStyleBundle.INSTANCE.sedooStyle().newsTitle() + "\">" + StringUtil.trimToEmpty(message.getTitle()) + "</div>");
		sb.appendHtmlConstant("<div class=\"" + SedooStyleBundle.INSTANCE.sedooStyle().newsAddedOrModified() + "\">" + NewsMessages.INSTANCE.addedOrModified() + " "
				+ DateUtil.getDateByLocale(message.getDate(), language) + " " + CommonMessages.INSTANCE.by() + " " + StringUtil.trimToEmpty(message.getAuthor()) + "</div>");

		String aux = StringUtil.trimToEmpty(message.getContent());

		if (aux.length() > length) {
			aux = aux.substring(0, length) + " [...]";
		}
		sb.appendHtmlConstant("<div class=\"" + SedooStyleBundle.INSTANCE.sedooStyle().newsContent() + "\">" + aux + "</div>");
		sb.appendHtmlConstant("<table width=\"100%\" style=\"border-bottom: 1px solid #D4D4CF;text-align:justify;cursor:default\" class=\"localCellList\">");
		sb.appendHtmlConstant("<tr><td><div style=\"border-bottom: 3px; border-bottom-style:solid; border-bottom-color:transparent;\">");
		sb.appendHtmlConstant("</td></tr>");
		sb.appendHtmlConstant("</table>");
		sb.appendHtmlConstant("</a>");
		sb.appendHtmlConstant("</div>");
	}

}