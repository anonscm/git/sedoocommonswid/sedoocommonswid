package fr.sedoo.commons.client.util;

import java.util.Date;

import com.google.gwt.i18n.client.DateTimeFormat;

public class DateUtil {

	/**
	 * Private constructor to prevent instanciation
	 */
	protected DateUtil() {

	}

	private static DateTimeFormat englishFormat = DateTimeFormat
			.getFormat("MMMM dd yyyy");
	private static DateTimeFormat frenchFormat = DateTimeFormat
			.getFormat("dd MMMM yyyy");

	public static String getDateByLocale(Date date, String locale) {
		if (date == null) {
			return "";
		} else {
			if (locale.compareTo(LocaleUtil.FRENCH) == 0) {
				return frenchFormat.format(date);
			} else {
				return englishFormat.format(date);
			}
		}
	}

	public static boolean after(final Date baseDate, final Date afterDate) {
		final long baseTime = baseDate.getTime() / 1000;
		final long afterTime = afterDate.getTime() / 1000;
		return baseTime < afterTime;
	}

}
