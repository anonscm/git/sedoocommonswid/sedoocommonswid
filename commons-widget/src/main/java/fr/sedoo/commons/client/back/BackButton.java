package fr.sedoo.commons.client.back;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;

import fr.sedoo.commons.client.message.CommonMessages;

public class BackButton extends Button implements ClickHandler{
	
	private BackPresenter presenter;
	
	public BackButton() {
		setText(CommonMessages.INSTANCE.back());
		addClickHandler(this);
	}
	
	public void setPresenter(BackPresenter presenter) {
		this.presenter = presenter;
	}

	@Override
	public void onClick(ClickEvent event) {
		if (presenter != null)
		{
			presenter.back();
		}
		
	}

}
