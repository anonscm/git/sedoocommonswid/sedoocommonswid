package fr.sedoo.commons.client.widget.uploader;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.moxieapps.gwt.uploader.client.Uploader;
import org.moxieapps.gwt.uploader.client.events.FileDialogCompleteEvent;
import org.moxieapps.gwt.uploader.client.events.FileDialogCompleteHandler;
import org.moxieapps.gwt.uploader.client.events.FileDialogStartEvent;
import org.moxieapps.gwt.uploader.client.events.FileDialogStartHandler;
import org.moxieapps.gwt.uploader.client.events.FileQueueErrorEvent;
import org.moxieapps.gwt.uploader.client.events.FileQueueErrorHandler;
import org.moxieapps.gwt.uploader.client.events.FileQueuedEvent;
import org.moxieapps.gwt.uploader.client.events.FileQueuedHandler;
import org.moxieapps.gwt.uploader.client.events.UploadErrorEvent;
import org.moxieapps.gwt.uploader.client.events.UploadErrorHandler;
import org.moxieapps.gwt.uploader.client.events.UploadProgressEvent;
import org.moxieapps.gwt.uploader.client.events.UploadProgressHandler;
import org.moxieapps.gwt.uploader.client.events.UploadSuccessEvent;
import org.moxieapps.gwt.uploader.client.events.UploadSuccessHandler;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.DragLeaveEvent;
import com.google.gwt.event.dom.client.DragLeaveHandler;
import com.google.gwt.event.dom.client.DragOverEvent;
import com.google.gwt.event.dom.client.DragOverHandler;
import com.google.gwt.event.dom.client.DropEvent;
import com.google.gwt.event.dom.client.DropHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.widgetideas.client.ProgressBar;

import fr.sedoo.commons.client.image.CommonBundle;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.news.bundle.NewsMessages;
import fr.sedoo.commons.client.news.style.NewsStyleBundle;
import fr.sedoo.commons.client.util.ElementUtil;

public class FileUploader extends VerticalPanel 
{
	Logger logger = Logger.getLogger("");
	private final static String DISABLED_TEXT_COLOR = "#929292";
	private final static String ACTIVATED_TEXT_COLOR = "#000000";
	private final static String UPLOAD_ID = "UPLOAD_ID";
	private ProgressBar progressBar = new ProgressBar(0.0, 1.0, 0.0, new CancelProgressBarTextFormatter());
	private Image cancelButton = new Image(CommonBundle.INSTANCE.ko());
	private Boolean enabled=true;
	private Label orLabel = new Label(CommonMessages.INSTANCE.or().toLowerCase());
	private Label dropFileLabel = new Label();
	private boolean dropEnabled = true;
	private List<UploadListener> listeners = new ArrayList<UploadListener>();
	private Uploader uploader;

	public FileUploader()
	{
		super();
		CommonBundle.INSTANCE.commonStyle().ensureInjected();
		NewsStyleBundle.INSTANCE.sedooStyle().ensureInjected();
		HorizontalPanel progressBarPanel = new HorizontalPanel();
		progressBarPanel.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
		progressBar.setStyleName(CommonBundle.INSTANCE.commonStyle().rightMargin3px());
		ElementUtil.hide(progressBar);
		ElementUtil.hide(cancelButton);
		progressBarPanel.add(progressBar);
		progressBarPanel.add(cancelButton);

		final VerticalPanel filePanel = new VerticalPanel();
		uploader = new Uploader();
		if (isEnabled()) {
			uploader.setButtonCursor(Uploader.Cursor.HAND);
			uploader.setButtonTextStyle(".buttonText {font-family: Arial, sans-serif; padding:5px;font-size: 12px; line-height:28px;border-radius: 3px; border: 1px solid #ADADAD ;background-color: #F1F1F1;color: "
					+ ACTIVATED_TEXT_COLOR + ";cursor:pointer;}");

		} else {
			uploader.setButtonCursor(Uploader.Cursor.ARROW);
			uploader.setButtonTextStyle(".buttonText {font-family: Arial, sans-serif; padding:5px;font-size: 12px; line-height:28px;border-radius: 3px; border: 1px solid #ADADAD ;background-color: #F1F1F1;color: "
					+ DISABLED_TEXT_COLOR + ";cursor:default;}");
		}

		filePanel.setSpacing(2);
		filePanel.getElement().getStyle().setMarginLeft(30, Unit.PX);
		filePanel.getElement().getStyle().setMarginTop(5, Unit.PX);
		filePanel.setStyleName(CommonBundle.INSTANCE.commonStyle().leftMargin5px());
		filePanel.add(uploader);

		uploader.setButtonText("<span id=\"" + UPLOAD_ID + "\" class=\"buttonText\">" + NewsMessages.INSTANCE.chooseFile() + "</span>")
		.setButtonWidth(133).setButtonHeight(28).setFileSizeLimit("20 GB").setButtonCursor(Uploader.Cursor.HAND).setButtonAction(Uploader.ButtonAction.SELECT_FILES)
		.setFileQueuedHandler(new FileQueuedHandler() {
			public boolean onFileQueued(final FileQueuedEvent fileQueuedEvent) {
				// Create a Progress Bar for this file
				progressBar.setTitle(fileQueuedEvent.getFile().getName());
				progressBar.setHeight("18px");
				progressBar.setWidth("200px");

				// As of an upload has started permanent information is
				// lost
				//				fileName = fileQueuedEvent.getFile().getName();
				//				isTmpFile = true;

				// Add Cancel Button Image
				cancelButton.setStyleName("cancelButton");
				cancelButton.addClickHandler(new ClickHandler() {
					public void onClick(ClickEvent event) {
						uploader.cancelUpload(fileQueuedEvent.getFile().getId(), false);
						// progressBar.setProgress(-1.0d);
						reset();
					}
				});

				ElementUtil.show(progressBar);
				ElementUtil.show(cancelButton);

				return true;
			}
		}).setFileDialogStartHandler(new FileDialogStartHandler() {
			public boolean onFileDialogStartEvent(FileDialogStartEvent fileDialogStartEvent) {
				if (uploader.getStats().getUploadsInProgress() <= 0) {
					ElementUtil.hide(progressBar);
					ElementUtil.hide(cancelButton);
				}
				return true;
			}
		}).setUploadProgressHandler(new UploadProgressHandler() {
			public boolean onUploadProgress(UploadProgressEvent uploadProgressEvent) {
				progressBar.setProgress((double) uploadProgressEvent.getBytesComplete() / uploadProgressEvent.getBytesTotal());
				return true;
			}
		}).setUploadSuccessHandler(new UploadSuccessHandler() {

			@Override
			public boolean onUploadSuccess(UploadSuccessEvent uploadSuccessEvent) {
				ElementUtil.hide(cancelButton);
				ElementUtil.hide(progressBar);
				
				Iterator<UploadListener> iterator = listeners.iterator();
				while (iterator.hasNext()) {
					iterator.next().fileUploaded(new Long(uploadSuccessEvent.getServerData()));;
				}
				return true;
			}
		}).setFileDialogCompleteHandler(new FileDialogCompleteHandler() {
			public boolean onFileDialogComplete(FileDialogCompleteEvent fileDialogCompleteEvent) {
				if (fileDialogCompleteEvent.getTotalFilesInQueue() > 0 && uploader.getStats().getUploadsInProgress() <= 0) {
					progressBar.setProgress(0.0);
					uploader.startUpload();
				}
				return true;
			}
		}).setFileQueueErrorHandler(new FileQueueErrorHandler() {
			public boolean onFileQueueError(FileQueueErrorEvent fileQueueErrorEvent) {
				reset();
				Window.alert("Upload of file " + fileQueueErrorEvent.getFile().getName() + " failed due to [" + fileQueueErrorEvent.getErrorCode().toString() + "]: "
						+ fileQueueErrorEvent.getMessage());
				return true;
			}
		}).setUploadErrorHandler(new UploadErrorHandler() {
			public boolean onUploadError(UploadErrorEvent uploadErrorEvent) {
				reset();
				Window.alert("Upload of file " + uploadErrorEvent.getFile().getName() + " failed due to [" + uploadErrorEvent.getErrorCode().toString() + "]: " + uploadErrorEvent.getMessage());
				return true;
			}
		});

		if (Uploader.isAjaxUploadWithProgressEventsSupported()) {
			filePanel.add(orLabel);
			dropFileLabel = new Label(NewsMessages.INSTANCE.dropFile());
			dropFileLabel.setStyleName(NewsStyleBundle.INSTANCE.sedooStyle().dropFiles());
			dropFileLabel.addDragOverHandler(new DragOverHandler() {
				public void onDragOver(DragOverEvent event) {
					if (!uploader.getButtonDisabled()) {
						dropFileLabel.addStyleName(NewsStyleBundle.INSTANCE.sedooStyle().dropFilesOver());
					}
				}
			});
			dropFileLabel.addDragLeaveHandler(new DragLeaveHandler() {
				public void onDragLeave(DragLeaveEvent event) {
					dropFileLabel.removeStyleName(NewsStyleBundle.INSTANCE.sedooStyle().dropFilesOver());
				}
			});
			dropFileLabel.addDropHandler(new DropHandler() {
				public void onDrop(DropEvent event) {
					if (dropEnabled) {
						dropFileLabel.removeStyleName(NewsStyleBundle.INSTANCE.sedooStyle().dropFilesOver());

						if (uploader.getStats().getUploadsInProgress() <= 0) {
							progressBar.setProgress(-1.0d);
						}

						uploader.addFilesToQueue(Uploader.getDroppedFiles(event.getNativeEvent()));
						event.preventDefault();
					}
				}
			});
			filePanel.add(dropFileLabel);

			filePanel.setSpacing(2);
			filePanel.getElement().getStyle().setMarginLeft(30, Unit.PX);
			filePanel.getElement().getStyle().setMarginTop(5, Unit.PX);
			filePanel.setStyleName(CommonBundle.INSTANCE.commonStyle().leftMargin5px());


			add(progressBarPanel);
			add(filePanel);

		}
	}

	private String getUploaderPath() 
	{
		return "/rest/image/upload/";
	}

	public Boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	protected class CancelProgressBarTextFormatter extends ProgressBar.TextFormatter {
		@Override
		protected String getText(ProgressBar bar, double curProgress) {
			if (curProgress < 0) {
				return "Cancelled";
			}
			return ((int) (100 * bar.getPercent())) + "%";
		}
	}

	private void reset() {
		progressBar.setProgress(0.0);
		ElementUtil.hide(progressBar);
		ElementUtil.hide(cancelButton);
	}
	
	public void addUploadListener(UploadListener listener)
	{
		listeners.add(listener);
	}
	
	public void setUuid(String uuid)
	{
		String hostPageBaseURL = GWT.getHostPageBaseURL();
		logger.log(Level.FINE, "hostPage: "+hostPageBaseURL);
		if (GWT.isProdMode())
		{
			logger.log(Level.FINE, "Prod Mode detected");
			String url=hostPageBaseURL+GWT.getModuleName()+getUploaderPath()+uuid;
			logger.log(Level.FINE, "Upload URL:"+url);
			uploader.setUploadURL(url);
		}
		else
		{
			logger.log(Level.FINE, "Dev Mode detected");
			String url=hostPageBaseURL+GWT.getModuleName()+getUploaderPath()+uuid;
			logger.log(Level.FINE, "Upload URL:"+url);
			uploader.setUploadURL(url);
		}
	}
}
