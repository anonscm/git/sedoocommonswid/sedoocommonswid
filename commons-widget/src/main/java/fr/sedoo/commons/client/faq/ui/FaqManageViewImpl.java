package fr.sedoo.commons.client.faq.ui;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Widget;

import fr.sedoo.commons.client.component.AbstractView;
import fr.sedoo.commons.client.faq.ui.table.FaqTable;
import fr.sedoo.commons.shared.domain.HasIdentifier;
import fr.sedoo.commons.shared.domain.faq.FaqTableItem;

public class FaqManageViewImpl extends AbstractView implements FaqManageView {

	private static FaqManageViewImplUiBinder uiBinder = GWT.create(FaqManageViewImplUiBinder.class);

	interface FaqManageViewImplUiBinder extends UiBinder<Widget, FaqManageViewImpl> {
	}

	@UiField
	FaqTable faqTable;

	@Override
	public void setPresenter(Presenter presenter) {
		faqTable.setPresenter(presenter);
	}

	public FaqManageViewImpl() {
		super();
		initWidget(uiBinder.createAndBindUi(this));
		applyCommonStyle();
		faqTable.setAddButtonEnabled(true);
		faqTable.init(new ArrayList<HasIdentifier>());
	}

	@Override
	public void setQuestions(ArrayList<FaqTableItem> questions) {
		faqTable.init(questions);
	}

	@Override
	public void broadcastDeletion(String uuid) {
		faqTable.broadcastDeletion(uuid);

	}

	@Override
	public void broadcastSwap(String firstOrder, String secondOrder) {
		faqTable.swapOrder(firstOrder, secondOrder);
	}

}
