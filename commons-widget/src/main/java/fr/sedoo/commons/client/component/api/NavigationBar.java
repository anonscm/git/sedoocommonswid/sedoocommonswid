package fr.sedoo.commons.client.component.api;

import fr.sedoo.commons.client.event.ActivityStartEventHandler;
import fr.sedoo.commons.client.event.BackEventHandler;
import fr.sedoo.commons.client.event.BreadCrumbChangeEventHandler;
import fr.sedoo.commons.client.widget.PreferredHeightWidget;

public interface NavigationBar extends PreferredHeightWidget, ActivityStartEventHandler, BreadCrumbChangeEventHandler, BackEventHandler
{
}