package fr.sedoo.commons.client.widget.tooltip;

import org.gwtbootstrap3.client.ui.Popover;
import org.gwtbootstrap3.client.ui.constants.Placement;

import com.google.gwt.uibinder.client.UiConstructor;
import com.google.gwt.user.client.ui.Image;

import fr.sedoo.commons.client.image.CommonBundle;

public class HelpTooltip extends Popover {
	@UiConstructor
	public HelpTooltip(String text) {
		super();
		Image helpImage = new Image(CommonBundle.INSTANCE.help());
		add(helpImage);
		setTitle("");
		setContent(text);
	}

	public HelpTooltip(String text, Placement placement) {
		this(text);
		setPlacement(placement);
	}

	public void setText(String text) {
		setContent(text);
	}
}
