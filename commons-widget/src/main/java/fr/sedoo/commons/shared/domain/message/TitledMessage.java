package fr.sedoo.commons.shared.domain.message;

import java.io.Serializable;
import java.util.Date;

import fr.sedoo.commons.shared.domain.New;

public class TitledMessage implements Serializable, New {

	private String title;
	private String content;
	private Date date;
	private String author;
	private String url;
	private String imageType;
	private String uuid;

	public TitledMessage() {
	}

	public TitledMessage(String title, String content, String author, Date date, String url, String imageType, String uuid) {
		setTitle(title);
		setContent(content);
		setAuthor(author);
		setDate(date);
		setUrl(url);
		setImageType(imageType);
		setUuid(uuid);
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getImageType() {
		return imageType;
	}

	public void setImageType(String imageType) {
		this.imageType = imageType;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

}
