package fr.sedoo.commons.client.widget.panel;

import java.util.Iterator;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiConstructor;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

public class HorizontalTitledPanel extends Composite implements HasWidgets  {

	private static HorizontalCollapsingPanelUiBinder uiBinder = GWT
			.create(HorizontalCollapsingPanelUiBinder.class);

	interface HorizontalCollapsingPanelUiBinder extends
	UiBinder<Widget, HorizontalTitledPanel> {
	}

	@UiConstructor
	public HorizontalTitledPanel(String title) {
		initWidget(uiBinder.createAndBindUi(this));
		setTitle(title);
	}

	@UiField
	Label title;

	@UiField
	VerticalPanel content;

	public void setTitle(String title) {
		this.title.setText(title);
	}

	

	@Override
	public void add(Widget widget) {
		content.add(widget);
	}

	@Override
	public void clear() {
		content.clear();
	}

	@Override
	public Iterator<Widget> iterator() {
		return content.iterator();
	}

	@Override
	public boolean remove(Widget w) {
		return content.remove(w);
	}

	
}
