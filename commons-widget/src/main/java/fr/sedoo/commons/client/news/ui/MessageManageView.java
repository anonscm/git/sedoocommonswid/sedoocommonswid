package fr.sedoo.commons.client.news.ui;

import java.util.ArrayList;

import com.google.gwt.user.client.ui.IsWidget;

import fr.sedoo.commons.client.mvp.BasicClientFactory;
import fr.sedoo.commons.shared.domain.message.MessageTableItem;

public interface MessageManageView extends IsWidget {

	void setMessages(ArrayList<MessageTableItem> messages);
	void setPresenter(Presenter presenter);
	void setTitleVisible(boolean value);
	void broadcastDeletion(String uuid);

	public interface Presenter 
	{
		void edit(String uuid);
		void create();
		void delete(String uuid);
		BasicClientFactory getClientFactory();
	}


}
