package fr.sedoo.commons.server.news.dao;

import java.util.ArrayList;
import java.util.List;

import fr.sedoo.commons.shared.domain.Message;
import fr.sedoo.commons.shared.domain.New;
import fr.sedoo.commons.shared.domain.message.MessageDTO;
import fr.sedoo.commons.shared.domain.message.TitledMessage;

public interface MessageDAO {

	public final static String BEAN_NAME = "messageDAO";

	ArrayList<Message> findAll();

	Message save(Message message);

	void deleteMessageByUuid(String uuid);

	TitledMessage getConsultMessageContentByUuid(String uuid, String preferredLanguage, List<String> alternateLanguages);

	MessageDTO findMessageContentByUuid(String uuid);

	ArrayList<String> getUuids();

	ArrayList<String> getCarouselNewsUuid(Integer carouselLength);
	
	ArrayList<String> getOtherNewsUuid(Integer carouselLength, Integer otherNewsLength);

	ArrayList<String> getRssNewsUuid(Integer rssLength);

}
