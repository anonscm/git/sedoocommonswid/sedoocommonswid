package fr.sedoo.commons.client.news.widget;

import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.cell.client.ValueUpdater;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.EventTarget;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;

import fr.sedoo.commons.client.DefaultClientApplication;
import fr.sedoo.commons.client.util.LocaleUtil;
import fr.sedoo.commons.shared.domain.New;
import fr.sedoo.commons.shared.domain.message.TitledMessage;

public class NewsCell extends AbstractCell<New> {

	private NewsListPresenter presenter;
	TitledMessageRenderer titledMessageRenderer;

	public NewsCell() {
		// On ecoute seulement les click éventuels sur les boutons
		super("click");
		titledMessageRenderer = GWT.create(TitledMessageRenderer.class);
		if (titledMessageRenderer == null) {
			titledMessageRenderer = new DefaultTitledMessageRenderer();
		}

	}

	@Override
	public void render(Context context, New value, SafeHtmlBuilder sb) {
		if (value == null) {
			return;
		} else if (value instanceof TitledMessage) {
			titledMessageRenderer.render(context, (TitledMessage) value, sb, LocaleUtil.getClientLocaleLanguage(DefaultClientApplication.getClientFactory().getDefaultLanguage()));
			return;
		} else {
			return;
		}
	}

	@Override
	public void onBrowserEvent(com.google.gwt.cell.client.Cell.Context context, Element parent, New value, NativeEvent event, ValueUpdater<New> valueUpdater) {
		super.onBrowserEvent(context, parent, value, event, valueUpdater);
		EventTarget eventTarget = event.getEventTarget();
		if (Element.is(eventTarget)) {
			Element src = Element.as(eventTarget);
			presenter.displayNew(value.getUuid());
		}
	}

	public void setPresenter(NewsListPresenter presenter) {
		this.presenter = presenter;
	}

}
