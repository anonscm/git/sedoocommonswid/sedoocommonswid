package fr.sedoo.commons.server.faq.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import fr.sedoo.commons.shared.domain.Faq;
import fr.sedoo.commons.shared.domain.faq.QuestionAndAnswer;

public interface FaqDAO {

	public final static String BEAN_NAME = "faqDAO";

	ArrayList<Faq> findAll();

	Faq save(Faq screen);

	void deleteFaqByUuid(String uuid);

	QuestionAndAnswer getConsultFaqContentByUuid(String uuid, String preferredLanguage, List<String> alternateLanguages);

	HashMap<String, QuestionAndAnswer> findFaqContentByUuid(String uuid);

	ArrayList<String> getUuids();

	void swap(String firstId, String secondId);

}
