package fr.sedoo.commons.client.component;

import java.util.List;

import com.google.gwt.resources.client.CssResource;

public interface CssResourceProvider {
	List<CssResource> getCssResources();
}
