package fr.sedoo.commons.client.logfile.service;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;

import fr.sedoo.commons.shared.domain.LogFile;

public interface LogFilesServiceAsync {

	void getLogFiles(AsyncCallback<ArrayList<LogFile>> callback);

}
