package fr.sedoo.commons.client.widget.uploader;

public interface UploadListener 
{
	void fileUploaded(Long id);
}
