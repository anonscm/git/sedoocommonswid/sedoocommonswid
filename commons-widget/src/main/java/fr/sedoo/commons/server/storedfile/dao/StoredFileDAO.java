package fr.sedoo.commons.server.storedfile.dao;

import java.util.ArrayList;

import fr.sedoo.commons.shared.domain.StoredFile;

public interface StoredFileDAO {

	public final static String BEAN_NAME = "storedFileDAO";

	StoredFile save(StoredFile storedFile);

	void deleteStoredFileByAttacherUuid(String attacherUuid);

	void deleteStoredFileById(Long id);

	StoredFile findByAttacherUuid(String attacherUuid);

	StoredFile findById(Long id);

	ArrayList<StoredFile> findFilesByAttacherUuid(String attacherUuid, boolean full);

}
