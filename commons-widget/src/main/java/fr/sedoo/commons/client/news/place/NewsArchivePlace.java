package fr.sedoo.commons.client.news.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

public class NewsArchivePlace extends Place
{
	public static NewsArchivePlace instance;
	
	public NewsArchivePlace()
	{
	}
	
	public static class Tokenizer implements PlaceTokenizer<NewsArchivePlace>
	{
		
		public NewsArchivePlace getPlace(String token) {
			if (instance == null)
			{
				instance = new NewsArchivePlace();
			}
			return instance;
		}

		public String getToken(NewsArchivePlace place) {
			return "";
		}
	}
	
}