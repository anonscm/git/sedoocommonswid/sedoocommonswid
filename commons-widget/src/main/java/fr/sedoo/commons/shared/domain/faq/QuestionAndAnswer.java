package fr.sedoo.commons.shared.domain.faq;

import java.io.Serializable;

public class QuestionAndAnswer implements Serializable {

	private String question;
	private String answer;
	private Integer order;

	public QuestionAndAnswer() {
	}

	public QuestionAndAnswer(String question, String answer, Integer order) {
		this.question = question;
		this.answer = answer;
		this.order = order;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public Integer getOrder() {
		return order;
	}

	public void setOrder(Integer order) {
		this.order = order;
	}

}
