package fr.sedoo.commons.client.faq.activity;

import java.util.ArrayList;
import java.util.Iterator;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

import fr.sedoo.commons.client.event.ActionEventConstants;
import fr.sedoo.commons.client.event.ActionStartEvent;
import fr.sedoo.commons.client.event.ActivityStartEvent;
import fr.sedoo.commons.client.event.NotificationEvent;
import fr.sedoo.commons.client.event.PlaceNavigationEvent;
import fr.sedoo.commons.client.faq.mvp.FaqClientFactory;
import fr.sedoo.commons.client.faq.place.FaqEditPlace;
import fr.sedoo.commons.client.faq.place.FaqManagePlace;
import fr.sedoo.commons.client.faq.service.FaqService;
import fr.sedoo.commons.client.faq.service.FaqServiceAsync;
import fr.sedoo.commons.client.faq.ui.FaqManageView;
import fr.sedoo.commons.client.faq.ui.FaqManageView.Presenter;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.mvp.activity.AdministrationActivity;
import fr.sedoo.commons.client.service.DefaultAbstractCallBack;
import fr.sedoo.commons.client.util.LocaleUtil;
import fr.sedoo.commons.shared.domain.faq.FaqTableItem;

public abstract class FaqManageActivity extends AdministrationActivity implements Presenter {

	private Integer maxOrder = 1;

	private static final FaqServiceAsync FAQ_SERVICE = GWT.create(FaqService.class);

	private FaqManageView faqManageView;

	public FaqManageActivity(FaqManagePlace place, FaqClientFactory clientFactory) {
		super(clientFactory, place);
	}

	/**
	 * Invoked by the ActivityManager to start a new Activity
	 */
	@Override
	public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {
		if (isValidUser() == false) {
			goToLoginPlace();
			return;
		}
		faqManageView = ((FaqClientFactory) clientFactory).getFaqManageView();
		faqManageView.setPresenter(this);
		eventBus.fireEvent(new ActivityStartEvent(this));
		containerWidget.setWidget(faqManageView.asWidget());
		ActionStartEvent e = new ActionStartEvent(CommonMessages.INSTANCE.loading(), ActionEventConstants.BASIC_LOADING_EVENT, true);
		eventBus.fireEvent(e);
		maxOrder = 0;

		FAQ_SERVICE.getQuestions(LocaleUtil.getClientLocaleLanguage(clientFactory.getDefaultLanguage()), clientFactory.getLanguages(),
				new DefaultAbstractCallBack<ArrayList<FaqTableItem>>(e, eventBus) {

					@Override
					public void onSuccess(ArrayList<FaqTableItem> result) {
						super.onSuccess(result);
						Iterator<FaqTableItem> iterator = result.iterator();
						while (iterator.hasNext()) {
							FaqTableItem faqTableItem = iterator.next();
							if (faqTableItem.getOrder() > maxOrder) {
								maxOrder = faqTableItem.getOrder();
							}
						}
						faqManageView.setQuestions(result);
					}

				});

	}

	@Override
	public void edit(String id) {
		clientFactory.getEventBus().fireEvent(new PlaceNavigationEvent(new FaqEditPlace(id)));
	}

	@Override
	public void create() {
		clientFactory.getEventBus().fireEvent(new PlaceNavigationEvent(new FaqEditPlace(maxOrder + 1)));
	}

	@Override
	public void delete(final String uuid) {

		EventBus eventBus = clientFactory.getEventBus();
		ActionStartEvent e = new ActionStartEvent(CommonMessages.INSTANCE.deleting(), ActionEventConstants.BASIC_DELETING_EVENT, true);
		eventBus.fireEvent(e);
		FAQ_SERVICE.deleteFaqByUuid(uuid, new DefaultAbstractCallBack<Void>(e, eventBus) {

			@Override
			public void onSuccess(Void result) {
				super.onSuccess(result);
				faqManageView.broadcastDeletion(uuid);
				clientFactory.getEventBus().fireEvent(new NotificationEvent(CommonMessages.INSTANCE.deletedElement()));
			}
		});
	}

	@Override
	public void swap(final String firstId, final String secondId) {

		EventBus eventBus = clientFactory.getEventBus();
		ActionStartEvent e = new ActionStartEvent(CommonMessages.INSTANCE.operationInProgress(), ActionEventConstants.BASIC_WORKING_EVENT, true);
		eventBus.fireEvent(e);
		FAQ_SERVICE.swap(firstId, secondId, new DefaultAbstractCallBack<Void>(e, eventBus) {

			@Override
			public void onSuccess(Void result) {
				super.onSuccess(result);
				faqManageView.broadcastSwap(firstId, secondId);
				clientFactory.getEventBus().fireEvent(new NotificationEvent(CommonMessages.INSTANCE.operationSuccessful()));
			}
		});
	}

}