package fr.sedoo.commons.client.language.event;

import com.google.gwt.event.shared.GwtEvent;

public class LanguageSwitchEvent extends GwtEvent<LanguageSwitchEventHandler> {

	public static final Type<LanguageSwitchEventHandler> TYPE = new Type<LanguageSwitchEventHandler>();
	private String language;

	public LanguageSwitchEvent(String language) {
		this.setLanguage(language);

	}

	@Override
	protected void dispatch(LanguageSwitchEventHandler handler) {
		handler.onNotification(this);
	}

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<LanguageSwitchEventHandler> getAssociatedType() {
		return TYPE;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

}
