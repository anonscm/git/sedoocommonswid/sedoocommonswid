package fr.sedoo.commons.client.logfile.activity;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

import fr.sedoo.commons.client.back.BackPresenter;
import fr.sedoo.commons.client.back.BackUtils;
import fr.sedoo.commons.client.event.ActionEventConstants;
import fr.sedoo.commons.client.event.ActionStartEvent;
import fr.sedoo.commons.client.event.ActivityStartEvent;
import fr.sedoo.commons.client.logfile.LogClientFactory;
import fr.sedoo.commons.client.logfile.place.LogFileDisplayPlace;
import fr.sedoo.commons.client.logfile.service.LogFilesService;
import fr.sedoo.commons.client.logfile.service.LogFilesServiceAsync;
import fr.sedoo.commons.client.logfile.ui.LogFileDisplayView;
import fr.sedoo.commons.client.logfile.ui.LogFileDisplayView.Presenter;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.mvp.AuthenticatedClientFactory;
import fr.sedoo.commons.client.mvp.activity.AdministrationActivity;
import fr.sedoo.commons.client.service.DefaultAbstractCallBack;
import fr.sedoo.commons.shared.domain.LogFile;

public abstract class LogFileDisplayActivity extends AdministrationActivity implements BackPresenter, Presenter {

	private static final LogFilesServiceAsync LOG_SERVICE = GWT.create(LogFilesService.class);

	public LogFileDisplayActivity(LogFileDisplayPlace place, AuthenticatedClientFactory clientFactory) {
		super(clientFactory, place);
	}

	LogFileDisplayView view;

	/**
	 * Invoked by the ActivityManager to start a new Activity
	 */
	@Override
	public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {
		if (isValidUser() == false) {
			goToLoginPlace();
			return;
		}
		view = ((LogClientFactory) clientFactory).getLogDisplayView();
		view.setBackPresenter(this);
		view.setPresenter(this);
		containerWidget.setWidget(view.asWidget());
		view.reset();
		eventBus.fireEvent(new ActivityStartEvent(this));

		ActionStartEvent e = new ActionStartEvent(CommonMessages.INSTANCE.loading(), ActionEventConstants.BASIC_LOADING_EVENT, true);
		eventBus.fireEvent(e);

		LOG_SERVICE.getLogFiles(new DefaultAbstractCallBack<ArrayList<LogFile>>(e, eventBus) {

			@Override
			public void onSuccess(ArrayList<LogFile> result) {
				super.onSuccess(result);
				view.setLogFiles(result);
			}

		});

	}

	@Override
	public void back() {
		BackUtils.goBack(clientFactory);
	}

	@Override
	public void downloadLog(String id) {
		Window.open(GWT.getModuleBaseURL() + "rest/file/getByKey/" + id, "", "");
	}

}