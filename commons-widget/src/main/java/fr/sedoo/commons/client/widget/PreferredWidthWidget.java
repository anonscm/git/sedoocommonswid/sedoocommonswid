package fr.sedoo.commons.client.widget;

import com.google.gwt.user.client.ui.IsWidget;

public interface PreferredWidthWidget extends IsWidget {

	double getPreferredWidth();
}
