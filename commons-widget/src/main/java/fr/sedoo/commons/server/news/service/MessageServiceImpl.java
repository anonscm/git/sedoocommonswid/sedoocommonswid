package fr.sedoo.commons.server.news.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import fr.sedoo.commons.client.news.service.MessageService;
import fr.sedoo.commons.client.util.ServiceException;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.server.DefaultServerApplication;
import fr.sedoo.commons.server.news.dao.MessageDAO;
import fr.sedoo.commons.server.storedfile.TmpFileManager;
import fr.sedoo.commons.server.storedfile.dao.StoredFileDAO;
import fr.sedoo.commons.shared.domain.Message;
import fr.sedoo.commons.shared.domain.StoredFile;
import fr.sedoo.commons.shared.domain.message.MessageDTO;
import fr.sedoo.commons.shared.domain.message.MessageTableItem;
import fr.sedoo.commons.shared.domain.message.TitledMessage;

public class MessageServiceImpl extends RemoteServiceServlet implements MessageService {

	protected MessageDAO dao;
	protected StoredFileDAO storedFileDAO;
	private final String PREFIX = "ILLUSTRATION_OF_";
	private static int NEWS_CAROUSEL_LENGTH = 3;
	private static int NEWS_PREVIEW_LIST_LENGTH = 5;

	@Override
	public MessageDTO getMessageContentByUuid(String uuid) throws ServiceException {

		initDao();
		MessageDTO result = dao.findMessageContentByUuid(uuid);
		StoredFile storedFile = storedFileDAO.findByAttacherUuid(PREFIX + uuid);
		result.setFileName(storedFile.getName());
		return result;
	}

	@Override
	public TitledMessage getConsultMessageContentByUuid(String uuid, String preferredLanguage, List<String> alternateLanguages) throws ServiceException {
		initDao();
		return dao.getConsultMessageContentByUuid(uuid, preferredLanguage, alternateLanguages);
	}

	protected void initDao() {
		if (dao == null) {
			dao = (MessageDAO) DefaultServerApplication.getSpringBeanFactory().getBeanByName(MessageDAO.BEAN_NAME);
		}
		if (storedFileDAO == null) {
			storedFileDAO = (StoredFileDAO) DefaultServerApplication.getSpringBeanFactory().getBeanByName(StoredFileDAO.BEAN_NAME);
		}
	}

	@Override
	public String saveMessage(String uuid, String author, HashMap<String, TitledMessage> content, String externaImagelUrl, String illustrationImageType, String fileName, boolean isTmpFile)
			throws ServiceException {

		initDao();
		if (uuid.length() == 0) {
			// creation
			uuid = UUID.randomUUID().toString();
			if (illustrationImageType.compareToIgnoreCase(Message.INTERNAL_IMAGE) == 0) {
				// We store the image
				StoredFile storedFile = new StoredFile();
				storedFile.setName(fileName);
				storedFile.setAttacherUuid(PREFIX + uuid);
				try {
					storedFile.setContent(TmpFileManager.getBytesForFile(fileName));
				} catch (Exception e) {
					throw new ServiceException(e.getMessage());
				}
				storedFileDAO.save(storedFile);
				TmpFileManager.removeTmpFile(fileName);
			}
		} else {
			// On supprime toutes les anciennes valeurs
			dao.deleteMessageByUuid(uuid);
			if (illustrationImageType.compareToIgnoreCase(Message.INTERNAL_IMAGE) != 0) {
				// We must delete any existing stored file
				storedFileDAO.deleteStoredFileByAttacherUuid(PREFIX + uuid);
			} else {
				if (isTmpFile) {
					// We must delete any existing stored file
					storedFileDAO.deleteStoredFileByAttacherUuid(PREFIX + uuid);
					StoredFile storedFile = new StoredFile();
					storedFile.setName(fileName);
					storedFile.setAttacherUuid(PREFIX + uuid);
					try {
						storedFile.setContent(TmpFileManager.getBytesForFile(fileName));
					} catch (Exception e) {
						throw new ServiceException(e.getMessage());
					}
					storedFileDAO.save(storedFile);
					TmpFileManager.removeTmpFile(fileName);
				} else {
					// We've got nothing to do the image is unchanged
				}
			}
		}
		Iterator<String> iterator = content.keySet().iterator();
		Date date = new Date();
		while (iterator.hasNext()) {
			String language = iterator.next();
			TitledMessage titledMessage = content.get(language);
			Message aux = new Message();
			aux.setContent(StringUtil.trimToEmpty(titledMessage.getContent()));
			aux.setTitle(StringUtil.trimToEmpty(titledMessage.getTitle()));
			aux.setLanguage(language);
			aux.setAuthor(author);
			aux.setDate(date);
			aux.setUuid(uuid);
			aux.setIllustrationImageType(illustrationImageType);
			aux.setExternaImagelUrl(StringUtil.trimToEmpty(externaImagelUrl));
			dao.save(aux);
		}

		return uuid;
	}

	@Override
	public ArrayList<MessageTableItem> getTitles(String preferredLanguage, List<String> alternateLanguages) throws ServiceException {
		ArrayList<MessageTableItem> result = new ArrayList<MessageTableItem>();
		initDao();

		ArrayList<String> uuids = dao.getUuids();
		for (String uuid : uuids) {
			TitledMessage aux = dao.getConsultMessageContentByUuid(uuid, preferredLanguage, alternateLanguages);
			result.add(new MessageTableItem(uuid, aux.getTitle(), aux.getAuthor(), aux.getDate()));
		}
		return result;
	}

	@Override
	public void deleteMessageByUuid(String uuid) throws ServiceException {
		initDao();
		dao.deleteMessageByUuid(uuid);
		storedFileDAO.deleteStoredFileByAttacherUuid(PREFIX + uuid);
	}

	@Override
	public ArrayList<TitledMessage> getAllTitledMessage(String preferredLanguage, List<String> alternateLanguages) throws ServiceException {
		ArrayList<TitledMessage> result = new ArrayList<TitledMessage>();
		initDao();

		ArrayList<String> uuids = dao.getUuids();
		for (String uuid : uuids) {
			result.add(dao.getConsultMessageContentByUuid(uuid, preferredLanguage, alternateLanguages));
		}
		return result;
	}

	@Override
	public ArrayList<TitledMessage> getTitleMessagesByPage(Integer newNumber, Integer pageSize, String preferredLanguage, List<String> alternateLanguages) throws ServiceException {
		ArrayList<String> uuids = dao.getUuids();
		ArrayList<String> pageUuids = new ArrayList<String>();
		if (uuids.size() >= newNumber) {
			if (uuids.size() >= (newNumber + pageSize)) {
				pageUuids.addAll(uuids.subList(newNumber - 1, newNumber + pageSize - 1));
			} else {
				pageUuids.addAll(uuids.subList(newNumber - 1, uuids.size()));
			}
		}

		ArrayList<TitledMessage> result = new ArrayList<TitledMessage>();

		for (String uuid : pageUuids) {
			result.add(dao.getConsultMessageContentByUuid(uuid, preferredLanguage, alternateLanguages));
		}
		return result;
	}

	@Override
	public Integer getMessageHits() throws ServiceException {
		initDao();
		ArrayList<String> uuids = dao.getUuids();
		if (uuids == null) {
			return new Integer(0);
		}
		return new Integer(uuids.size());
	}

	@Override
	public ArrayList<TitledMessage> getCarouselNews(String preferredLanguage, List<String> alternateLanguages) throws ServiceException {
		initDao();
		ArrayList<String> uuids = dao.getCarouselNewsUuid(NEWS_CAROUSEL_LENGTH);
		ArrayList<TitledMessage> result = new ArrayList<TitledMessage>();
		for (String uuid : uuids) {
			result.add(dao.getConsultMessageContentByUuid(uuid, preferredLanguage, alternateLanguages));
		}
		return result;
	}

	@Override
	public ArrayList<TitledMessage> getLastTitledMessage(String preferredLanguage, List<String> alternateLanguages) throws ServiceException {
		initDao();
		ArrayList<String> uuids = dao.getOtherNewsUuid(NEWS_CAROUSEL_LENGTH, NEWS_PREVIEW_LIST_LENGTH);
		ArrayList<TitledMessage> result = new ArrayList<TitledMessage>();
		for (String uuid : uuids) {
			result.add(dao.getConsultMessageContentByUuid(uuid, preferredLanguage, alternateLanguages));
		}
		return result;
	}

	public ArrayList<TitledMessage> getLastRssMessage(String preferredLanguage, ArrayList<String> alternateLanguages) throws ServiceException {
		initDao();
		ArrayList<String> uuids = dao.getRssNewsUuid(new Integer(40));
		ArrayList<TitledMessage> result = new ArrayList<TitledMessage>();
		for (String uuid : uuids) {
			result.add(dao.getConsultMessageContentByUuid(uuid, preferredLanguage, alternateLanguages));
		}
		return result;
	}

}
