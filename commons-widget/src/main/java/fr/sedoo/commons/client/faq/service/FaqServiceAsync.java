package fr.sedoo.commons.client.faq.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;

import fr.sedoo.commons.shared.domain.faq.FaqTableItem;
import fr.sedoo.commons.shared.domain.faq.QuestionAndAnswer;

public interface FaqServiceAsync {

	void saveFaq(String uuid, HashMap<String, QuestionAndAnswer> content, AsyncCallback<String> callback);

	void getFaqContentByUuid(String uuid, AsyncCallback<HashMap<String, QuestionAndAnswer>> callback);

	void getConsultFaqContentByUuid(String uuid, String preferredLanguage, List<String> alternateLanguages, AsyncCallback<QuestionAndAnswer> callback);

	void getQuestions(String preferredLanguage, List<String> alternateLanguages, AsyncCallback<ArrayList<FaqTableItem>> callback);

	void deleteFaqByUuid(String uuid, AsyncCallback<Void> callback);

	void getAllQuestionsAndAnswers(String preferredLanguage, List<String> alternateLanguages, AsyncCallback<ArrayList<QuestionAndAnswer>> callback);

	void swap(String firstId, String secondId, AsyncCallback<Void> callback);

}
