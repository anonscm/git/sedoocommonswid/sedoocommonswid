package fr.sedoo.commons.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;

import fr.sedoo.commons.client.mvp.CommonClientFactory;

public abstract class DefaultClientApplication implements EntryPoint {

	private static CommonClientFactory clientFactory;

	public void onModuleLoad() {
		clientFactory = getClientFactory();
	}

	public static CommonClientFactory getClientFactory() {
		if (clientFactory == null) {
			clientFactory = GWT.create(CommonClientFactory.class);
		}
		return clientFactory;
	}
}
