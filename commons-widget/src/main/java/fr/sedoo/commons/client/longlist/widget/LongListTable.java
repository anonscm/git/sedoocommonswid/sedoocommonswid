package fr.sedoo.commons.client.longlist.widget;

import java.util.List;

import com.google.gwt.user.cellview.client.SimplePager;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.view.client.AbstractDataProvider;
import com.google.gwt.view.client.AsyncDataProvider;
import com.google.gwt.view.client.HasData;
import com.google.gwt.view.client.Range;

import fr.sedoo.commons.client.longlist.LongListPresenter;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.widget.style.CellTableResources;
import fr.sedoo.commons.client.widget.table.FormattedCellTable;
import fr.sedoo.commons.shared.domain.HasIdentifier;

public abstract class LongListTable extends Composite {

	protected FormattedCellTable<HasIdentifier> table;

	protected VerticalPanel tablePanel;
	protected HorizontalPanel toolBarPanel;

	private HorizontalPanel emptyListPanel;
	
	private LongListPresenter presenter;
	
	private SimplePager pager;
	private List<? extends HasIdentifier> currentDisplayedValues = null;

	public LongListTable() {
		super();
		
		pager = new InternationalizedSimplePager();
		
		if (tablePanel == null)
		{
			tablePanel = new VerticalPanel();
		}
		else
		{
			tablePanel.clear();
		}

		emptyListPanel = new HorizontalPanel();
		emptyListPanel.add(new Label(CommonMessages.INSTANCE.emptyList()));

		checkEmptyPanelVisibility();

		tablePanel.add(emptyListPanel);

		table = new FormattedCellTable<HasIdentifier>(Integer.MAX_VALUE,CellTableResources.INSTANCE);
		table.setWidth("100%", false);
		
		pager.setDisplay(table);
		pager.setPageSize(getPageSize());
		getDataProvider().addDataDisplay(table);

		toolBarPanel = new HorizontalPanel();
		toolBarPanel.setSpacing(5);
		//addItemPanel.setWidth("100%");
		tablePanel.add(pager);
		tablePanel.add(table);
		tablePanel.add(toolBarPanel);
		tablePanel.setWidth("100%");
		initColumns();
		initWidget(tablePanel);


	}

	protected abstract void initColumns() ;
	
	protected abstract int getPageSize();

	private void checkEmptyPanelVisibility() {
		
		if ((currentDisplayedValues == null) || currentDisplayedValues.size() == 0)
		{
			emptyListPanel.setVisible(true);
			if (table != null)
			{
				table.setVisible(false);
			}
		}
		else
		{
			emptyListPanel.setVisible(false);
			if (table != null)
			{
				table.setVisible(true);
			}
		}

	}

	
	protected AbstractDataProvider<HasIdentifier> getDataProvider()
	{
		AsyncDataProvider<HasIdentifier> dataProvider = new AsyncDataProvider<HasIdentifier>()
		{

			@Override
			protected void onRangeChanged(HasData<HasIdentifier> display)
			{

				final Range range = display.getVisibleRange();
				int start = range.getStart();
				if (presenter != null)
				{
					presenter.getEntries(start, getPageSize());
				}
			}
		};

		return dataProvider;
	}
	
	public String getDeleteItemConfirmationText() 
	{
		return CommonMessages.INSTANCE.deletionConfirmMessage();
	}

	public LongListPresenter getPresenter() {
		return presenter;
	}

	public void setPresenter(LongListPresenter presenter) {
		this.presenter = presenter;
	}

	public void setDataPage(int i, List<? extends HasIdentifier> values, Integer hits)
	{
		currentDisplayedValues = values;
		table.setRowData(i, values);
		table.setRowCount(hits);
		checkEmptyPanelVisibility();
		pager.setVisible(values.size() > 0);
	}
}
