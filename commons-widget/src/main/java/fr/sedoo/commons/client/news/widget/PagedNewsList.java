package fr.sedoo.commons.client.news.widget;

import java.util.ArrayList;

import org.gwtbootstrap3.client.ui.Alert;

import com.google.gwt.user.cellview.client.SimplePager;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.view.client.AbstractDataProvider;
import com.google.gwt.view.client.AsyncDataProvider;
import com.google.gwt.view.client.HasData;
import com.google.gwt.view.client.Range;

import fr.sedoo.commons.client.longlist.widget.InternationalizedSimplePager;
import fr.sedoo.commons.client.news.activity.NewsArchiveActivity;
import fr.sedoo.commons.shared.domain.New;
import fr.sedoo.commons.shared.domain.message.TitledMessage;

public class PagedNewsList extends VerticalPanel {

	NewsList list;
	private SimplePager pager;
	Alert emptyList;
	private NewsListPresenter presenter;
	private int pageSize;

	public PagedNewsList(int pageSize) {
		super();
		this.pageSize = pageSize;
		setWidth("100%");
		list = new NewsList();
		pager = new InternationalizedSimplePager();
		pager.setDisplay(list);
		reset();
		getDataProvider().addDataDisplay(list);
		add(pager);
		add(list);
	}

	public void reset() {
		pager.setPageSize(pageSize);
		pager.setPageStart(0);
		list.setRowData(0, new ArrayList<New>());
		list.setRowCount(0);
	}

	public void setHits(int hits) {
		list.setRowCount(hits);
	}

	public void setNewsByRange(int i, ArrayList<TitledMessage> messages) {
		list.setRowData(i, messages);
		// list.setVisibleRangeAndClearData(new Range(0, 25), true);
	}

	protected AbstractDataProvider<New> getDataProvider() {
		AsyncDataProvider<New> dataProvider = new AsyncDataProvider<New>() {

			@Override
			protected void onRangeChanged(HasData<New> display) {

				final Range range = display.getVisibleRange();
				int start = range.getStart();
				if (presenter != null) {
					presenter.loadPageEntries(start + 1);
				}
			}
		};

		return dataProvider;
	}

	public void setPresenter(NewsListPresenter presenter) {
		this.presenter = presenter;
		list.setPresenter(presenter);
	}

}
