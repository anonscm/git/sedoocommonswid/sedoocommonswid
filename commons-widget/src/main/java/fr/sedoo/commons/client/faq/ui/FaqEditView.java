package fr.sedoo.commons.client.faq.ui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.google.gwt.user.client.ui.IsWidget;

import fr.sedoo.commons.client.view.ActivityLifeCycleAwareView;
import fr.sedoo.commons.client.widget.uploader.FileUploadPresenter;
import fr.sedoo.commons.shared.domain.faq.QuestionAndAnswer;
import fr.sedoo.commons.shared.domain.misc.ImageUrl;

public interface FaqEditView extends IsWidget, ActivityLifeCycleAwareView {

	void setContent(HashMap<String, QuestionAndAnswer> questions, List<String> languages, Integer order);

	void reset();

	void setUuid(String uuid);

	void setImages(ArrayList<ImageUrl> images);

	void setPresenter(Presenter presenter);

	void broadcastDeletion(String id);

	public interface Presenter extends FileUploadPresenter {
		void save(HashMap<String, QuestionAndAnswer> result);

		void back();
	}

}
