package fr.sedoo.commons.client.callback;

public interface OperationCallBack<E> {

	public void postExecution(boolean result, E src);

}
