package fr.sedoo.commons.client.image;


import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;


public interface GeographicalBundle extends ClientBundle {

	public static final GeographicalBundle INSTANCE = GWT.create(GeographicalBundle.class);

	@Source("drag.png")
	ImageResource drag();

	@Source("eraseRectangularArea.png")
	ImageResource eraseRectangularArea();

	@Source("drawRectangularArea.png")
	ImageResource drawRectangularArea();
}