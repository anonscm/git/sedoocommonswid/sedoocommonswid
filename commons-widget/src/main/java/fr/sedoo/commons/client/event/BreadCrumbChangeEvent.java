package fr.sedoo.commons.client.event;

import java.util.List;

import com.google.gwt.event.shared.GwtEvent;

import fr.sedoo.commons.client.widget.breadcrumb.Shortcut;

public class BreadCrumbChangeEvent extends GwtEvent<BreadCrumbChangeEventHandler>{

    public static final Type<BreadCrumbChangeEventHandler> TYPE = new Type<BreadCrumbChangeEventHandler>();
    
    private List<Shortcut> shortcuts;

    public BreadCrumbChangeEvent(List<Shortcut> shortcuts)
    {
       this.shortcuts = shortcuts;     
    }

    @Override
    protected void dispatch(BreadCrumbChangeEventHandler handler) {
            handler.onNotification(this);
    }

    @Override
    public com.google.gwt.event.shared.GwtEvent.Type<BreadCrumbChangeEventHandler> getAssociatedType() {
            return TYPE;
    }

	public List<Shortcut> getShortcuts() {
		return shortcuts;
	}


}