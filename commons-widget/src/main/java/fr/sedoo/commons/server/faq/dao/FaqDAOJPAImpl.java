package fr.sedoo.commons.server.faq.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.shared.domain.Faq;
import fr.sedoo.commons.shared.domain.faq.QuestionAndAnswer;

@Repository
public class FaqDAOJPAImpl implements FaqDAO {

	protected EntityManager em;

	public EntityManager getEntityManager() {
		return em;
	}

	@PersistenceContext
	public void setEntityManager(EntityManager entityManager) {
		this.em = entityManager;
	}

	@Override
	@Transactional
	public ArrayList<Faq> findAll() {
		List<Faq> aux = em.createQuery("select faq from Faq faq order by faq.order asc").getResultList();
		ArrayList<Faq> result = new ArrayList<Faq>();
		result.addAll(aux);
		return result;
	}

	@Transactional
	public Faq save(Faq faq) {
		if (faq.getId() == null) {
			getEntityManager().persist(faq);
			return faq;
		} else {
			getEntityManager().merge(faq);
			return faq;
		}
	}

	@Override
	@Transactional
	public QuestionAndAnswer getConsultFaqContentByUuid(String uuid, String preferredLanguage, List<String> alternateLanguages) {
		String query = "select faq from Faq as faq where faq.uuid='" + uuid + "' and faq.language='" + preferredLanguage + "' order by faq.order asc";
		List<Faq> aux = em.createQuery(query).getResultList();
		if ((aux.isEmpty()) || (StringUtil.isEmpty(aux.get(0).getQuestion())) || (StringUtil.trimToEmpty(aux.get(0).getQuestion()).compareToIgnoreCase("<br>") == 0)) {
			if (alternateLanguages.isEmpty() == false) {
				// On va utiliser une nouvelle langue
				String newLanguage = alternateLanguages.get(0);
				alternateLanguages.remove(0);
				return getConsultFaqContentByUuid(uuid, newLanguage, alternateLanguages);
			} else {
				return new QuestionAndAnswer("", "", 0);
			}
		} else {
			Faq faq = aux.get(0);
			return new QuestionAndAnswer(faq.getQuestion(), faq.getAnswer(), faq.getOrder());
		}
	}

	@Override
	@Transactional
	public HashMap<String, QuestionAndAnswer> findFaqContentByUuid(String uuid) {

		HashMap<String, QuestionAndAnswer> result = new HashMap<String, QuestionAndAnswer>();
		String query = "select faq from Faq as faq where faq.uuid='" + uuid + "' order by faq.order asc";
		List<Faq> list = em.createQuery(query).getResultList();
		if (list.isEmpty() == false) {
			for (Faq faq : list) {
				String question = StringUtil.trimToEmpty(faq.getQuestion());
				String answer = StringUtil.trimToEmpty(faq.getAnswer());
				result.put(faq.getLanguage(), new QuestionAndAnswer(question, answer, faq.getOrder()));
			}
		}
		return result;
	}

	@Override
	@Transactional
	public void deleteFaqByUuid(String uuid) {
		List<Faq> list = em.createQuery("select faq from Faq faq where faq.uuid='" + uuid + "' order by faq.order asc").getResultList();
		for (Faq faq : list) {
			em.remove(faq);
		}
	}

	@Override
	@Transactional
	public ArrayList<String> getUuids() {
		List<Object[]> resultList = em.createQuery("select distinct faq.uuid, faq.order from Faq faq order by faq.order asc").getResultList();
		ArrayList<String> result = new ArrayList<String>();
		Iterator<Object[]> iterator = resultList.iterator();
		while (iterator.hasNext()) {
			Object[] object = iterator.next();
			result.add((String) object[0]);
		}
		return result;
	}

	@Override
	@Transactional
	public void swap(String firstId, String secondId) {
		Integer fistOrder = findFaqContentByUuid(firstId).values().iterator().next().getOrder();
		Integer secondOrder = findFaqContentByUuid(secondId).values().iterator().next().getOrder();
		em.createQuery("update Faq faq set faq.order = " + secondOrder + " where faq.uuid='" + firstId + "'").executeUpdate();
		em.createQuery("update Faq faq set faq.order = " + fistOrder + " where faq.uuid='" + secondId + "'").executeUpdate();
	}

}