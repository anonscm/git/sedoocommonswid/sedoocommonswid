package fr.sedoo.commons.client.widget.map.impl;

import fr.sedoo.commons.shared.domain.GeographicBoundingBoxDTO;

public interface RectangularAreaListener {
	void onRectangleAdded(GeographicBoundingBoxDTO box);

	void onRectangleUpdated(GeographicBoundingBoxDTO box);

	void onRectangleDeleted(String uuid);
}
