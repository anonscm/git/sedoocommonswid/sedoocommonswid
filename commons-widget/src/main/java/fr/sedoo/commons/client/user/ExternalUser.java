package fr.sedoo.commons.client.user;

public class ExternalUser extends DefaultAuthenticatedUser{
	
	private String email;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
}
