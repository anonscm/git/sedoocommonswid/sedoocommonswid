package fr.sedoo.commons.client.news.event;

import com.google.gwt.event.shared.EventHandler;

public interface MessageChangeEventHandler extends EventHandler {
    void onNotification(MessageChangeEvent event);
}
