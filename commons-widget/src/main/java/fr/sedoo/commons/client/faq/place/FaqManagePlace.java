package fr.sedoo.commons.client.faq.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

import fr.sedoo.commons.client.mvp.place.AuthenticatedPlace;

public class FaqManagePlace extends Place implements AuthenticatedPlace
{

	public static FaqManagePlace instance;
	

	public static class Tokenizer implements PlaceTokenizer<FaqManagePlace>
	{
		@Override
		public String getToken(FaqManagePlace place)
		{
			return "";
		}

		@Override
		public FaqManagePlace getPlace(String token)
		{
			if (instance == null)
			{
				instance = new FaqManagePlace();
			}
			return instance;
		}
	}

	public FaqManagePlace()
	{
		
	}
	
}
