package fr.sedoo.commons.client.news.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;

import fr.sedoo.commons.shared.domain.message.MessageDTO;
import fr.sedoo.commons.shared.domain.message.MessageTableItem;
import fr.sedoo.commons.shared.domain.message.TitledMessage;

public interface MessageServiceAsync {

	void deleteMessageByUuid(String uuid, AsyncCallback<Void> callback);

	void getAllTitledMessage(String preferredLanguage, List<String> alternateLanguages, AsyncCallback<ArrayList<TitledMessage>> callback);

	void getConsultMessageContentByUuid(String uuid, String preferredLanguage, List<String> alternateLanguages, AsyncCallback<TitledMessage> callback);

	void getTitles(String preferredLanguage, List<String> alternateLanguages, AsyncCallback<ArrayList<MessageTableItem>> callback);

	void saveMessage(String uuid, String author, HashMap<String, TitledMessage> content, String externaImagelUrl, String illustrationImageType, String fileName, boolean isTmpFile,
			AsyncCallback<String> callback);

	void getMessageContentByUuid(String uuid, AsyncCallback<MessageDTO> callback);

	void getLastTitledMessage(String preferredLanguage,
			List<String> alternateLanguages,
			AsyncCallback<ArrayList<TitledMessage>> callback);

	void getMessageHits(AsyncCallback<Integer> callback);

	void getTitleMessagesByPage(Integer pageNumber, Integer pageSize, String preferredLanguage, List<String> alternateLanguages, AsyncCallback<ArrayList<TitledMessage>> callback);

	void getCarouselNews(String preferredLanguage,
			List<String> alternateLanguages,
			AsyncCallback<ArrayList<TitledMessage>> callback);

}
