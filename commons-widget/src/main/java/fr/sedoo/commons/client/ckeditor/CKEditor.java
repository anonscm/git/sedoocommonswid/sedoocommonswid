package fr.sedoo.commons.client.ckeditor;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.user.client.TakesValue;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.TextArea;

import fr.sedoo.commons.client.util.ElementUtil;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.client.util.UuidUtil;
import fr.sedoo.commons.client.widget.api.HtmlTextEditor;

public class CKEditor extends Composite implements TakesValue<String>, HtmlTextEditor {
	TextArea text = new TextArea();
	protected JavaScriptObject editor;
	protected CKConfig config;
	private String styleName;
	private String styleDefinitions;
	private String uuid;
	protected boolean enabledInHostedMode = true;
	protected boolean replaced = false;
	protected boolean textWaitingForAttachment = false;
    protected String waitingText;
	
	public CKEditor() {
		CKEditorJavascriptInjector.injectCKEditorLibrairies();
		initWidget(text);
		uuid = UuidUtil.uuid();
	}

	public CKEditor(CKConfig config) {
		this();
		this.config = config;
	}

	@Override
	protected void onAttach() {
		super.onAttach();
		text.getElement().setId(uuid);
		if (config == null)
		{
			initCKEditor(text.getElement().getId());
			replaced=true;
		}
		else
		{
			initCKEditor(text.getElement().getId(), this.config.getConfigObject());
			replaced=true;
		}
		if (textWaitingForAttachment) {
            textWaitingForAttachment = false;
            setHtml(waitingText);
		}
		if (StringUtil.isNotEmpty(styleName))
		{
			//    	addNativeStylesSet(styleName, JsonUtils.safeEval(styleDefinitions));
		}
	}

	private native void initCKEditor(String id) /*-{
    this.@fr.sedoo.commons.client.ckeditor.CKEditor::editor =  $wnd.CKEDITOR.replace( id );
  }-*/;

	private native void initCKEditor(String id, JavaScriptObject config) /*-{
  this.@fr.sedoo.commons.client.ckeditor.CKEditor::editor =  $wnd.CKEDITOR.replace( id, config );
}-*/;

	public final native void resize(int width, int height)
	/*-{
	 	this.resize(width, height);
	}-*/;

	@Override
	public native void setValue(String value) /*-{
    this.@fr.sedoo.commons.client.ckeditor.CKEditor::editor.setData(value);
  }-*/;

	@Override
	public native String getValue() /*-{
    this.@fr.sedoo.commons.client.ckeditor.CKEditor::editor.setData(value);
  }-*/;

	public native String addNativeStylesSet(String name, JavaScriptObject definitions) /*-{
    this.@fr.sedoo.commons.client.ckeditor.CKEditor::editor.stylesSet.add(name,definitions);
  }-*/;

	public void setStyles(String styleName, String styleDefinitions)
	{
		this.styleName = styleName;
		this.styleDefinitions = styleDefinitions;
		//config.setStyleSet(styleName);
	}

	@Override
	public String getHtml() {
             if (GWT.isScript() || enabledInHostedMode) {
                     if (replaced)
                             return getNativeHTML();
                     else {
                             return waitingText;
                     }
             } else {
                     return text.getText();
             }
	}
	
	 private native String getNativeHTML() /*-{
     var e = this.@fr.sedoo.commons.client.ckeditor.CKEditor::editor;
     return e.getData();
}-*/;

	/**
     * Set the editor's html
     * 
     * @param html
     *  The html string to set
     */
    public void setHtml(String html) {
            if (GWT.isScript() || enabledInHostedMode) {
                    if (replaced)
                            setNativeHTML(html);
                    else {
                            waitingText = html;
                            textWaitingForAttachment = true;
                    }
            } else {
                    text.setText(html);
            }
    }
    
    private native void setNativeHTML(String html) /*-{
    var e = this.@fr.sedoo.commons.client.ckeditor.CKEditor::editor;
    e.setData(html,new Function());
}-*/;

	public void onVisible() {
		ElementUtil.hide(text);
	}

}