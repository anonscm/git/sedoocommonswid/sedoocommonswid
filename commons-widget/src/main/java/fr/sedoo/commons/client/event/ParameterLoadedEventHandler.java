package fr.sedoo.commons.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface ParameterLoadedEventHandler extends EventHandler {
    void onNotification(ParameterLoadedEvent event);
}
