package fr.sedoo.commons.shared.domain;

import java.util.Date;

public interface New {

	Date getDate();

	String getUuid();
}
