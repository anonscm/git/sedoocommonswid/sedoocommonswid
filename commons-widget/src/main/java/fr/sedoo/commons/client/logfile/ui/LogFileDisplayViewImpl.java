package fr.sedoo.commons.client.logfile.ui;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Widget;

import fr.sedoo.commons.client.back.BackButton;
import fr.sedoo.commons.client.back.BackPresenter;
import fr.sedoo.commons.client.component.AbstractView;
import fr.sedoo.commons.client.logfile.ui.table.LogFileTable;
import fr.sedoo.commons.shared.domain.HasIdentifier;
import fr.sedoo.commons.shared.domain.LogFile;

public class LogFileDisplayViewImpl extends AbstractView implements LogFileDisplayView {

	private static LogFileDisplayViewImplUiBinder uiBinder = GWT.create(LogFileDisplayViewImplUiBinder.class);

	interface LogFileDisplayViewImplUiBinder extends UiBinder<Widget, LogFileDisplayViewImpl> {
	}

	@UiField
	LogFileTable logFileTable;

	@UiField
	BackButton backButton;

	public LogFileDisplayViewImpl() {
		super();
		initWidget(uiBinder.createAndBindUi(this));
		applyCommonStyle();
		logFileTable.hideToolBar();
		logFileTable.init(new ArrayList<HasIdentifier>());
		backButton.addStyleName("btn-primary");
	}

	@Override
	public void reset() {
		logFileTable.init(new ArrayList<HasIdentifier>());
	}

	@Override
	public void setBackPresenter(BackPresenter presenter) {
		backButton.setPresenter(presenter);
	}

	@Override
	public void setLogFiles(ArrayList<LogFile> files) {
		logFileTable.init(files);
	}

	@Override
	public void setPresenter(Presenter presenter) {
		logFileTable.setPresenter(presenter);
	}

}
