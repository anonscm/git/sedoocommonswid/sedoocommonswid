package fr.sedoo.commons.client.faq.activity;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

import fr.sedoo.commons.client.event.ActionEventConstants;
import fr.sedoo.commons.client.event.ActionStartEvent;
import fr.sedoo.commons.client.event.ActivityStartEvent;
import fr.sedoo.commons.client.faq.mvp.FaqClientFactory;
import fr.sedoo.commons.client.faq.place.FaqConsultPlace;
import fr.sedoo.commons.client.faq.service.FaqService;
import fr.sedoo.commons.client.faq.service.FaqServiceAsync;
import fr.sedoo.commons.client.faq.ui.FaqConsultView;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.mvp.CommonClientFactory;
import fr.sedoo.commons.client.mvp.activity.DefaultActivity;
import fr.sedoo.commons.client.service.DefaultAbstractCallBack;
import fr.sedoo.commons.client.util.LocaleUtil;
import fr.sedoo.commons.shared.domain.faq.QuestionAndAnswer;

public class FaqConsultActivity extends DefaultActivity {

	private static final FaqServiceAsync FAQ_SERVICE = GWT.create(FaqService.class);

    public FaqConsultActivity(FaqConsultPlace place, CommonClientFactory clientFactory) {
    	super(clientFactory);
    }

    /**
     * Invoked by the ActivityManager to start a new Activity
     */
    @Override
    public void start(final AcceptsOneWidget containerWidget, EventBus eventBus) {
		final FaqConsultView consultView = ((FaqClientFactory) clientFactory).getFaqConsultView();
		consultView.reset();
    	eventBus.fireEvent(new ActivityStartEvent(this));
    		ActionStartEvent e = new ActionStartEvent(CommonMessages.INSTANCE.loading(), ActionEventConstants.BASIC_LOADING_EVENT, true);
        	eventBus.fireEvent(e);
        	
        	FAQ_SERVICE.getAllQuestionsAndAnswers(LocaleUtil.getClientLocaleLanguage(clientFactory.getDefaultLanguage()), clientFactory.getLanguages(), new DefaultAbstractCallBack<ArrayList<QuestionAndAnswer>>(e, eventBus) {
    			@Override
    			public void onSuccess(ArrayList<QuestionAndAnswer> result) {
    				super.onSuccess(result);
    				consultView.setContent(result);
    				containerWidget.setWidget(consultView.asWidget());
    			}
    		});
    }


	
   
}