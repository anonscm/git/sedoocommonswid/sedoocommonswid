package fr.sedoo.commons.client.util;

public class ServiceException extends Exception{
	
	public ServiceException() {
	}
	
	public ServiceException(Exception e)
	{
		super(e);
	}

	public ServiceException(String message)
	{
		super(message);
	}
}
