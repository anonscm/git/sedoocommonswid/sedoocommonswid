package fr.sedoo.commons.client.news.service;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;

import fr.sedoo.commons.shared.domain.New;

public interface NewsServiceAsync {

	void getLatest(AsyncCallback<ArrayList<New>> callback);


}
