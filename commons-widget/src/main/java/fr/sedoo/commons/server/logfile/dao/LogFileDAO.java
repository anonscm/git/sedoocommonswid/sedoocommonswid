package fr.sedoo.commons.server.logfile.dao;

import java.util.ArrayList;

import fr.sedoo.commons.shared.domain.LogFile;

public interface LogFileDAO {
	
	public final static String BEAN_NAME = "logFileDAO";
	ArrayList<LogFile> getLogFiles() throws Exception;

}
