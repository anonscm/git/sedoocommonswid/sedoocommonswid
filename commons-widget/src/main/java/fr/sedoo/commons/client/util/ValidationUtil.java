package fr.sedoo.commons.client.util;


public class ValidationUtil {

	public static boolean isValidEmail(String email) {
		String emailPattern = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.(?:[a-zA-Z]{2,6})$";
		if (email.matches(emailPattern) == false) {
			return false;
		}
		return true;
	}
}
