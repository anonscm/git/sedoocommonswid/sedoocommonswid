package fr.sedoo.commons.client.widget.dialog;

import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.IsWidget;

public interface DialogBoxContent extends IsWidget{
	
	void setDialogBox(DialogBox dialog);
	String getPreferredHeight();

}
