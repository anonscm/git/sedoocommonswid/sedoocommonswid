package fr.sedoo.commons.client.faq.mvp;

import fr.sedoo.commons.client.faq.ui.FaqConsultView;
import fr.sedoo.commons.client.faq.ui.FaqEditView;
import fr.sedoo.commons.client.faq.ui.FaqManageView;
import fr.sedoo.commons.client.mvp.AuthenticatedClientFactory;

public interface FaqClientFactory extends AuthenticatedClientFactory {

	FaqEditView getFaqEditView();
	FaqConsultView getFaqConsultView();
	FaqManageView getFaqManageView();

}
