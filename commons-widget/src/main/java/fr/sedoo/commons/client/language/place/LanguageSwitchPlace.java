package fr.sedoo.commons.client.language.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

import fr.sedoo.commons.client.util.LocaleUtil;

public class LanguageSwitchPlace extends Place {

	public static LanguageSwitchPlace instance;

	private String locale;

	public LanguageSwitchPlace(String locale) {
		super();
		this.locale = locale;
	}

	public static class Tokenizer implements PlaceTokenizer<LanguageSwitchPlace> {
		@Override
		public String getToken(LanguageSwitchPlace place) {
			return "";
		}

		@Override
		public LanguageSwitchPlace getPlace(String token) {
			if (instance == null) {
				instance = new LanguageSwitchPlace(LocaleUtil.ENGLISH);
			}
			return instance;
		}
	}

	public String getLocale() {
		return locale.toLowerCase();
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

}
