package fr.sedoo.commons.client.news.widget;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Widget;

import fr.sedoo.commons.client.back.BackButton;
import fr.sedoo.commons.client.back.BackPresenter;
import fr.sedoo.commons.client.component.AbstractView;
import fr.sedoo.commons.client.news.activity.NewsArchiveActivity;
import fr.sedoo.commons.shared.domain.New;
import fr.sedoo.commons.shared.domain.message.TitledMessage;

public class NewsArchiveViewImpl extends AbstractView implements NewsArchiveView {

	private static WelcomeViewImplUiBinder uiBinder = GWT.create(WelcomeViewImplUiBinder.class);

	interface WelcomeViewImplUiBinder extends UiBinder<Widget, NewsArchiveViewImpl> {
	}

	@UiField(provided=true)
	PagedNewsList list;

	@UiField
	BackButton backButton;

	public NewsArchiveViewImpl() {
		super();
		list = new PagedNewsList(NewsArchiveActivity.DEFAULT_PAGE_SIZE);
		initWidget(uiBinder.createAndBindUi(this));
		applyCommonStyle();
		backButton.addStyleName("btn-primary");
	}

	@Override
	public void reset() {
		list.reset();
	}

	@Override
	public void setBackPresenter(BackPresenter presenter) {
		backButton.setPresenter(presenter);
	}

	@Override
	public void setNews(ArrayList<? extends New> news) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setHits(int hits) {
		list.setHits(hits);
	}

	@Override
	public void setNewsByRange(int i, ArrayList<TitledMessage> messages) {
		list.setNewsByRange(i, messages);
	}

	@Override
	public void setNewsListPresenter(NewsListPresenter presenter) {
		list.setPresenter(presenter);

	}

}
