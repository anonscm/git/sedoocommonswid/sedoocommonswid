package fr.sedoo.commons.client.component.impl.menu;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.Command;

import fr.sedoo.commons.client.event.PlaceNavigationEvent;

public class PlaceCommand implements Command
{
	Place place;
	EventBus eventBus;
	
	public PlaceCommand(EventBus eventBus, Place place) 
	{
		this.eventBus = eventBus;
		this.place = place;
	}

	@Override
	public void execute() 
	{
		eventBus.fireEvent(new PlaceNavigationEvent(place));
	}
}
