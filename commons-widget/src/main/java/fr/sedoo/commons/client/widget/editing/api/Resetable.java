package fr.sedoo.commons.client.widget.editing.api;

public interface Resetable {

	void reset();
}
