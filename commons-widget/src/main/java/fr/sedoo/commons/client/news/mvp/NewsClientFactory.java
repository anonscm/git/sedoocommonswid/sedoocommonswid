package fr.sedoo.commons.client.news.mvp;

import fr.sedoo.commons.client.mvp.CommonClientFactory;
import fr.sedoo.commons.client.news.widget.NewDisplayView;
import fr.sedoo.commons.client.news.widget.NewsArchiveView;

public interface NewsClientFactory extends CommonClientFactory {

	NewDisplayView getNewDisplayView();

	NewsArchiveView getNewsArchiveView();

}
