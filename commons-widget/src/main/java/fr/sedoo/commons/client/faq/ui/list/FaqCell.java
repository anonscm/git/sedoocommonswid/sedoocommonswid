package fr.sedoo.commons.client.faq.ui.list;

import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.cell.client.ValueUpdater;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.EventTarget;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.dom.client.Style.Display;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.client.DOM;

import fr.sedoo.commons.client.animation.VerticalSlideAnimation;
import fr.sedoo.commons.client.util.ElementUtil;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.shared.domain.faq.QuestionAndAnswer;

public class FaqCell extends AbstractCell<QuestionAndAnswer>
{

	public FaqCell()
	{
		// On ecoute seulement les click
		super("click");
	}

	@Override
	public void render(Context context, QuestionAndAnswer value, SafeHtmlBuilder sb)
	{
		FaqRenderer.render(context, value, sb);
	}

	@Override
	public void onBrowserEvent(com.google.gwt.cell.client.Cell.Context context, Element parent, QuestionAndAnswer value, NativeEvent event, ValueUpdater<QuestionAndAnswer> valueUpdater)
	{
		super.onBrowserEvent(context, parent, value, event, valueUpdater);
		EventTarget eventTarget = event.getEventTarget();
		
		if (Element.is(eventTarget))
		{
			Element src = Element.as(eventTarget);
			String id = src.getId();
			if (FaqRenderer.isSrcId(id))
			{
				String answerId= FaqRenderer.getAnwerIdFromSrcId(id);
				String openId = FaqRenderer.getOpenIdFromSrcId(id);
				String closeId = FaqRenderer.getCloseIdFromSrcId(id);
				Element answer = DOM.getElementById(answerId);
				Element openDot = DOM.getElementById(openId);
				Element closeDot = DOM.getElementById(closeId);
				
				if (ElementUtil.isVisible(openDot))
				{
					ElementUtil.hide(openDot);
					ElementUtil.show(closeDot);
				}
				else
				{
					ElementUtil.hide(closeDot);
					ElementUtil.show(openDot);
				}
				
				VerticalSlideAnimation aux = new VerticalSlideAnimation(answer);
				aux.run(500);
//				if (StringUtil.isEmpty(answer.getStyle().getProperty("display")))
//				{
//					
////					answer.getStyle().setProperty("display", "none");
//				}
//				else
//				{
//					answer.getStyle().setProperty("display", "");
//				}
			}
		}
//		if (Element.is(eventTarget))
//		{
//			Element src = Element.as(eventTarget);
//
////			if (value instanceof MetadataUpdateNewDTO)
////			{
////				SummaryDTO summaryDTO = ((MetadataUpdateNewDTO) value).getSummaryDTO();
////				SummaryClickHandler.handleClick(src, summaryDTO, getPresenter());
////			}
//		}
	}

	

}
