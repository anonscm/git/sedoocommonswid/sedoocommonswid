package fr.sedoo.commons.client.style;

import com.google.gwt.resources.client.CssResource;

public interface SedooStyleCss extends CssResource {

	@ClassName("blockHeader")
	String blockHeader();

	@ClassName("blockHeaderText")
	String blockHeaderText();

	@ClassName("rssIcon")
	String rssIcon();

	@ClassName("newsTitle")
	String newsTitle();

	@ClassName("newsImageContainer")
	String newsImageContainer();

	@ClassName("newsImage")
	String newsImage();

	@ClassName("newsContent")
	String newsContent();

	@ClassName("newsBlock")
	String newsBlock();

	@ClassName("newsAddedOrModified")
	String newsAddedOrModified();

	@ClassName("dropFiles")
	String dropFiles();

	@ClassName("dropFilesOver")
	String dropFilesOver();

	@ClassName("dropFilesDisabled")
	String dropFilesDisabled();

	@ClassName("frontPageContent")
	String frontPageContent();

	@ClassName("frontPageContainer")
	String frontPageContainer();

	@ClassName("carouselImage")
	String carouselImage();

	@ClassName("softTopMargin")
	String softTopMargin();

	@ClassName("menuLink")
	String menuLink();

	@ClassName("sedooStackHeader")
	String sedooStackHeader();

	@ClassName("blueHeader")
	String blueHeader();

}
