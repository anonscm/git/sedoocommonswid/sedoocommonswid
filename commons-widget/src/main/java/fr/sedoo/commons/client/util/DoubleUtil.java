package fr.sedoo.commons.client.util;

import com.google.gwt.regexp.shared.RegExp;

public class DoubleUtil {

	public static RegExp decimalRegexp = RegExp.compile("^[-+]?\\d+(\\.{0,1}(\\d+?))?$");

	public static boolean checkDouble(String value) {
		if (value == null) {
			return false;
		} else {
			return decimalRegexp.test(value);
		}
	}

	public static String protectNullDouble(Double value) {
		if (value == null) {
			return "";
		} else if (value.isNaN()) {
			return "";
		} else {
			return value.toString();
		}
	}

}
