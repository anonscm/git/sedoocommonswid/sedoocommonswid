package fr.sedoo.commons.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface MaximizeEventHandler extends EventHandler {
    void onNotification(MaximizeEvent event);
}

