package fr.sedoo.commons.client.logfile.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

public class LogFileDisplayPlace extends Place {
	public static LogFileDisplayPlace instance;

	public LogFileDisplayPlace() {
	}

	public static class Tokenizer implements PlaceTokenizer<LogFileDisplayPlace> {

		public LogFileDisplayPlace getPlace(String token) {
			if (instance == null) {
				instance = new LogFileDisplayPlace();
			}
			return instance;
		}

		public String getToken(LogFileDisplayPlace place) {
			return "";
		}

	}
}