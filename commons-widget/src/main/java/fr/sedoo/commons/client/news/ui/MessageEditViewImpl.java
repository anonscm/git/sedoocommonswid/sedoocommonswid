package fr.sedoo.commons.client.news.ui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.gwtbootstrap3.client.ui.constants.Placement;
import org.moxieapps.gwt.uploader.client.Uploader;
import org.moxieapps.gwt.uploader.client.events.FileDialogCompleteEvent;
import org.moxieapps.gwt.uploader.client.events.FileDialogCompleteHandler;
import org.moxieapps.gwt.uploader.client.events.FileDialogStartEvent;
import org.moxieapps.gwt.uploader.client.events.FileDialogStartHandler;
import org.moxieapps.gwt.uploader.client.events.FileQueueErrorEvent;
import org.moxieapps.gwt.uploader.client.events.FileQueueErrorHandler;
import org.moxieapps.gwt.uploader.client.events.FileQueuedEvent;
import org.moxieapps.gwt.uploader.client.events.FileQueuedHandler;
import org.moxieapps.gwt.uploader.client.events.UploadErrorEvent;
import org.moxieapps.gwt.uploader.client.events.UploadErrorHandler;
import org.moxieapps.gwt.uploader.client.events.UploadProgressEvent;
import org.moxieapps.gwt.uploader.client.events.UploadProgressHandler;
import org.moxieapps.gwt.uploader.client.events.UploadSuccessEvent;
import org.moxieapps.gwt.uploader.client.events.UploadSuccessHandler;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.dom.client.NodeList;
import com.google.gwt.dom.client.Style.Cursor;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.DragLeaveEvent;
import com.google.gwt.event.dom.client.DragLeaveHandler;
import com.google.gwt.event.dom.client.DragOverEvent;
import com.google.gwt.event.dom.client.DragOverHandler;
import com.google.gwt.event.dom.client.DropEvent;
import com.google.gwt.event.dom.client.DropHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.TabPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.widgetideas.client.ProgressBar;

import fr.sedoo.commons.client.ckeditor.CKEditor;
import fr.sedoo.commons.client.ckeditor.DefaultCKEditorConfig;
import fr.sedoo.commons.client.component.AbstractView;
import fr.sedoo.commons.client.image.CommonBundle;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.news.bundle.NewsMessages;
import fr.sedoo.commons.client.news.style.NewsStyleBundle;
import fr.sedoo.commons.client.util.DateUtil;
import fr.sedoo.commons.client.util.ElementUtil;
import fr.sedoo.commons.client.util.LocaleUtil;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.client.widget.DialogBoxTools;
import fr.sedoo.commons.client.widget.api.HtmlTextEditor;
import fr.sedoo.commons.client.widget.tooltip.HelpTooltip;
import fr.sedoo.commons.client.widget.uploader.ImagePanelContent;
import fr.sedoo.commons.server.storedfile.TmpFileUploadServlet;
import fr.sedoo.commons.shared.domain.Message;
import fr.sedoo.commons.shared.domain.message.TitledMessage;
import fr.sedoo.commons.shared.domain.misc.ImageUrl;

/**
 * 
 * @author Francois ANDRE
 * 
 */
public class MessageEditViewImpl extends AbstractView implements MessageEditView, ClickHandler, KeyUpHandler {

	private final static int TITLE_LENGTH = 120;
	private final static String UPLOAD_ID = "ILLUSTRATION_UPLOAD_ID";

	private static MessageEditViewImplUiBinder uiBinder = GWT.create(MessageEditViewImplUiBinder.class);

	interface MessageEditViewImplUiBinder extends UiBinder<Widget, MessageEditViewImpl> {
	}

	boolean scheduledEnabled = false;

	private ProgressBar progressBar = new ProgressBar(0.0, 1.0, 0.0, new CancelProgressBarTextFormatter());
	private Image cancelButton = new Image(CommonBundle.INSTANCE.ko());

	HashMap<String, TextBox> titleEditors = new HashMap<String, TextBox>();
	HashMap<String, HtmlTextEditor> contentEditors = new HashMap<String, HtmlTextEditor>();

	@UiField
	VerticalPanel mainPanel;

	private ImagePanelContent imagePanelContent = new ImagePanelContent();

	Presenter presenter;
	private List<String> languages = new ArrayList<String>();

	private RadioButton urlButton;

	private RadioButton fileButton;

	private TextBox urlTextBox = new TextBox();

	private Label dropFileLabel = new Label();
	private Button urlViewButton = new Button();
	private Button fileViewButton = new Button();
	private Label orLabel = new Label(CommonMessages.INSTANCE.or().toLowerCase());

	/*
	 * Display the name of the image file. If no file has been uploaded a
	 * corresponding message is displayed.
	 */
	private Label uploadedFileName = new Label(NewsMessages.INSTANCE.noFileDownloaded());

	private String fileName = "";

	/* Indicates if the file is store temporarily or permanently */
	private boolean isTmpFile = false;

	/* Indicates if the drop zone is enabled or not */
	private boolean dropEnabled = false;
	private String uuid;
	private final static String DISABLED_TEXT_COLOR = "#929292";
	private final static String ACTIVATED_TEXT_COLOR = "#000000";
	private String displayLanguage;

	private Integer selectedTab;
	private boolean resizeNeeded;
	private int maxLangagesTab;

	public MessageEditViewImpl(String displayLanguage) {
		super();
		this.displayLanguage = displayLanguage;
		CommonBundle.INSTANCE.commonStyle().ensureInjected();
		NewsStyleBundle.INSTANCE.sedooStyle().ensureInjected();
		uploadedFileName.setStyleName(CommonBundle.INSTANCE.commonStyle().italic());
		initWidget(uiBinder.createAndBindUi(this));
		applyCommonStyle();
	}

	@Override
	public void setUuid(String uuid) {
		this.uuid = uuid;
		imagePanelContent.setUuid(uuid);
	}

	@Override
	public void setContent(HashMap<String, TitledMessage> messages, List<String> languages, String externaImagelUrl, String illustrationImageType, String fileName) {
		this.languages = languages;
		maxLangagesTab = languages.size() - 1;
		reset();
		TabPanel tabPanel = new TabPanel();
		tabPanel.setWidth("100%");
		tabPanel.setHeight("100%");
		contentEditors.clear();
		titleEditors.clear();
		Iterator<String> iterator = languages.iterator();
		while (iterator.hasNext()) {
			String language = (String) iterator.next();
			TitledMessage message = messages.get(language);
			if (message == null) {
				message = new TitledMessage();
			}
			TextBox titleEditor = new TextBox();
			titleEditor.setVisibleLength(TITLE_LENGTH);
			HtmlTextEditor contentEditor = new CKEditor(new DefaultCKEditorConfig(uuid, displayLanguage));
			titleEditors.put(language, titleEditor);
			titleEditor.setText(StringUtil.trimToEmpty(message.getTitle()));
			contentEditors.put(language, contentEditor);
			contentEditor.setHtml(StringUtil.trimToEmpty(message.getContent()));

			VerticalPanel panel = new VerticalPanel();
			panel.setWidth("100%");
			panel.setHeight("100%");
			panel.add(new Label(NewsMessages.INSTANCE.author() + " : " + StringUtil.trimToEmpty(message.getAuthor())));
			panel.add(new Label(CommonMessages.INSTANCE.date() + " : "
					+ DateUtil.getDateByLocale(message.getDate(), LocaleUtil.getClientLocaleLanguage(presenter.getClientFactory().getDefaultLanguage()))));
			panel.add(new Label(NewsMessages.INSTANCE.title()));
			panel.add(titleEditor);
			panel.add(new Label(NewsMessages.INSTANCE.content()));
			panel.add(contentEditor);

			tabPanel.add(panel, LocaleUtil.getLanguageFlag(language));
		}

		Image picture = new Image(CommonBundle.INSTANCE.picture());
		picture.setTitle(CommonMessages.INSTANCE.images());
		tabPanel.add(imagePanelContent, picture);

		Image illustration = new Image(CommonBundle.INSTANCE.illustration());
		illustration.setTitle(NewsMessages.INSTANCE.illustrationImage());
		VerticalPanel illustrationPanel = new VerticalPanel();
		illustrationPanel.setWidth("100%");

		HorizontalPanel panel = new HorizontalPanel();
		panel.setSpacing(5);
		panel.add(new Label(NewsMessages.INSTANCE.illustrationImage()));

		panel.add(new HelpTooltip(NewsMessages.INSTANCE.illustrationImageTooltip(), Placement.RIGHT));
		panel.add(new Label(":"));
		illustrationPanel.add(panel);

		if (illustrationImageType.compareTo(Message.INTERNAL_IMAGE) == 0) {
			illustrationPanel.add(getFileUploader(true));
			this.fileName = StringUtil.trimToEmpty(fileName);
			fileButton.setValue(true);
			setUrlPanelEnabled(false);
			setFilePanelEnabled(true);
			if (StringUtil.isEmpty(fileName)) {
				uploadedFileName.setText(NewsMessages.INSTANCE.noFileDownloaded());
				ElementUtil.hide(fileViewButton);
			} else {
				uploadedFileName.setText(fileName);
				ElementUtil.show(fileViewButton);
			}
		} else {
			illustrationPanel.add(getFileUploader(false));
			urlTextBox.setText(StringUtil.trimToEmpty(externaImagelUrl));
			setUrlPanelEnabled(true);
			setFilePanelEnabled(false);
		}

		tabPanel.add(illustrationPanel, illustration);

		selectedTab = 0;
		tabPanel.selectTab(selectedTab);
		mainPanel.add(tabPanel);
		mainPanel.setHeight("100%");

		tabPanel.addSelectionHandler(new SelectionHandler<Integer>() {

			@Override
			public void onSelection(SelectionEvent<Integer> event) {
				selectedTab = event.getSelectedItem();
				if (selectedTab <= maxLangagesTab) {
					resizeNeeded = true;
				} else {
					resizeNeeded = false;
				}
				Iterator<HtmlTextEditor> editorIterator = contentEditors.values().iterator();
				while (editorIterator.hasNext()) {
					HtmlTextEditor aux = (HtmlTextEditor) editorIterator.next();
					if (aux instanceof CKEditor) {
						((CKEditor) aux).onVisible();
					}

				}
			}
		});
		Scheduler.get().scheduleDeferred(new ScheduledCommand() {
			@Override
			public void execute() {
				onResize();
			}
		});

	}

	private VerticalPanel getFileUploader(boolean isFilePanelActivated) {
		VerticalPanel panel = new VerticalPanel();
		panel.setSpacing(5);
		panel.setHeight("20px");
		HorizontalPanel urlPanel = new HorizontalPanel();
		urlPanel.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
		urlPanel.setWidth("100%");
		urlButton = new RadioButton("image", (NewsMessages.INSTANCE.url()));
		urlButton.setStyleName(CommonBundle.INSTANCE.commonStyle().rightMargin3px());

		urlButton.addClickHandler(this);
		urlPanel.add(urlButton);
		urlTextBox = new TextBox();
		urlTextBox.setVisibleLength(60);
		urlTextBox.setStyleName(CommonBundle.INSTANCE.commonStyle().rightMargin3px());
		urlTextBox.addKeyUpHandler(this);
		urlPanel.add(urlTextBox);
		urlViewButton = new Button(CommonMessages.INSTANCE.view());
		urlPanel.add(urlViewButton);
		urlViewButton.addClickHandler(this);

		panel.add(urlPanel);
		HorizontalPanel fileButtonPanel = new HorizontalPanel();
		fileButtonPanel.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
		fileButton = new RadioButton("image", (NewsMessages.INSTANCE.file()));
		fileButton.setStyleName(CommonBundle.INSTANCE.commonStyle().rightMargin3px());
		progressBar.setStyleName(CommonBundle.INSTANCE.commonStyle().rightMargin3px());
		uploadedFileName.addStyleName(CommonBundle.INSTANCE.commonStyle().rightMargin3px());
		fileViewButton = new Button(CommonMessages.INSTANCE.view());
		fileViewButton.addClickHandler(this);
		ElementUtil.hide(progressBar);
		ElementUtil.hide(cancelButton);
		fileButton.addClickHandler(this);
		fileButtonPanel.add(fileButton);
		fileButtonPanel.add(uploadedFileName);
		fileButtonPanel.add(progressBar);
		fileButtonPanel.add(cancelButton);
		fileButtonPanel.add(fileViewButton);

		panel.add(fileButtonPanel);
		final VerticalPanel filePanel = new VerticalPanel();
		final Uploader uploader = new Uploader();
		if (isFilePanelActivated) {
			uploader.setButtonCursor(Uploader.Cursor.HAND);
			uploader.setButtonTextStyle(".buttonText {font-family: Arial, sans-serif; padding:5px;font-size: 12px; line-height:28px;border-radius: 3px; border: 1px solid #ADADAD ;background-color: #F1F1F1;color: "
					+ ACTIVATED_TEXT_COLOR + ";cursor:pointer;}");

		} else {
			uploader.setButtonCursor(Uploader.Cursor.ARROW);
			uploader.setButtonTextStyle(".buttonText {font-family: Arial, sans-serif; padding:5px;font-size: 12px; line-height:28px;border-radius: 3px; border: 1px solid #ADADAD ;background-color: #F1F1F1;color: "
					+ DISABLED_TEXT_COLOR + ";cursor:default;}");
		}

		if (GWT.isProdMode()) {
			uploader.setUploadURL(GWT.getModuleName() + TmpFileUploadServlet.PATH);
		} else {
			String hostPageBaseURL = GWT.getHostPageBaseURL();
			uploader.setUploadURL(hostPageBaseURL + GWT.getModuleName() + TmpFileUploadServlet.PATH);
		}

		uploader.setButtonText("<span id=\"" + UPLOAD_ID + "\" class=\"buttonText\">" + NewsMessages.INSTANCE.chooseFile() + "</span>").setButtonWidth(133).setButtonHeight(28)
				.setFileSizeLimit("20 GB").setButtonCursor(Uploader.Cursor.HAND).setButtonAction(Uploader.ButtonAction.SELECT_FILES).setFileQueuedHandler(new FileQueuedHandler() {
					public boolean onFileQueued(final FileQueuedEvent fileQueuedEvent) {
						// Create a Progress Bar for this file
						progressBar.setTitle(fileQueuedEvent.getFile().getName());
						progressBar.setHeight("18px");
						progressBar.setWidth("200px");

						// As of an upload has started permanent information is
						// lost
						fileName = fileQueuedEvent.getFile().getName();
						isTmpFile = true;

						// Add Cancel Button Image
						cancelButton.setStyleName("cancelButton");
						cancelButton.addClickHandler(new ClickHandler() {
							public void onClick(ClickEvent event) {
								uploader.cancelUpload(fileQueuedEvent.getFile().getId(), false);
								// progressBar.setProgress(-1.0d);
								resetUploader();
							}
						});

						ElementUtil.show(progressBar);
						ElementUtil.show(cancelButton);
						ElementUtil.hide(uploadedFileName);
						ElementUtil.hide(fileViewButton);

						return true;
					}
				}).setFileDialogStartHandler(new FileDialogStartHandler() {
					public boolean onFileDialogStartEvent(FileDialogStartEvent fileDialogStartEvent) {
						if (uploader.getStats().getUploadsInProgress() <= 0) {
							ElementUtil.hide(progressBar);
							ElementUtil.hide(cancelButton);
						}
						return true;
					}
				}).setUploadProgressHandler(new UploadProgressHandler() {
					public boolean onUploadProgress(UploadProgressEvent uploadProgressEvent) {
						progressBar.setProgress((double) uploadProgressEvent.getBytesComplete() / uploadProgressEvent.getBytesTotal());
						return true;
					}
				}).setUploadSuccessHandler(new UploadSuccessHandler() {

					@Override
					public boolean onUploadSuccess(UploadSuccessEvent uploadSuccessEvent) {
						ElementUtil.hide(cancelButton);
						ElementUtil.hide(progressBar);
						uploadedFileName.setText(fileName);
						ElementUtil.show(uploadedFileName);
						ElementUtil.show(fileViewButton);
						return true;
					}
				}).setFileDialogCompleteHandler(new FileDialogCompleteHandler() {
					public boolean onFileDialogComplete(FileDialogCompleteEvent fileDialogCompleteEvent) {
						if (fileDialogCompleteEvent.getTotalFilesInQueue() > 0 && uploader.getStats().getUploadsInProgress() <= 0) {
							progressBar.setProgress(0.0);
							uploader.startUpload();
						}
						return true;
					}
				}).setFileQueueErrorHandler(new FileQueueErrorHandler() {
					public boolean onFileQueueError(FileQueueErrorEvent fileQueueErrorEvent) {
						resetUploader();
						Window.alert("Upload of file " + fileQueueErrorEvent.getFile().getName() + " failed due to [" + fileQueueErrorEvent.getErrorCode().toString() + "]: "
								+ fileQueueErrorEvent.getMessage());
						return true;
					}
				}).setUploadErrorHandler(new UploadErrorHandler() {
					public boolean onUploadError(UploadErrorEvent uploadErrorEvent) {
						resetUploader();
						Window.alert("Upload of file " + uploadErrorEvent.getFile().getName() + " failed due to [" + uploadErrorEvent.getErrorCode().toString() + "]: " + uploadErrorEvent.getMessage());
						return true;
					}
				});

		filePanel.setSpacing(2);
		filePanel.getElement().getStyle().setMarginLeft(30, Unit.PX);
		filePanel.getElement().getStyle().setMarginTop(5, Unit.PX);
		filePanel.setStyleName(CommonBundle.INSTANCE.commonStyle().leftMargin5px());
		filePanel.add(uploader);

		if (Uploader.isAjaxUploadWithProgressEventsSupported()) {
			filePanel.add(orLabel);
			dropFileLabel = new Label(NewsMessages.INSTANCE.dropFile());
			dropFileLabel.setStyleName(NewsStyleBundle.INSTANCE.sedooStyle().dropFiles());
			dropFileLabel.addDragOverHandler(new DragOverHandler() {
				public void onDragOver(DragOverEvent event) {
					if (!uploader.getButtonDisabled()) {
						dropFileLabel.addStyleName(NewsStyleBundle.INSTANCE.sedooStyle().dropFilesOver());
					}
				}
			});
			dropFileLabel.addDragLeaveHandler(new DragLeaveHandler() {
				public void onDragLeave(DragLeaveEvent event) {
					dropFileLabel.removeStyleName(NewsStyleBundle.INSTANCE.sedooStyle().dropFilesOver());
				}
			});
			dropFileLabel.addDropHandler(new DropHandler() {
				public void onDrop(DropEvent event) {
					if (dropEnabled) {
						dropFileLabel.removeStyleName(NewsStyleBundle.INSTANCE.sedooStyle().dropFilesOver());

						if (uploader.getStats().getUploadsInProgress() <= 0) {
							progressBar.setProgress(-1.0d);
						}

						uploader.addFilesToQueue(Uploader.getDroppedFiles(event.getNativeEvent()));
						event.preventDefault();
					}
				}
			});
			filePanel.add(dropFileLabel);
		}

		urlButton.setValue(true);
		setUrlPanelEnabled(true);
		setFilePanelEnabled(false);
		urlTextBox.setText("");
		urlViewButton.setEnabled(false);
		panel.add(filePanel);
		return panel;
	}

	/**
	 * In case of failure of the upload, the uploader is reseted.
	 */
	private void resetUploader() {
		progressBar.setProgress(0.0);
		ElementUtil.hide(progressBar);
		ElementUtil.hide(cancelButton);
		fileName = "";
		uploadedFileName.setText(NewsMessages.INSTANCE.noFileDownloaded());
		ElementUtil.show(uploadedFileName);
		ElementUtil.hide(fileViewButton);
	}

	@Override
	public void reset() {
		mainPanel.clear();
		if (urlTextBox != null) {
			urlTextBox.setText("");
		}
		setUrlPanelEnabled(true);
		setFilePanelEnabled(false);
		imagePanelContent.reset();
	}

	@UiHandler("saveButton")
	void onSaveButtonClicked(ClickEvent event) {
		HashMap<String, TitledMessage> result = new HashMap<String, TitledMessage>();
		Iterator<String> iterator = languages.iterator();
		while (iterator.hasNext()) {
			String language = (String) iterator.next();
			String title = StringUtil.trimToEmpty(titleEditors.get(language).getText());
			String content = StringUtil.trimToEmpty(contentEditors.get(language).getHtml());
			result.put(language, new TitledMessage(title, content, "", null, "", "", uuid));
		}

		String illustrationImageType = Message.NO_IMAGE;
		String url = StringUtil.trimToEmpty(urlTextBox.getText());
		fileName = StringUtil.trimToEmpty(fileName);
		if (urlButton.getValue() == true) {
			if (StringUtil.isNotEmpty(url)) {
				illustrationImageType = Message.EXTERNAL_IMAGE;
			}
		} else if (fileButton.getValue() == true) {
			if (StringUtil.isNotEmpty(fileName)) {
				illustrationImageType = Message.INTERNAL_IMAGE;
			}
		}

		presenter.save(result, url, illustrationImageType, fileName, isTmpFile);
	}

	@UiHandler("backButton")
	void onBackButtonClicked(ClickEvent event) {
		presenter.back();
	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
		imagePanelContent.setPresenter(presenter);
	}

	@Override
	public void onClick(ClickEvent event) {
		if (event.getSource() == urlButton) {
			setUrlPanelEnabled(true);
			setFilePanelEnabled(false);
		} else if (event.getSource() == fileButton) {
			setUrlPanelEnabled(false);
			setFilePanelEnabled(true);
		} else if (event.getSource() == urlViewButton) {
			DialogBoxTools.popUpImage(NewsMessages.INSTANCE.illustrationImage(), StringUtil.trimToEmpty(urlTextBox.getText()));
		} else if (event.getSource() == fileViewButton) {
			String url = "";
			if (isTmpFile) {
				url = GWT.getModuleBaseURL() + getTmpFileServicePath() + fileName;
			} else {
				url = GWT.getModuleBaseURL() + getStoredFileServicePath() + "ILLUSTRATION_OF_" + uuid;
			}
			DialogBoxTools.popUpImage(NewsMessages.INSTANCE.illustrationImage(), url);
		}
	}

	private void setFilePanelEnabled(boolean value) {
		setDropFileEnabled(value);
		Element upload = DOM.getElementById(UPLOAD_ID);
		if (upload != null) {
			if (value == true) {
				upload.getStyle().setCursor(Cursor.POINTER);
				upload.getStyle().setColor("black");
			} else {
				upload.getStyle().setCursor(Cursor.DEFAULT);
				upload.getStyle().setColor(DISABLED_TEXT_COLOR);
			}
		}

		if (value == true) {
			orLabel.getElement().getStyle().setColor("black");
		} else {
			orLabel.getElement().getStyle().setColor(DISABLED_TEXT_COLOR);
			resetUploader();
		}

	}

	private void setDropFileEnabled(boolean value) {
		if (value == true) {
			dropFileLabel.removeStyleName(NewsStyleBundle.INSTANCE.sedooStyle().dropFilesDisabled());
		} else {
			dropFileLabel.addStyleName(NewsStyleBundle.INSTANCE.sedooStyle().dropFilesDisabled());
		}

	}

	private void setUrlPanelEnabled(boolean value) {
		urlTextBox.setEnabled(value);
		if (value == false) {
			urlViewButton.setEnabled(false);
			urlTextBox.setText("");
			dropEnabled = true;
		} else {
			urlViewButton.setEnabled(StringUtil.isNotEmpty(urlTextBox.getText()));
			dropEnabled = false;
		}
	}

	@Override
	public void onKeyUp(KeyUpEvent event) {
		if (event.getSource() == urlTextBox) {
			urlViewButton.setEnabled(StringUtil.isNotEmpty(urlTextBox.getText()));
		}

	}

	/**
	 * A renderer displaying the progression of the upload in percents.
	 * 
	 * @author Francois ANDRE
	 * 
	 */
	protected class CancelProgressBarTextFormatter extends ProgressBar.TextFormatter {
		@Override
		protected String getText(ProgressBar bar, double curProgress) {
			if (curProgress < 0) {
				return "Cancelled";
			}
			return ((int) (100 * bar.getPercent())) + "%";
		}
	}

	/*
	 * Return the part of the path to access to the temporary file REST service.
	 */
	private String getTmpFileServicePath() {
		return "rest/image/tmp/";
	}

	/*
	 * Return the part of the path to access to the stored file REST service.
	 */
	public static String getStoredFileServicePath() {
		return "rest/image/stored/";
	}

	/**
	 * Update the view after a successful save:
	 * <ul>
	 * <li>The file is no more considerated as temporary</li>
	 * <li>The uuid is updated</li>
	 * </ul>
	 */

	@Override
	public void brodcastSaved(String uuid) {
		isTmpFile = false;
		this.uuid = uuid;
	}

	@Override
	public void setImages(ArrayList<ImageUrl> images) {
		imagePanelContent.setImages(images);
	}

	@Override
	public void onResize() {
		super.onResize();
		resizeNeeded = true;
	}

	private void resizeCurrentEditor() {

		if (resizeNeeded) {
			if (selectedTab != null) {
				String index = "";
				CKEditor current = (CKEditor) contentEditors.get(languages.get(selectedTab));
				NodeList<com.google.gwt.dom.client.Element> divs = current.getElement().getParentElement().getElementsByTagName("div");
				if (divs == null) {
					return;
				} else {
					int length = divs.getLength();
					for (int i = 0; i < length; i++) {
						String id = divs.getItem(i).getId();
						if ((id.startsWith("cke_")) && (id.endsWith("_contents"))) {
							String[] split = id.split("_");
							index = split[1];
							break;
						}

					}
				}
				int containerHeight = mainPanel.getElement().getParentElement().getParentElement().getClientHeight();
				Element top = DOM.getElementById("cke_" + index + "_top");
				Element contents = DOM.getElementById("cke_" + index + "_contents");
				Element bottom = DOM.getElementById("cke_" + index + "_bottom");

				if ((top != null) && (contents != null) && (bottom != null)) {
					int topHeight = top.getClientHeight();
					// System.out.println("Container "+containerHeight);
					// System.out.println("TopHeight "+topHeight);
					// System.out.println("BottomHeight "+bottomHeight);
					int bottomHeight = bottom.getClientHeight();
					contents.getStyle().setHeight(containerHeight - topHeight - bottomHeight - 150, Unit.PX);
					resizeNeeded = false;
				}
			}
		}
	}

	@Override
	public void activityStart() {
		scheduledEnabled = true;
		Scheduler.get().scheduleFixedDelay(new Scheduler.RepeatingCommand() {
			@Override
			public boolean execute() {
				if (scheduledEnabled) {
					resizeCurrentEditor();
					return scheduledEnabled;
				} else {
					return false;
				}
			}
		}, 1000);
	}

	@Override
	public void activityStop() {
		scheduledEnabled = false;

	}

	@Override
	public void broadcastDeletion(String id) {
		imagePanelContent.broadcastDeletion(id);
	}
}
