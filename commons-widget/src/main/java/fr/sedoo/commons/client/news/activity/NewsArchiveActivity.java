package fr.sedoo.commons.client.news.activity;

import java.util.ArrayList;

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

import fr.sedoo.commons.client.back.BackPresenter;
import fr.sedoo.commons.client.back.BackUtils;
import fr.sedoo.commons.client.event.ActionEventConstants;
import fr.sedoo.commons.client.event.ActionStartEvent;
import fr.sedoo.commons.client.event.ActivityStartEvent;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.news.mvp.NewsClientFactory;
import fr.sedoo.commons.client.news.place.NewDisplayPlace;
import fr.sedoo.commons.client.news.place.NewsArchivePlace;
import fr.sedoo.commons.client.news.service.MessageService;
import fr.sedoo.commons.client.news.service.MessageServiceAsync;
import fr.sedoo.commons.client.news.widget.NewsArchiveView;
import fr.sedoo.commons.client.news.widget.NewsListPresenter;
import fr.sedoo.commons.client.service.DefaultAbstractCallBack;
import fr.sedoo.commons.client.util.LocaleUtil;
import fr.sedoo.commons.shared.domain.message.TitledMessage;

public class NewsArchiveActivity extends AbstractActivity implements BackPresenter, NewsListPresenter {

	private NewsClientFactory clientFactory;
	public final static int DEFAULT_PAGE_SIZE = 5;
	private final static int FIRST_NEW = 1;

	public final static MessageServiceAsync MESSAGE_SERVICE = GWT.create(MessageService.class);
	private NewsArchivePlace place;

	public NewsArchiveActivity(NewsArchivePlace place, NewsClientFactory clientFactory) {
		this.place = place;
		this.clientFactory = clientFactory;
	}

	public NewsArchivePlace getPlace() {
		return place;
	}

	private NewsArchiveView newArchiveView;

	/**
	 * Invoked by the ActivityManager to start a new Activity
	 */
	@Override
	public void start(AcceptsOneWidget containerWidget, final EventBus eventBus) {

		newArchiveView = clientFactory.getNewsArchiveView();
		newArchiveView.setBackPresenter(this);
		newArchiveView.setNewsListPresenter(this);
		containerWidget.setWidget(newArchiveView.asWidget());
		newArchiveView.reset();
		eventBus.fireEvent(new ActivityStartEvent(this));

		final ActionStartEvent e = new ActionStartEvent(CommonMessages.INSTANCE.loading(), ActionEventConstants.NEWS_LOADING_EVENT, true);
		eventBus.fireEvent(e);

		MESSAGE_SERVICE.getMessageHits(new DefaultAbstractCallBack<Integer>(e, eventBus) {
			@Override
			public void onSuccess(Integer hits) {
				super.onSuccess(hits);
				newArchiveView.setHits(hits);
				if (hits > 0) {
					eventBus.fireEvent(e);

					MESSAGE_SERVICE.getTitleMessagesByPage(FIRST_NEW, getPageSize(), LocaleUtil.getClientLocaleLanguage(clientFactory.getDefaultLanguage()), clientFactory.getLanguages(),
							new DefaultAbstractCallBack<ArrayList<TitledMessage>>(e, eventBus) {
								@Override
								public void onSuccess(ArrayList<TitledMessage> messages) {
									super.onSuccess(messages);
									newArchiveView.setNewsByRange(FIRST_NEW - 1, messages);
								}
							});

				}
			}
		});

	}

	@Override
	public void back() {
		BackUtils.goBack(clientFactory);
	}

	@Override
	public void displayNew(String newsUuid) {
		clientFactory.getPlaceController().goTo(new NewDisplayPlace(newsUuid));
	}

	@Override
	public void loadPageEntries(final int newNumber) {

		final ActionStartEvent e = new ActionStartEvent(CommonMessages.INSTANCE.loading(), ActionEventConstants.NEWS_LOADING_EVENT, true);
		EventBus eventBus = clientFactory.getEventBus();
		eventBus.fireEvent(e);

		MESSAGE_SERVICE.getTitleMessagesByPage(newNumber, getPageSize(), LocaleUtil.getClientLocaleLanguage(clientFactory.getDefaultLanguage()), clientFactory.getLanguages(),
				new DefaultAbstractCallBack<ArrayList<TitledMessage>>(e, eventBus) {
					@Override
					public void onSuccess(ArrayList<TitledMessage> messages) {
						super.onSuccess(messages);
						newArchiveView.setNewsByRange(newNumber - 1, messages);
					}
				});

	}
	
	public int getPageSize() {
		return DEFAULT_PAGE_SIZE;
	}

}