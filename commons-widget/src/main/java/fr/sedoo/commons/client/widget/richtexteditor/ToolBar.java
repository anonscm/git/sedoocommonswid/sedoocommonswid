package fr.sedoo.commons.client.widget.richtexteditor;

import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.RichTextArea;

public interface ToolBar extends IsWidget {

	void setRichTextArea(RichTextArea richTextArea);
}
