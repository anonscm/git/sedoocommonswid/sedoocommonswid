package fr.sedoo.commons.client.mvp;

import java.util.List;

import com.google.gwt.event.shared.EventBus;

import fr.sedoo.commons.client.language.place.LanguageSwitchPlace;
import fr.sedoo.commons.client.language.ui.LanguageSwitchingView;

/**
 * Une BasicClientFactory est une fabrique de vue permettant de gérer une
 * application multilingue minimale comprenant - Un EventBus - Une définition
 * des languges supportées par l'application (y.c. la langue par défaut - Une
 * vue affichant les informations relatives à la bascule des vues
 * 
 * @author francois
 * 
 */
public interface BasicClientFactory {
	EventBus getEventBus();

	String getDefaultLanguage();

	List<String> getLanguages();

	LanguageSwitchPlace getLanguageSwitchPlace(String language);

	LanguageSwitchingView getLanguageSwitchingView();
}
