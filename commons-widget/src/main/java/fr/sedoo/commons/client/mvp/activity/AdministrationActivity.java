package fr.sedoo.commons.client.mvp.activity;

import com.google.gwt.place.shared.Place;

import fr.sedoo.commons.client.event.ActivityStartEvent;
import fr.sedoo.commons.client.mvp.AuthenticatedClientFactory;

public abstract class AdministrationActivity extends AuthenticatedActivity {

	public AdministrationActivity(AuthenticatedClientFactory clientFactory, Place place) {
		super(clientFactory, place);
	}

	// @Override
	// protected boolean isValidUser() {
	// // Test if the user is logged
	// if (super.isValidUser() == false) {
	// return false;
	// } else
	// // Test if the user is admin
	// {
	// return ((AuthenticatedClientFactory)
	// clientFactory).getUserManager().getUser().isAdmin();
	// }
	// }

	protected void sendActivityStartEvent() {
		clientFactory.getEventBus().fireEvent(new ActivityStartEvent(ActivityStartEvent.ADMINISTRATOR_ACTIVITY));
	}

}
