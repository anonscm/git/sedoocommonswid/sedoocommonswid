package fr.sedoo.commons.shared.domain.faq;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import fr.sedoo.commons.client.widget.editing.ValidationAlert;
import fr.sedoo.commons.shared.domain.HasIdentifier;

public class FaqTableItem implements HasIdentifier, Serializable {
	private String uuid;
	private String question;
	private Integer order;

	public FaqTableItem() {
	}

	public FaqTableItem(String uuid, String question, Integer order) {
		setUuid(uuid);
		setQuestion(question);
		setOrder(order);
	}

	@Override
	public String getIdentifier() {
		return getUuid();
	}

	@Override
	public List<ValidationAlert> validate() {
		return new ArrayList<ValidationAlert>();
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public Integer getOrder() {
		return order;
	}

	public void setOrder(Integer order) {
		this.order = order;
	}
}
