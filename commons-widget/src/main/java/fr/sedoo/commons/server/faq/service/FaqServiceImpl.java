package fr.sedoo.commons.server.faq.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import fr.sedoo.commons.client.faq.service.FaqService;
import fr.sedoo.commons.client.util.ServiceException;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.server.DefaultServerApplication;
import fr.sedoo.commons.server.faq.dao.FaqDAO;
import fr.sedoo.commons.shared.domain.Faq;
import fr.sedoo.commons.shared.domain.faq.FaqTableItem;
import fr.sedoo.commons.shared.domain.faq.QuestionAndAnswer;

public class FaqServiceImpl extends RemoteServiceServlet implements FaqService {

	FaqDAO dao;

	@Override
	public HashMap<String, QuestionAndAnswer> getFaqContentByUuid(String uuid) throws ServiceException {

		initDao();
		return dao.findFaqContentByUuid(uuid);
	}

	@Override
	public QuestionAndAnswer getConsultFaqContentByUuid(String uuid, String preferredLanguage, List<String> alternateLanguages) throws ServiceException {
		initDao();
		return dao.getConsultFaqContentByUuid(uuid, preferredLanguage, alternateLanguages);
	}

	private void initDao() {
		if (dao == null) {
			dao = (FaqDAO) DefaultServerApplication.getSpringBeanFactory().getBeanByName(FaqDAO.BEAN_NAME);
		}
	}

	@Override
	public String saveFaq(String uuid, HashMap<String, QuestionAndAnswer> content) throws ServiceException {

		initDao();
		if (uuid.length() == 0) {
			// creation
			uuid = UUID.randomUUID().toString();
		} else {
			// On supprime toutes les anciennes valeurs
			dao.deleteFaqByUuid(uuid);
		}
		Iterator<String> iterator = content.keySet().iterator();
		while (iterator.hasNext()) {
			String language = iterator.next();
			QuestionAndAnswer faqContent = content.get(language);
			Faq aux = new Faq();
			aux.setAnswer(StringUtil.trimToEmpty(faqContent.getAnswer()));
			aux.setQuestion(StringUtil.trimToEmpty(faqContent.getQuestion()));
			aux.setOrder(faqContent.getOrder());
			aux.setLanguage(language);
			aux.setUuid(uuid);
			dao.save(aux);
		}
		return uuid;

	}

	@Override
	public ArrayList<FaqTableItem> getQuestions(String preferredLanguage, List<String> alternateLanguages) throws ServiceException {
		ArrayList<FaqTableItem> result = new ArrayList<FaqTableItem>();
		initDao();

		ArrayList<String> uuids = dao.getUuids();
		for (String uuid : uuids) {
			QuestionAndAnswer aux = dao.getConsultFaqContentByUuid(uuid, preferredLanguage, alternateLanguages);
			result.add(new FaqTableItem(uuid, aux.getQuestion(), aux.getOrder()));
		}
		return result;
	}

	@Override
	public void deleteFaqByUuid(String uuid) throws ServiceException {
		initDao();
		dao.deleteFaqByUuid(uuid);
	}

	@Override
	public ArrayList<QuestionAndAnswer> getAllQuestionsAndAnswers(String preferredLanguage, List<String> alternateLanguages) throws ServiceException {
		ArrayList<QuestionAndAnswer> result = new ArrayList<QuestionAndAnswer>();
		initDao();

		ArrayList<String> uuids = dao.getUuids();
		for (String uuid : uuids) {
			result.add(dao.getConsultFaqContentByUuid(uuid, preferredLanguage, alternateLanguages));
		}
		return result;
	}

	@Override
	public void swap(String firstId, String secondId) {
		initDao();
		dao.swap(firstId, secondId);
	}

}
