package fr.sedoo.commons.client.faq.ui.table;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;

import com.google.gwt.cell.client.Cell;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.ui.Image;

import fr.sedoo.commons.client.faq.bundle.FaqMessages;
import fr.sedoo.commons.client.faq.ui.FaqManageView.Presenter;
import fr.sedoo.commons.client.image.CommonBundle;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.widget.table.AbstractListTable;
import fr.sedoo.commons.client.widget.table.ImagedActionCell;
import fr.sedoo.commons.client.widget.table.ImagedActionCell.Delegate;
import fr.sedoo.commons.shared.domain.HasIdentifier;
import fr.sedoo.commons.shared.domain.faq.FaqTableItem;

public class FaqTable extends AbstractListTable {

	public FaqTable()
	{
		super();
	}
	
	private Presenter presenter;
	protected Column<HasIdentifier, String> upColumn;
	private Delegate<HasIdentifier> upAction;
	protected Column<HasIdentifier, String> downColumn;
	private Delegate<HasIdentifier> downAction;

	@Override
	public void presenterEdit(HasIdentifier hasIdentifier) {
		presenter.edit(((FaqTableItem) hasIdentifier).getUuid());
	}

	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
	}

	protected void initColumns() {

		TextColumn<HasIdentifier> questionColumn = new TextColumn<HasIdentifier>() {
			@Override
			public String getValue(HasIdentifier aux) {
				return ((FaqTableItem) aux).getQuestion();
			}
		};
		itemTable.addColumn(questionColumn, FaqMessages.INSTANCE.questions());
		itemTable.setColumnWidth(questionColumn, 100.0, Unit.PCT);

		itemTable.addColumn(editColumn);
		itemTable.setColumnWidth(editColumn, 30.0, Unit.PX);

		itemTable.addColumn(deleteColumn);
		itemTable.setColumnWidth(deleteColumn, 30.0, Unit.PX);

		Cell<HasIdentifier> upActionCell = new ImagedActionCell<HasIdentifier>(getUpAction(), new Image(CommonBundle.INSTANCE.upArrow()), CommonMessages.INSTANCE.up());
		upColumn = new Column(upActionCell) {

			@Override
			public HasIdentifier getValue(Object object) {
				return (HasIdentifier) object;
			}
		};

		Cell<HasIdentifier> downActionCell = new ImagedActionCell<HasIdentifier>(getDownAction(), new Image(CommonBundle.INSTANCE.downArrow()), CommonMessages.INSTANCE.down());
		downColumn = new Column(downActionCell) {

			@Override
			public HasIdentifier getValue(Object object) {
				return (HasIdentifier) object;
			}
		};

		itemTable.addColumn(upColumn);
		itemTable.setColumnWidth(upColumn, 30.0, Unit.PX);

		itemTable.addColumn(downColumn);
		itemTable.setColumnWidth(downColumn, 30.0, Unit.PX);

	}

	@Override
	public void presenterDelete(HasIdentifier hasId) {
		presenter.delete(((FaqTableItem) hasId).getUuid());
	}

	@Override
	public void addItem() {
		presenter.create();
	}

	@Override
	public String getAddItemText() {
		return FaqMessages.INSTANCE.addNewQuestion();
	}

	public void broadcastDeletion(String uuid) {

		ListIterator<FaqTableItem> listIterator = (ListIterator<FaqTableItem>) model.listIterator();
		while (listIterator.hasNext()) {
			FaqTableItem item = listIterator.next();
			if (item.getUuid().compareTo(uuid) == 0) {
				listIterator.remove();
				break;
			}
		}
		init(model);

	}

	private Delegate<HasIdentifier> getUpAction() {
		if (upAction == null) {
			upAction = new Delegate<HasIdentifier>() {

				@Override
				public void execute(final HasIdentifier hasId) {
					String previousId = getPreviousId(hasId.getIdentifier());
					if (previousId != null) {
						presenter.swap(previousId, hasId.getIdentifier());
					}
				}

			};
		}
		return upAction;
	}

	private Delegate<HasIdentifier> getDownAction() {
		if (downAction == null) {
			downAction = new Delegate<HasIdentifier>() {

				@Override
				public void execute(final HasIdentifier hasId) {
					String nextId = getNextId(hasId.getIdentifier());
					if (nextId != null) {
						presenter.swap(hasId.getIdentifier(), nextId);
					}
				}

			};
		}
		return downAction;
	}

	private String getNextId(String currentId) {
		Iterator<? extends HasIdentifier> iterator = getModel().iterator();
		while (iterator.hasNext()) {
			HasIdentifier current = iterator.next();
			if (current.getIdentifier().compareToIgnoreCase(currentId) == 0) {
				if (iterator.hasNext()) {
					return iterator.next().getIdentifier();
				}
			}
		}

		return null;
	}

	private String getPreviousId(String currentId) {
		String previousId = null;

		Iterator<? extends HasIdentifier> iterator = getModel().iterator();
		while (iterator.hasNext()) {
			HasIdentifier current = iterator.next();
			if (current.getIdentifier().compareToIgnoreCase(currentId) == 0) {
				return previousId;
			} else {
				previousId = current.getIdentifier();
			}
		}
		return previousId;
	}

	public void swapOrder(String firstId, String secondId) {
		ArrayList<HasIdentifier> newModel = new ArrayList<HasIdentifier>();
		ListIterator<FaqTableItem> listIterator = (ListIterator<FaqTableItem>) model.listIterator();
		while (listIterator.hasNext()) {
			FaqTableItem current = listIterator.next();
			if (current.getIdentifier().compareToIgnoreCase(firstId) == 0) {
				FaqTableItem next = listIterator.next();
				Integer tmp = next.getOrder();
				next.setOrder(current.getOrder());
				current.setOrder(tmp);
				newModel.add(next);
				newModel.add(current);
			} else {
				newModel.add(current);
			}
		}
		init(newModel);
	}
}
