package fr.sedoo.commons.client.news.widget;

import java.util.ArrayList;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.place.shared.PlaceChangeEvent;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;

import fr.sedoo.commons.client.DefaultClientApplication;
import fr.sedoo.commons.client.image.CommonBundle;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.news.place.NewsArchivePlace;
import fr.sedoo.commons.shared.domain.New;

public class NewsBlock extends VerticalPanel {
	NewsList newsList;

	public NewsBlock() {
		super();
		CommonBundle.INSTANCE.commonStyle().ensureInjected();
		setWidth("100%");
		setHeight("100%");
		newsList = new NewsList();
		add(newsList);
		add(getAllNewsLink());
	}

	private HorizontalPanel getAllNewsLink() {
		HorizontalPanel panel = new HorizontalPanel();
		panel.setWidth("100%");
		panel.setStyleName("showAll");

		HorizontalPanel secondaryPanel = new HorizontalPanel();

		Image image = new Image(CommonBundle.INSTANCE.menuDot());
		image.setStyleName(CommonBundle.INSTANCE.commonStyle().leftMargin5px());
		secondaryPanel.add(image);
		Label label = new Label(CommonMessages.INSTANCE.news() + " : " + CommonMessages.INSTANCE.completeList());
		label.setStyleName(CommonBundle.INSTANCE.commonStyle().leftMargin5px());
		label.addStyleName(CommonBundle.INSTANCE.commonStyle().clickable());
		label.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				NewsArchivePlace place = new NewsArchivePlace();
				DefaultClientApplication.getClientFactory().getEventBus().fireEvent(new PlaceChangeEvent(place));
			}
		});
		secondaryPanel.add(label);
		panel.add(secondaryPanel);

		return panel;
	}

	public void setNews(ArrayList<? extends New> news) {
		newsList.setRowData(news);
	}

	public void reset() {
		newsList.reset();
	}

	public void setPresenter(NewsListPresenter presenter) {
		newsList.setPresenter(presenter);
	}
}
