package fr.sedoo.commons.client.news.service;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import fr.sedoo.commons.client.util.ServiceException;
import fr.sedoo.commons.shared.domain.New;

@RemoteServiceRelativePath("news")

public interface NewsService extends RemoteService {
	ArrayList<New> getLatest() throws ServiceException;
}
