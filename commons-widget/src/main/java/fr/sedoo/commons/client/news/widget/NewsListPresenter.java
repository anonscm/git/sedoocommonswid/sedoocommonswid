package fr.sedoo.commons.client.news.widget;


public interface NewsListPresenter 
{
	void displayNew(String newsUuid);
	void loadPageEntries(int pageNumber);
}
