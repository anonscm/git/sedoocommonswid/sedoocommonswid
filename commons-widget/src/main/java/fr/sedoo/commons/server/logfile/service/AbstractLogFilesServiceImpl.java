package fr.sedoo.commons.server.logfile.service;

import java.util.ArrayList;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import fr.sedoo.commons.client.logfile.service.LogFilesService;
import fr.sedoo.commons.client.util.ServiceException;
import fr.sedoo.commons.server.DefaultServerApplication;
import fr.sedoo.commons.server.logfile.dao.LogFileDAO;
import fr.sedoo.commons.server.logfile.dao.LogFileDefaultDAO;
import fr.sedoo.commons.shared.domain.LogFile;

public abstract class AbstractLogFilesServiceImpl extends RemoteServiceServlet implements LogFilesService {

	LogFileDAO dao;

	@Override
	public ArrayList<LogFile> getLogFiles() throws ServiceException {
		initDao();
		try {
			return dao.getLogFiles();
		} catch (Exception e) {
			throw new ServiceException(e.getMessage());
		}
	}

	private void initDao() throws ServiceException {
		if (dao == null) {
			dao = (LogFileDefaultDAO) DefaultServerApplication.getSpringBeanFactory().getBeanByName(LogFileDAO.BEAN_NAME);
			((LogFileDefaultDAO) dao).setLogDirectory(getLogDirectory());
		}
	}

	protected abstract String getLogDirectory();

}
