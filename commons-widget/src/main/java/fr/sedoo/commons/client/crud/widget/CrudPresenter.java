package fr.sedoo.commons.client.crud.widget;

import fr.sedoo.commons.shared.domain.HasIdentifier;

public interface CrudPresenter 
{
	void delete(HasIdentifier hasIdentifier);
	void create(HasIdentifier hasIdentifier);
	void edit(HasIdentifier hasIdentifier);
}
