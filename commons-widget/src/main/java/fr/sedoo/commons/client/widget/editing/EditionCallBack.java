package fr.sedoo.commons.client.widget.editing;

public interface EditionCallBack<T> {
	
	void postEditProcess(boolean result, T editedObject);

}
