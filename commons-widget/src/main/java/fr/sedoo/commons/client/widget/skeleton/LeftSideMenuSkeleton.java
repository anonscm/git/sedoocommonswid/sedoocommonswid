package fr.sedoo.commons.client.widget.skeleton;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.SimpleLayoutPanel;
import com.google.gwt.user.client.ui.SplitLayoutPanel;
import com.google.gwt.user.client.ui.Widget;

import fr.sedoo.commons.client.component.api.NavigationBar;
import fr.sedoo.commons.client.event.MaximizeEvent;
import fr.sedoo.commons.client.event.MaximizeEventHandler;
import fr.sedoo.commons.client.event.MinimizeEvent;
import fr.sedoo.commons.client.event.MinimizeEventHandler;
import fr.sedoo.commons.client.widget.PreferredHeightWidget;
import fr.sedoo.commons.client.widget.PreferredWidthWidget;

public class LeftSideMenuSkeleton extends Skeleton implements  MinimizeEventHandler, MaximizeEventHandler{

	private DockLayoutPanel mainPanel;
	private SplitLayoutPanel splitPanel;
	
	private Widget north;
	private Widget south;
	private Widget west;
	
	private SimpleLayoutPanel contentPanel =  new SimpleLayoutPanel();
	
	public LeftSideMenuSkeleton (EventBus eventBus)
	{
		super(eventBus);
	}
	
	public void guiInit(PreferredHeightWidget header, PreferredHeightWidget footer, PreferredWidthWidget leftMenu, NavigationBar navigationBar)
	{
		north = header.asWidget();
		south = footer.asWidget();
		west = leftMenu.asWidget();
		eventBus.addHandler(MaximizeEvent.TYPE, this);
		eventBus.addHandler(MinimizeEvent.TYPE, this);
		init(header, footer, leftMenu, navigationBar);
	}

	private void init(PreferredHeightWidget header, PreferredHeightWidget footer, PreferredWidthWidget menu, NavigationBar navigationBar) {
		mainPanel = new DockLayoutPanel(Unit.PX);
		mainPanel.getElement().getStyle().setProperty("border","white solid 0px");
		mainPanel.getElement().getStyle().setProperty("boxShadow","1px 1px 5px 1px rgba(0, 0, 0, 0.7)");
		mainPanel.setWidth("960px");
		mainPanel.getElement().getStyle().setProperty("margin", "auto");	
		mainPanel.addNorth(header, header.getPreferredHeight());
		mainPanel.addSouth(footer, footer.getPreferredHeight());
		DockLayoutPanel aux = new DockLayoutPanel(Unit.PX);
		aux.addNorth(navigationBar, navigationBar.getPreferredHeight());
		splitPanel = new SplitLayoutPanel();
		splitPanel.addWest(menu, menu.getPreferredWidth());
		splitPanel.add(contentPanel);
		aux.add(splitPanel);
		mainPanel.add(aux);
		
		add(mainPanel);
	}

	
	public AcceptsOneWidget getContentPanel() {
		return contentPanel;
	}
	
	@Override
	public void onNotification(MaximizeEvent event) {
		mainPanel.setWidgetHidden(north, true);
		mainPanel.setWidgetHidden(south, true);
		splitPanel.setWidgetHidden(west, true);
		mainPanel.animate(500);
		splitPanel.animate(500);
	}

	@Override
	public void onNotification(MinimizeEvent event) {
		mainPanel.setWidgetHidden(north, false);
		mainPanel.setWidgetHidden(south, false);
		splitPanel.setWidgetHidden(west, false);
		mainPanel.animate(500);
		splitPanel.animate(500);
	}
	
	

}
