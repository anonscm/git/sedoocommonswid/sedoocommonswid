package fr.sedoo.commons.client.widget.panel;

import java.util.Iterator;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiConstructor;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import fr.sedoo.commons.client.animation.VerticalSlideAnimation;

public class HorizontalCollapsingPanel extends Composite implements HasWidgets  {

	private static HorizontalCollapsingPanelUiBinder uiBinder = GWT
			.create(HorizontalCollapsingPanelUiBinder.class);

	interface HorizontalCollapsingPanelUiBinder extends
	UiBinder<Widget, HorizontalCollapsingPanel> {
	}

	@UiConstructor
	public HorizontalCollapsingPanel(String title) {
		initWidget(uiBinder.createAndBindUi(this));
		setTitle(title);
	}

	@UiField
	Image showPanel;

	@UiField
	Image hidePanel;

	@UiField
	Label title;

	@UiField
	VerticalPanel content;

	private int milliseconds=500;
	
	public void setTitle(String title) {
		this.title.setText(title);
	}

	@UiHandler("showPanel")
	void onShowFilterButtonClicked(ClickEvent event) {
		showPanel();
	}

	@UiHandler("hidePanel")
	void onHideFilterButtonClicked(ClickEvent event) {
		hidePanel();
	}

	void showFilterContent(boolean value)
	{
		int widgetCount = content.getWidgetCount();
		for (int i = 0; i < widgetCount; i++) 
		{
			content.getWidget(i).setVisible(value);
		}
	}

	@Override
	public void add(Widget widget) {
		content.add(widget);
	}

	@Override
	public void clear() {
		content.clear();
	}

	@Override
	public Iterator<Widget> iterator() {
		return content.iterator();
	}

	@Override
	public boolean remove(Widget w) {
		return content.remove(w);
	}

	public void hidePanel()
	{
		hidePanel.setVisible(false);
		showPanel.setVisible(true);
		showFilterContent(false);
		VerticalSlideAnimation animation = new VerticalSlideAnimation(content);
		if (getMilliseconds() > 0)
		{
			animation.run(getMilliseconds());
		}
	}
	
	public void showPanel()
	{
		hidePanel.setVisible(true);
		showPanel.setVisible(false);
		showFilterContent(true);
		VerticalSlideAnimation animation = new VerticalSlideAnimation(content);
		if (getMilliseconds() > 0)
		{
			animation.run(getMilliseconds());
		}	}

	public int getMilliseconds() {
		return milliseconds;
	}

	public void setMilliseconds(int milliseconds) {
		this.milliseconds = milliseconds;
	}
	
}
