package fr.sedoo.commons.client.news.activity;

public interface MessageManagementEventConstant {
	public final int MESSAGE_LOADING_EVENT = 1001;
	public final int MESSAGE_SAVING_EVENT = 1002;
	public final int MESSAGE_DELETING_EVENT = 1003;
	public final int LANGUAGE_SWITCHING_EVENT = 1004;
}
