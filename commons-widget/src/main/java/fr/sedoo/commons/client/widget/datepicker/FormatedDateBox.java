package fr.sedoo.commons.client.widget.datepicker;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.datepicker.client.DateBox;

public class FormatedDateBox extends DateBoxWithYearSelector{
	
	public FormatedDateBox(String format) {
		super(new DateBox.DefaultFormat(DateTimeFormat.getFormat(format)));
	}

}
