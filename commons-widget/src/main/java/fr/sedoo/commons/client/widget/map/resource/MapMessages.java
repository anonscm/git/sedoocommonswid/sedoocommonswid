package fr.sedoo.commons.client.widget.map.resource;

import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.Messages;

public interface MapMessages extends Messages
{

	public static final MapMessages INSTANCE = GWT.create(MapMessages.class);

	@Key("metadataEditing.noValidationAlert")
	public String metadataEditingNoValidationAlert();
}