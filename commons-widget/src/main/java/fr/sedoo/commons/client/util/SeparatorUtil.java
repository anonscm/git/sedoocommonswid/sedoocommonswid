package fr.sedoo.commons.client.util;

public class SeparatorUtil {
	public final static String UNDERSCORE_SEPARATOR = "_";
	public final static String AROBAS_SEPARATOR = "@";
	public static final String PIPE_SEPARATOR = "|";
}
