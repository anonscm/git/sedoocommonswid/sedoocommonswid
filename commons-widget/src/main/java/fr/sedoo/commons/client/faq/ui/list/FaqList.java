package fr.sedoo.commons.client.faq.ui.list;

import com.google.gwt.user.cellview.client.CellList;

import fr.sedoo.commons.client.widget.celllist.CellListResources;
import fr.sedoo.commons.shared.domain.New;
import fr.sedoo.commons.shared.domain.faq.QuestionAndAnswer;

public class FaqList extends CellList<QuestionAndAnswer>{

	FaqCell faqCell;
	
	public FaqList() 
	{
		super(new FaqCell(),CellListResources.INSTANCE);
		faqCell = (FaqCell) getCell();
		setKeyboardSelectionPolicy(KeyboardSelectionPolicy.DISABLED);
	}
	
	
	
	

}
