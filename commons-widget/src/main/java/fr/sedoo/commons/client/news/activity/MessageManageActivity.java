package fr.sedoo.commons.client.news.activity;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

import fr.sedoo.commons.client.event.ActionEventConstants;
import fr.sedoo.commons.client.event.ActionStartEvent;
import fr.sedoo.commons.client.event.ActivityStartEvent;
import fr.sedoo.commons.client.event.NotificationEvent;
import fr.sedoo.commons.client.event.PlaceNavigationEvent;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.mvp.BasicClientFactory;
import fr.sedoo.commons.client.mvp.activity.AdministrationActivity;
import fr.sedoo.commons.client.news.event.MessageChangeEvent;
import fr.sedoo.commons.client.news.mvp.MessageClientFactory;
import fr.sedoo.commons.client.news.place.MessageEditPlace;
import fr.sedoo.commons.client.news.place.MessageManagePlace;
import fr.sedoo.commons.client.news.service.MessageService;
import fr.sedoo.commons.client.news.service.MessageServiceAsync;
import fr.sedoo.commons.client.news.ui.MessageManageView;
import fr.sedoo.commons.client.news.ui.MessageManageView.Presenter;
import fr.sedoo.commons.client.service.DefaultAbstractCallBack;
import fr.sedoo.commons.client.util.LocaleUtil;
import fr.sedoo.commons.shared.domain.message.MessageTableItem;

public abstract class MessageManageActivity extends AdministrationActivity implements Presenter {

	private static final MessageServiceAsync MESSAGE_SERVICE = GWT.create(MessageService.class);

	protected MessageManageView messageManageView;

	public MessageManageActivity(MessageManagePlace place, MessageClientFactory clientFactory) {
		super(clientFactory, place);
	}

	/**
	 * Invoked by the ActivityManager to start a new Activity
	 */
	@Override
	public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {
		if (isValidUser() == false) {
			goToLoginPlace();
			return;
		}
		messageManageView = ((MessageClientFactory) clientFactory).getMessageManageView();
		messageManageView.setPresenter(this);
		eventBus.fireEvent(new ActivityStartEvent(this));
		containerWidget.setWidget(messageManageView.asWidget());
		ActionStartEvent e = new ActionStartEvent(CommonMessages.INSTANCE.loading(), ActionEventConstants.BASIC_LOADING_EVENT, true);
		eventBus.fireEvent(e);

		MESSAGE_SERVICE.getTitles(LocaleUtil.getClientLocaleLanguage(clientFactory.getDefaultLanguage()), clientFactory.getLanguages(), new DefaultAbstractCallBack<ArrayList<MessageTableItem>>(e,
				eventBus) {

			@Override
			public void onSuccess(ArrayList<MessageTableItem> result) {
				super.onSuccess(result);
				messageManageView.setMessages(result);
			}

		});

	}

	@Override
	public void edit(String id) {
		clientFactory.getEventBus().fireEvent(new PlaceNavigationEvent(new MessageEditPlace(id)));
	}

	@Override
	public void create() {
		clientFactory.getEventBus().fireEvent(new PlaceNavigationEvent(new MessageEditPlace()));
	}

	@Override
	public void delete(final String uuid) {

		EventBus eventBus = clientFactory.getEventBus();
		ActionStartEvent e = new ActionStartEvent(CommonMessages.INSTANCE.deleting(), ActionEventConstants.BASIC_DELETING_EVENT, true);
		eventBus.fireEvent(e);
		MESSAGE_SERVICE.deleteMessageByUuid(uuid, new DefaultAbstractCallBack<Void>(e, eventBus) {

			@Override
			public void onSuccess(Void result) {
				super.onSuccess(result);
				messageManageView.broadcastDeletion(uuid);
				clientFactory.getEventBus().fireEvent(new NotificationEvent(CommonMessages.INSTANCE.deletedElement()));
				clientFactory.getEventBus().fireEvent(new MessageChangeEvent());
			}
		});
	}

	@Override
	public BasicClientFactory getClientFactory() {
		return clientFactory;
	}

}