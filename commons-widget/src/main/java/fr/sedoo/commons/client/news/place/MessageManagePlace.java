package fr.sedoo.commons.client.news.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

import fr.sedoo.commons.client.mvp.place.AuthenticatedPlace;

public class MessageManagePlace extends Place implements AuthenticatedPlace
{

	public static MessageManagePlace instance;

	public static class Tokenizer implements PlaceTokenizer<MessageManagePlace>
	{
		@Override
		public String getToken(MessageManagePlace place)
		{
			return "";
		}

		@Override
		public MessageManagePlace getPlace(String token)
		{
			if (instance == null)
			{
				instance = new MessageManagePlace();
			}
			return instance;
		}
	}
	
}
