package fr.sedoo.commons.server;

import fr.sedoo.commons.spring.SpringBeanFactory;

public class DefaultServerApplication {

	private static SpringBeanFactory springBeanFactory = new SpringBeanFactory();

	public static SpringBeanFactory getSpringBeanFactory() {
		return springBeanFactory;
	}

	public static void setSpringBeanFactory(SpringBeanFactory springBeanFactory) {
		DefaultServerApplication.springBeanFactory = springBeanFactory;
	}
	
}
