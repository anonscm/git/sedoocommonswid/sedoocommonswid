package fr.sedoo.commons.client.news.event;

import com.google.gwt.event.shared.GwtEvent;

public class MessageChangeEvent extends GwtEvent<MessageChangeEventHandler>{

    public static final Type<MessageChangeEventHandler> TYPE = new Type<MessageChangeEventHandler>();

    public MessageChangeEvent()
    {
    }
    
	@Override
    protected void dispatch(MessageChangeEventHandler handler) {
            handler.onNotification(this);
    }

    @Override
    public com.google.gwt.event.shared.GwtEvent.Type<MessageChangeEventHandler> getAssociatedType() {
            return TYPE;
    }
    

}