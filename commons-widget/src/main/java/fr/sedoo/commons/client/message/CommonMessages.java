package fr.sedoo.commons.client.message;

import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.LocalizableResource.DefaultLocale;
import com.google.gwt.i18n.client.LocalizableResource.Generate;
import com.google.gwt.i18n.client.Messages;

@Generate(format = { "com.google.gwt.i18n.rebind.format.PropertiesFormat" }, fileName = "Message", locales = { "fr", "en", "default" })
@DefaultLocale("en")
public interface CommonMessages extends Messages {

	public static final CommonMessages INSTANCE = GWT.create(CommonMessages.class);

	@Key("welcome")
	public String welcome();

	@Key("home")
	public String home();

	@Key("help")
	public String help();

	@Key("about")
	public String about();

	@Key("news")
	public String news();

	@Key("noNews")
	public String noNews();

	@Key("message")
	public String message();

	@Key("addedOrModified")
	public String addedOrModified();

	@Key("introduction")
	public String introduction();

	@Key("information")
	public String information();

	@Key("description")
	public String description();

	@Key("error")
	public String error();

	@Key("anErrorHasOccurred")
	public String anErrorHasOccurred();

	@Key("confirm")
	public String confirm();

	@Key("doYouConfirmThisAction")
	public String doYouConfirmThisAction();

	@Key("yes")
	public String yes();

	@Key("no")
	public String no();

	@Key("ok")
	public String ok();

	@Key("cancel")
	public String cancel();

	@Key("version")
	public String version();

	@Key("minimize")
	public String minimize();

	@Key("maximize")
	public String maximize();

	@Key("breadCrumbIntroductionText")
	public String breadCrumbIntroductionText();

	@Key("parameterLoading")
	public String parameterLoading();

	@Key("loading")
	public String loading();

	@Key("refreshing")
	public String refreshing();

	@Key("connecting")
	public String connecting();

	@Key("edit")
	public String edit();

	@Key("view")
	public String view();

	@Key("delete")
	public String delete();

	@Key("emptyList")
	public String emptyList();

	@Key("deletionConfirmMessage")
	public String deletionConfirmMessage();

	@Key("deletedElement")
	public String deletedElement();

	@Key("saving")
	public String saving();

	@Key("save")
	public String save();

	@Key("savedElement")
	public String savedElement();

	@Key("name")
	public String name();

	@Key("firstName")
	public String firstName();

	@Key("familyName")
	public String familyName();

	@Key("email")
	public String email();

	@Key("organization")
	public String organization();

	@Key("address")
	public String address();

	@Key("street")
	public String street();

	@Key("zipCode")
	public String zipCode();

	@Key("country")
	public String country();

	@Key("city")
	public String city();

	@Key("login")
	public String login();

	@Key("password")
	public String password();

	@Key("confirmPassword")
	public String confirmPassword();

	@Key("externalAuthentication")
	public String externalAuthentication();

	@Key("content")
	public String content();

	@Key("value")
	public String value();

	@Key("date")
	public String date();

	@Key("incorrectLogin")
	public String incorrectLogin();

	@Key("incorrectPassword")
	public String incorrectPassword();

	@Key("incorrectLoginOrPassword")
	public String incorrectLoginOrPassword();

	@Key("authentication")
	public String authentication();

	@Key("signIn")
	public String signIn();

	@Key("signOut")
	public String signOut();

	@Key("loginFirst")
	public String loginFirst();

	@Key("loginSuccess")
	public String loginSuccess();

	@Key("loginFailure")
	public String loginFailure();

	@Key("notConnected")
	public String notConnected();

	@Key("connect")
	public String connect();

	@Key("loggedAs")
	public String loggedAs();

	@Key("pagerOf")
	public String pagerOf();

	@Key("pagerOfOver")
	public String pagerOfOver();

	@Key("deleting")
	public String deleting();

	@Key("back")
	public String back();

	@Key("reset")
	public String reset();

	@Key("resetting")
	public String resetting();

	@Key("operationSuccessful")
	public String operationSuccessful();

	@Key("systemInformation")
	public String systemInformation();

	@Key("by")
	public String by();

	@Key("or")
	public String or();

	@Key("and")
	public String and();

	@Key("validate")
	public String validate();

	@Key("type")
	public String type();

	@Key("zoom")
	public String zoom();

	@Key("completeList")
	public String completeList();

	@Key("userManagement")
	public String userManagement();

	@Key("download")
	public String download();

	@Key("up")
	public String up();

	@Key("down")
	public String down();

	@Key("operationInProgress")
	public String operationInProgress();

	@Key("images")
	public String images();

	@Key("addImage")
	public String addImage();

	@Key("processing")
	public String processing();

	@Key("launch")
	public String launch();

	@Key("role")
	public String role();

	@Key("roles")
	public String roles();

	@Key("addUser")
	public String addUser();

	@Key("insufficientPrivileges")
	public String insufficientPrivileges();

	@Key("print")
	public String print();
}
