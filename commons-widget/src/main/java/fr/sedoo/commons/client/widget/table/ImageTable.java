package fr.sedoo.commons.client.widget.table;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.cell.client.Cell;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.Column;

import fr.sedoo.commons.client.widget.uploader.FileUploadPresenter;
import fr.sedoo.commons.shared.domain.HasIdentifier;
import fr.sedoo.commons.shared.domain.IsUrl;
import fr.sedoo.commons.shared.domain.misc.ImageUrl;

public class ImageTable extends AbstractListTable {

	private FileUploadPresenter presenter;

	public ImageTable() {
		super();
		hideToolBar();
	}

	@Override
	public void presenterEdit(HasIdentifier hasIdentifier) {
		// Nothing is done this way
	}

	public void setPresenter(FileUploadPresenter presenter) {
		this.presenter = presenter;
	}

	protected void initColumns() {

		Cell<IsUrl> previewCell = new PreviewCell();
		Column previewColumn = new Column(previewCell) {

			@Override
			public IsUrl getValue(Object object) {
				return (IsUrl) object;
			}
		};

		itemTable.addColumn(previewColumn);
		itemTable.setColumnWidth(previewColumn, 110.0, Unit.PX);

		itemTable.addColumn(deleteColumn);
		itemTable.setColumnWidth(deleteColumn, 30.0, Unit.PX);

	}

	@Override
	public void presenterDelete(HasIdentifier hasId) {
		presenter.deleteImageById(hasId.getIdentifier());
	}

	@Override
	public void addItem() {

	}

	@Override
	public String getAddItemText() {
		return "";
	}

	public void reset() {
		init(new ArrayList<HasIdentifier>());
	}

	public void addElement(ImageUrl url) {
		ArrayList<HasIdentifier> newModel = new ArrayList<HasIdentifier>();
		List<? extends HasIdentifier> currentModel = getModel();
		newModel.addAll(currentModel);
		newModel.add(url);
		init(newModel);
	}

}
