package fr.sedoo.commons.client.news.widget;

import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;

import fr.sedoo.commons.shared.domain.message.TitledMessage;

public interface TitledMessageRenderer {
	void render(Context context, final TitledMessage message, final SafeHtmlBuilder sb, final String language);
}
