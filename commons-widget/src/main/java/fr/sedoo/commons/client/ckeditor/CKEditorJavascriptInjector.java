package fr.sedoo.commons.client.ckeditor;

import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.HeadElement;
import com.google.gwt.dom.client.ScriptElement;

public class CKEditorJavascriptInjector {

	private static HeadElement head;
	private static boolean loaded = false;

	private static void inject(String javascript) {
		if (loaded == false)
		{
			HeadElement head = getHead();
			ScriptElement element = createScriptElement();
			element.setText(javascript);
			head.appendChild(element);
		}
	}

	private static ScriptElement createScriptElement() {
		ScriptElement script = Document.get().createScriptElement();
		script.setAttribute("language", "javascript");
		return script;
	}

	private static HeadElement getHead() {
		if (head == null) {
			Element element = Document.get().getElementsByTagName("head").getItem(0);
			assert element != null : "HTML Head element required";
			HeadElement head = HeadElement.as(element);
			CKEditorJavascriptInjector.head = head;
		}
		return CKEditorJavascriptInjector.head;
	}

	public static native boolean libraryReady()
	/*-{
		if($wnd.CKEDITOR) {
			return true;
		} 
		return false;
	}-*/;

	public native static String version()
	/*-{
		return $wnd.CKEDITOR.version;
	}-*/;

	public static void injectCKEditorLibrairies() 
	{
		CKEditorJavascriptInjector.inject(CKEditorJSBundle.INSTANCE.ckEditorJS().getText());		
	}
}