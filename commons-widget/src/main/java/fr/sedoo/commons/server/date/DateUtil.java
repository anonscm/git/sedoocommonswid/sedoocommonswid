package fr.sedoo.commons.server.date;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {

	public final static String FRENCH = "fr";
	public final static String ENGLISH = "en";

	public final static SimpleDateFormat frenchFormatter = new SimpleDateFormat("dd/MM/yyyy");
	public final static SimpleDateFormat englishFormatter = new SimpleDateFormat("MM/dd/yyyy");

	public static String formatDate(Date date, String language) {
		if (language.compareToIgnoreCase(FRENCH) == 0) {
			return frenchFormatter.format(date);
		} else {
			return englishFormatter.format(date);
		}
	}

}
