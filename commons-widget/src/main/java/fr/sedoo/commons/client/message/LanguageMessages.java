package fr.sedoo.commons.client.message;

import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.LocalizableResource.Generate;
import com.google.gwt.i18n.client.Messages;

@Generate(format = { "com.google.gwt.i18n.rebind.format.PropertiesFormat" }, fileName = "Message")
public interface LanguageMessages extends Messages {

	public static final LanguageMessages INSTANCE = GWT.create(LanguageMessages.class);

	@Key("english")
	public String english();

	@Key("french")
	public String french();

	@Key("portuguese")
	public String portuguese();

	@Key("spanish")
	public String spanish();

}
