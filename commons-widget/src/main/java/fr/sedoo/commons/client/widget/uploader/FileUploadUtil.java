package fr.sedoo.commons.client.widget.uploader;

import com.google.gwt.core.client.GWT;

public class FileUploadUtil {

	public static String getImageUrlFromId(Long id) {
		return GWT.getModuleBaseURL() + "rest/image/storedById/" + id;
	}
}
