package fr.sedoo.commons.shared.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import fr.sedoo.commons.client.widget.editing.ValidationAlert;

@Entity
@Table(name = "MESSAGE")
public class Message implements HasIdentifier, Serializable, New {

	public final static String EXTERNAL_IMAGE = "EXTERNAL_IMAGE";
	public final static String INTERNAL_IMAGE = "INTERNAL_IMAGE";
	public final static String NO_IMAGE = "NO_IMAGE";

	@Column(name = "UUID")
	private String uuid;

	@Column(name = "LANGUAGE")
	private String language;

	@Column(name = "AUTHOR")
	private String author;

	@Column(name = "DATE")
	private Date date;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "TITLE", columnDefinition = "TEXT")
	private String title;

	@Column(name = "CONTENT", columnDefinition = "TEXT")
	private String content;

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	@Override
	public String getIdentifier() {
		return "" + getId();
	}

	@Override
	public List<ValidationAlert> validate() {
		return new ArrayList<ValidationAlert>();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getExternaImagelUrl() {
		return externaImagelUrl;
	}

	public void setExternaImagelUrl(String externaImagelUrl) {
		this.externaImagelUrl = externaImagelUrl;
	}

	public String getIllustrationImageType() {
		return illustrationImageType;
	}

	public void setIllustrationImageType(String illustrationImageType) {
		this.illustrationImageType = illustrationImageType;
	}

	@Column(name = "EXTERNAL_IMAGE_URL", nullable = true)
	private String externaImagelUrl;

	@Column(name = "ILLUSTRATION_IMAGE_TYPE", nullable = true)
	private String illustrationImageType;

}
