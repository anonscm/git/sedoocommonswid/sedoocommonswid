package fr.sedoo.commons.client.widget.dialog;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HasAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.widget.DialogBoxTools;

public class OkCancelDialog extends DialogBox implements ClickHandler 
{
	private OkCancelContent content;
	private Button okButton;
	private Button cancelButton;
	
	public OkCancelDialog(String title, OkCancelContent content)
	{
		super();
		this.content = content;
		setText(title);
		getElement().getStyle().setProperty("zIndex", ""+DialogBoxTools.getHigherZIndex());
		VerticalPanel panel = new VerticalPanel();
		panel.add(content);
		if (content.getPreferredHeight() != null)
		{
			content.asWidget().setHeight(content.getPreferredHeight());
		}
		
		HorizontalPanel toolBar = new HorizontalPanel();
		toolBar.setSpacing(5);
		
		
		okButton = new Button(CommonMessages.INSTANCE.ok());
		cancelButton = new Button(CommonMessages.INSTANCE.cancel());
		
		okButton.addClickHandler(this);
		cancelButton.addClickHandler(this);
		
		toolBar.add(okButton);
		toolBar.add(cancelButton);
		
		HorizontalPanel centeringPanel = new HorizontalPanel();
		centeringPanel.setWidth("100%");
		centeringPanel.setHorizontalAlignment(HasAlignment.ALIGN_CENTER);
		centeringPanel.add(toolBar);
		
		panel.add(centeringPanel);
		setWidget(panel);
		content.setDialogBox(this);
		setGlassEnabled(true);
		setAutoHideEnabled(true);
		center();
	}

	@Override
	public void onClick(ClickEvent event) {
		if (event.getSource() == okButton)
		{
			content.okClicked();
		}
		else if (event.getSource() == cancelButton)
		{
			content.cancelClicked();
		}
	}
}


