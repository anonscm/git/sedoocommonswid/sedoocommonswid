package fr.sedoo.commons.client.widget.table;

import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.core.client.GWT;
import com.google.gwt.safehtml.client.SafeHtmlTemplates;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;

interface RenderingTemplate extends SafeHtmlTemplates {
	@Template("<img src=\"{0}\" width=\"{1}\" class=\"{2}\" />")
	SafeHtml img(String url, String width, String className);
}

public class ImageRenderer {

	private String width;
	private String className;

	private static RenderingTemplate template;

	ImageRenderer(String width, String className) {
		if (template == null) {
			template = GWT.create(RenderingTemplate.class);
		}
		this.width = width;
		this.className = className;
	}

	public void render(Context context, String url, SafeHtmlBuilder sb) {
		if (url != null) {
			// The template will sanitize the URI.
			sb.append(template.img(url, width, className));
		}
	}
}
