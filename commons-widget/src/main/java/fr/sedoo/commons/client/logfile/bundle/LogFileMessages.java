package fr.sedoo.commons.client.logfile.bundle;

import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.LocalizableResource.DefaultLocale;
import com.google.gwt.i18n.client.LocalizableResource.Generate;
import com.google.gwt.i18n.client.Messages;

@Generate(format = { "com.google.gwt.i18n.rebind.format.PropertiesFormat" }, fileName = "Message", locales = { "en, fr" })
@DefaultLocale("en")
public interface LogFileMessages extends Messages {

	public static final LogFileMessages INSTANCE = GWT.create(LogFileMessages.class);

	@Key("logDisplay")
	public String logDisplay();

	@Key("fileName")
	public String fileName();

}
