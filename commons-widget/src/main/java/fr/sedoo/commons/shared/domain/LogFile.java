package fr.sedoo.commons.shared.domain;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.user.client.rpc.IsSerializable;

import fr.sedoo.commons.client.widget.editing.ValidationAlert;

public class LogFile implements HasIdentifier, IsSerializable {

	private String uuid;
	private String name;

	@Override
	public String getIdentifier() {
		return uuid;
	}

	@Override
	public List<ValidationAlert> validate() {
		return new ArrayList<ValidationAlert>();
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getUuid() {
		return uuid;
	}

}
