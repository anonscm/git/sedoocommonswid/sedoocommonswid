package fr.sedoo.commons.client.service;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.rpc.AsyncCallback;

import fr.sedoo.commons.client.event.ActionStartEvent;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.widget.DialogBoxTools;

public abstract class DefaultAbstractCallBack<T> implements AsyncCallback<T>{

	private ActionStartEvent event;
	private EventBus eventBus;
	
	public DefaultAbstractCallBack(ActionStartEvent event, EventBus eventBus) 
	{
		this.event = event;
		this.eventBus = eventBus;
	}
	
	@Override
	public void onFailure(Throwable caught) {
		eventBus.fireEvent(event.getEndingEvent());
		DialogBoxTools.modalAlert(CommonMessages.INSTANCE.error(), CommonMessages.INSTANCE.anErrorHasOccurred()+" : "+caught.getMessage());		
	}
	
	@Override
	public void onSuccess(T result) {
		eventBus.fireEvent(event.getEndingEvent());		
	}

	
}
