package fr.sedoo.commons.client.mvp.activity;

import com.google.gwt.place.shared.Place;

/**
 * Activité contenant une référence vers sa place
 * @author francois
 *
 */
public interface PlaceActivity {

	Place getPlace();
}
