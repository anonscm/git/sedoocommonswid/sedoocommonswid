package fr.sedoo.commons.client.widget.table;

import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.safehtml.client.SafeHtmlTemplates;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.safehtml.shared.SafeUri;
import com.google.gwt.safehtml.shared.UriUtils;

import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.shared.domain.IsUrl;

public class AnchorCell extends AbstractCell<IsUrl> {
	private static TemplateAvecDescription templateAvecDescription;
	private static TemplateSansDescription templateSansDescription;

	/*
	 * Template utilisé pour les liens ayant une description remplie
	 */

	interface TemplateAvecDescription extends SafeHtmlTemplates {
		@Template("- <a href=\"{0}\" target=\"_blank\">{1}</a> ({2})")
		SafeHtml hyperText(SafeUri link, String text, SafeHtml link2);
	}

	/*
	 * Template utilisé pour les liens n'ayant pas une description remplie
	 */

	interface TemplateSansDescription extends SafeHtmlTemplates {
		@Template("- <a href=\"{0}\" target=\"_blank\">{1}</a>")
		SafeHtml hyperText(SafeUri link, SafeHtml link2);
	}

	protected boolean showUrlWhenDescription = true;

	public AnchorCell(boolean showUrlWhenDescription) {
		super();
		this.showUrlWhenDescription = showUrlWhenDescription;
		if (templateAvecDescription == null) {
			templateAvecDescription = GWT.create(TemplateAvecDescription.class);
		}
		if (templateSansDescription == null) {
			templateSansDescription = GWT.create(TemplateSansDescription.class);
		}
	}

	@Override
	public void render(com.google.gwt.cell.client.Cell.Context context, IsUrl s, SafeHtmlBuilder sb) {
		if (StringUtil.isNotEmpty(s.getLabel())) {
			if (showUrlWhenDescription) {
				sb.append(templateAvecDescription.hyperText(UriUtils.fromString(s.getLink()), s.getLabel(), SafeHtmlUtils.fromString(s.getLink())));
			} else {
				sb.append(templateSansDescription.hyperText(UriUtils.fromString(s.getLink()), SafeHtmlUtils.fromString(s.getLabel())));
			}
		} else {
			sb.append(templateSansDescription.hyperText(UriUtils.fromString(s.getLink()), SafeHtmlUtils.fromString(s.getLink())));
		}
	}

}