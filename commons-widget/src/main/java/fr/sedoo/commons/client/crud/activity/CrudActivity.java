package fr.sedoo.commons.client.crud.activity;

import com.google.gwt.place.shared.Place;

import fr.sedoo.commons.client.crud.widget.CrudPresenter;
import fr.sedoo.commons.client.mvp.AuthenticatedClientFactory;
import fr.sedoo.commons.client.mvp.activity.AdministrationActivity;

public abstract class CrudActivity extends AdministrationActivity implements CrudPresenter{

	public CrudActivity(AuthenticatedClientFactory clientFactory, Place place) {
		super(clientFactory, place);
	}

}
