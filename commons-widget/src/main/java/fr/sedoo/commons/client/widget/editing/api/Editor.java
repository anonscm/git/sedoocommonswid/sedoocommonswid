package fr.sedoo.commons.client.widget.editing.api;

public interface Editor {

	void enableDisplayMode();
	void enableEditMode();
	
}
