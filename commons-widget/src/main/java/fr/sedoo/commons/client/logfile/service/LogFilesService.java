package fr.sedoo.commons.client.logfile.service;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import fr.sedoo.commons.client.util.ServiceException;
import fr.sedoo.commons.shared.domain.LogFile;

@RemoteServiceRelativePath("logFiles")

public interface LogFilesService extends RemoteService {
	ArrayList<LogFile> getLogFiles() throws ServiceException;
}
