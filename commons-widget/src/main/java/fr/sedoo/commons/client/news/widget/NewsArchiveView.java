package fr.sedoo.commons.client.news.widget;

import java.util.ArrayList;

import com.google.gwt.user.client.ui.IsWidget;

import fr.sedoo.commons.client.back.BackPresenter;
import fr.sedoo.commons.shared.domain.New;
import fr.sedoo.commons.shared.domain.message.TitledMessage;

public interface NewsArchiveView extends IsWidget
{
	void reset();
	void setNews(ArrayList<? extends New> news);
	void setBackPresenter(BackPresenter presenter);
	void setHits(int hits);
	void setNewsByRange(int i, ArrayList<TitledMessage> messages);
	void setNewsListPresenter(NewsListPresenter presenter);
}
