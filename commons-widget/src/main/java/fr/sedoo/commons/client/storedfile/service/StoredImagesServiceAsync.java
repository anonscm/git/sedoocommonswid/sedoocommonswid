package fr.sedoo.commons.client.storedfile.service;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;

import fr.sedoo.commons.shared.domain.misc.ImageUrl;

public interface StoredImagesServiceAsync {

	void deleteStoredImageByContainerId(String containerId, AsyncCallback<Void> callback);

	void deleteStoredImageById(Long id, AsyncCallback<Void> callback);

	void getStoredImagesByContainerId(String containerId, AsyncCallback<ArrayList<ImageUrl>> callback);

}
