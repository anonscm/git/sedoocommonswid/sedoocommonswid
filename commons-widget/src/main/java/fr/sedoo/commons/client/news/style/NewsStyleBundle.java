package fr.sedoo.commons.client.news.style;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.CssResource.NotStrict;

public interface NewsStyleBundle extends ClientBundle {

	@NotStrict
	@Source("newsStyle.css")
	NewsStyleCss sedooStyle();

	public static final NewsStyleBundle INSTANCE = GWT.create(NewsStyleBundle.class);

}