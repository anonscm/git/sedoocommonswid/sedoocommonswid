package fr.sedoo.commons.client.news.mvp;

import fr.sedoo.commons.client.mvp.AuthenticatedClientFactory;
import fr.sedoo.commons.client.news.ui.MessageEditView;
import fr.sedoo.commons.client.news.ui.MessageManageView;

public interface MessageClientFactory extends AuthenticatedClientFactory {

	MessageManageView getMessageManageView();
	MessageEditView getMessageEditView();

}
