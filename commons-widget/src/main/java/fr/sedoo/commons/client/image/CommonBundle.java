package fr.sedoo.commons.client.image;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.resources.client.CssResource.NotStrict;
import com.google.gwt.resources.client.ImageResource;

import fr.sedoo.commons.client.style.CommonStyleCss;

public interface CommonBundle extends ClientBundle {

	@NotStrict
	@Source("common.css")
	CssResource css();

	@NotStrict
	@Source("commonStyle.css")
	CommonStyleCss commonStyle();

	@Source("minimize.png")
	ImageResource minimize();

	@Source("help.png")
	ImageResource help();

	@Source("print.png")
	ImageResource print();

	@Source("maximize.png")
	ImageResource maximize();

	@Source("ajax-loader.gif")
	ImageResource loading();

	@Source("puce_fleche_bleu.png")
	ImageResource menuDot();

	@Source("puce_fleche_bleu_verticale.png")
	ImageResource verticalMenuDot();

	@Source("upArrow.png")
	ImageResource upArrow();

	@Source("downArrow.png")
	ImageResource downArrow();

	@Source("flag-en.png")
	ImageResource english();

	@Source("flag-fr.png")
	ImageResource french();

	@Source("ok.png")
	ImageResource ok();

	@Source("ko.png")
	ImageResource ko();

	@Source("zoom.png")
	ImageResource zoom();

	@Source("download.png")
	ImageResource download();

	@Source("picture.png")
	ImageResource picture();

	@Source("illustration.png")
	ImageResource illustration();

	public static final CommonBundle INSTANCE = GWT.create(CommonBundle.class);

}