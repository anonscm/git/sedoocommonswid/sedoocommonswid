package fr.sedoo.commons.client.news.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

public class NewDisplayPlace extends Place
{
	public static NewDisplayPlace instance;
	
	public String uuid;
	
	public NewDisplayPlace()
	{
	}
	
	public NewDisplayPlace(String uuid)
	{
		this.uuid = uuid;
	}

	public static class Tokenizer implements PlaceTokenizer<NewDisplayPlace>
	{
		
		public NewDisplayPlace getPlace(String token) {
			if (instance == null)
			{
				instance = new NewDisplayPlace();
				instance.setUuid(token);
			}
			return instance;
		}

		public String getToken(NewDisplayPlace place) {
			return place.getUuid();
		}
	}
	
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	
	public String getUuid() {
		return uuid;
	}
}