package fr.sedoo.commons.client.widget.map.resource;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

public interface MapBundle extends ClientBundle
{
	public static final MapBundle INSTANCE = GWT.create(MapBundle.class);
	
	@Source("drawRectangularAreaButton.png")
	ImageResource drawRectangularAreaButton();
	
	@Source("eraseRectangularAreaButton.png")
	ImageResource eraseRectangularAreaButton();
	
	@Source("drawMarkerButton.png")
	ImageResource drawMarkerButton();
	
	@Source("eraseMarkerButton.png")
	ImageResource eraseMarkerButton();
	
	@Source("dragMapButton.png")
	ImageResource dragMapButton();
}