package fr.sedoo.commons.client.back;

import fr.sedoo.commons.client.event.BackEvent;
import fr.sedoo.commons.client.mvp.BasicClientFactory;

public class BackUtils {
	
	public static void goBack(BasicClientFactory factory)
	{
		factory.getEventBus().fireEvent(new BackEvent());
	}

}
