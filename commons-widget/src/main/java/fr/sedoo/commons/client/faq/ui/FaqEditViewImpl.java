package fr.sedoo.commons.client.faq.ui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.dom.client.NodeList;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TabPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import fr.sedoo.commons.client.ckeditor.CKEditor;
import fr.sedoo.commons.client.ckeditor.DefaultCKEditorConfig;
import fr.sedoo.commons.client.component.AbstractView;
import fr.sedoo.commons.client.faq.bundle.FaqMessages;
import fr.sedoo.commons.client.image.CommonBundle;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.util.LocaleUtil;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.client.widget.api.HtmlTextEditor;
import fr.sedoo.commons.client.widget.uploader.ImagePanelContent;
import fr.sedoo.commons.shared.domain.faq.QuestionAndAnswer;
import fr.sedoo.commons.shared.domain.misc.ImageUrl;

public class FaqEditViewImpl extends AbstractView implements FaqEditView {

	private final static int QUESTION_LENGTH = 120;
	private String uuid;
	
	boolean tabActivated = false;
	boolean scheduledEnabled = false;
	private Integer selectedTab;
	
	private boolean resizeNeeded;
	private int maxLangagesTab;

	private static FaqEditViewImplUiBinder uiBinder = GWT.create(FaqEditViewImplUiBinder.class);

	private ImagePanelContent imagePanelContent = new ImagePanelContent();

	interface FaqEditViewImplUiBinder extends UiBinder<Widget, FaqEditViewImpl> {
	}

	HashMap<String, TextBox> questionsEditors = new HashMap<String, TextBox>();
	HashMap<String, HtmlTextEditor> answersEditors = new HashMap<String, HtmlTextEditor>();

	@Override
	public void setUuid(String uuid) {
		this.uuid = uuid;
		imagePanelContent.setUuid(uuid);
	}

	@UiField
	VerticalPanel mainPanel;

	Presenter presenter;
	private List<String> languages = new ArrayList<String>();

	private String displayLanguage;

	private Integer order;

	public FaqEditViewImpl(String displayLanguage) {
		super();
		this.displayLanguage = displayLanguage;
		// GWT.<GlobalBundle>create(GlobalBundle.class).css().ensureInjected();
		initWidget(uiBinder.createAndBindUi(this));
		applyCommonStyle();
	}

	@Override
	public void setContent(HashMap<String, QuestionAndAnswer> questions, List<String> languages, Integer order) {
		this.languages = languages;
		maxLangagesTab = languages.size()-1;
		this.order = order;
		reset();

		TabPanel tabPanel = new TabPanel();
		tabPanel.setWidth("100%");
		tabPanel.setHeight("100%");
		answersEditors.clear();
		questionsEditors.clear();
		Iterator<String> iterator = languages.iterator();
		while (iterator.hasNext()) {
			String language = (String) iterator.next();
			QuestionAndAnswer questionAndAnswer = questions.get(language);
			if (questionAndAnswer == null) {
				questionAndAnswer = new QuestionAndAnswer();
			}
			TextBox questionEditor = new TextBox();
			questionEditor.setVisibleLength(QUESTION_LENGTH);
			HtmlTextEditor answerEditor = new CKEditor(new DefaultCKEditorConfig(uuid, displayLanguage));
			// answerEditor.addToolBar(new
			// ResifRichTextToolbar(answerEditor.getRichTextArea()) );
			questionsEditors.put(language, questionEditor);
			questionEditor.setText(StringUtil.trimToEmpty(questionAndAnswer.getQuestion()));
			answersEditors.put(language, answerEditor);
			answerEditor.setHtml(StringUtil.trimToEmpty(questionAndAnswer.getAnswer()));

			VerticalPanel panel = new VerticalPanel();
			panel.setWidth("100%");
			panel.setHeight("100%");
			panel.add(new Label(FaqMessages.INSTANCE.question()));
			panel.add(questionEditor);
			panel.add(new Label(FaqMessages.INSTANCE.answer()));
			panel.add(answerEditor);

			tabPanel.add(panel, LocaleUtil.getLanguageFlag(language));

			mainPanel.add(tabPanel);
			
			tabPanel.addSelectionHandler(new SelectionHandler<Integer>() {

				@Override
				public void onSelection(SelectionEvent<Integer> event) {
					selectedTab = event.getSelectedItem();
					if (selectedTab <= maxLangagesTab)
					{
						resizeNeeded = true;
					}
					else
					{
						resizeNeeded = false;
					}
					Iterator<HtmlTextEditor> editorIterator = answersEditors.values().iterator();
					while (editorIterator.hasNext()) {
						HtmlTextEditor aux = (HtmlTextEditor) editorIterator.next();
						if (aux instanceof CKEditor)
						{
							((CKEditor)aux).onVisible();
						}

					}
				}
			});
			Scheduler.get().scheduleDeferred(new ScheduledCommand() {    
				@Override
				public void execute() {
					onResize();
				}
			});
			
		}
		Image picture = new Image(CommonBundle.INSTANCE.picture());
		picture.setTitle(CommonMessages.INSTANCE.images());
		tabPanel.add(imagePanelContent, picture);
		selectedTab = 0;
		tabPanel.selectTab(selectedTab);
		mainPanel.setHeight("100%");
	}

	@Override
	public void reset() {
		imagePanelContent.reset();
		mainPanel.clear();
	}

	@UiHandler("saveButton")
	void onSaveButtonClicked(ClickEvent event) {
		HashMap<String, QuestionAndAnswer> result = new HashMap<String, QuestionAndAnswer>();
		Iterator<String> iterator = languages.iterator();
		while (iterator.hasNext()) {
			String language = (String) iterator.next();
			String question = StringUtil.trimToEmpty(questionsEditors.get(language).getText());
			String answer = StringUtil.trimToEmpty(answersEditors.get(language).getHtml());
			result.put(language, new QuestionAndAnswer(question, answer, order));
		}

		// Iterator<Entry<String, RichTextEditor>> iterator =
		// editors.entrySet().iterator();
		// while (iterator.hasNext()) {
		// Entry<String, RichTextEditor> entry = (Entry<String, RichTextEditor>)
		// iterator.next();
		// result.put(entry.getKey(), entry.getValue().getHTML());
		// }
		presenter.save(result);
	}

	@UiHandler("backButton")
	void onBackButtonClicked(ClickEvent event) {
		presenter.back();
	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
		imagePanelContent.setPresenter(presenter);
	}

	@Override
	public void setImages(ArrayList<ImageUrl> images) {
		imagePanelContent.setImages(images);
	}

	@Override
	public void broadcastDeletion(String id) {
		imagePanelContent.broadcastDeletion(id);
	}
	
	@Override
	public void onResize() {
		super.onResize();
		resizeNeeded = true;
	}

	private void resizeCurrentEditor() {

		if (resizeNeeded)
		{
			if (selectedTab != null)
			{
				String index="";
				CKEditor current = (CKEditor) answersEditors.get(languages.get(selectedTab));
				NodeList<com.google.gwt.dom.client.Element> divs = current.getElement().getParentElement().getElementsByTagName("div");
				if (divs == null)
				{
					return;
				}
				else
				{
					int length = divs.getLength();
					for (int i=0; i<length;i++)
					{
						String id = divs.getItem(i).getId();
						if ((id.startsWith("cke_")) && (id.endsWith("_contents")))
						{
							String[] split = id.split("_");
							index=split[1];
							break;
						}

					}
				}
				int containerHeight = mainPanel.getElement().getParentElement().getClientHeight();
				Element top = DOM.getElementById("cke_"+index+"_top");
				Element contents = DOM.getElementById("cke_"+index+"_contents");
				Element bottom = DOM.getElementById("cke_"+index+"_bottom");

				if ((top != null) && (contents != null) && (bottom != null))
				{
					int topHeight = top.getClientHeight();
					int bottomHeight = bottom.getClientHeight();
					contents.getStyle().setHeight(containerHeight-topHeight-bottomHeight-115, Unit.PX);
					resizeNeeded = false;
				}
			}
		}
	}

	@Override
	public void activityStart() {
		scheduledEnabled = true;
		Scheduler.get().scheduleFixedDelay(new Scheduler.RepeatingCommand() {
			@Override
			public boolean execute() {
				if (scheduledEnabled)
				{
					resizeCurrentEditor();
					return scheduledEnabled;
				}
				else
				{
					return false;
				}
			}
		}, 1000);
	}

	@Override
	public void activityStop() {
		scheduledEnabled = false;
	}
}
