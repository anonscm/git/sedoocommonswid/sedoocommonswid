package fr.sedoo.commons.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface ActionEndEventHandler extends EventHandler {
    void onNotification(ActionEndEvent event);
}
