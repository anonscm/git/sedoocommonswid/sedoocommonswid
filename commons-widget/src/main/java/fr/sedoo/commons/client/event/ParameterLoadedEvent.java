package fr.sedoo.commons.client.event;

import java.util.HashMap;

import com.google.gwt.event.shared.GwtEvent;

import fr.sedoo.commons.client.parameter.ParameterValue;

public class ParameterLoadedEvent extends GwtEvent<ParameterLoadedEventHandler> {

	HashMap<String, ParameterValue> parameters;

	public static final Type<ParameterLoadedEventHandler> TYPE = new Type<ParameterLoadedEventHandler>();

	public ParameterLoadedEvent(HashMap<String, ParameterValue> parameters) {
		this.parameters = parameters;
	}

	@Override
	protected void dispatch(ParameterLoadedEventHandler handler) {
		handler.onNotification(this);
	}

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<ParameterLoadedEventHandler> getAssociatedType() {
		return TYPE;
	}

	public HashMap<String, ParameterValue> getParameters() {
		return parameters;
	}

}