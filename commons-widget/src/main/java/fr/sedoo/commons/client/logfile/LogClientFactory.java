package fr.sedoo.commons.client.logfile;

import fr.sedoo.commons.client.logfile.ui.LogFileDisplayView;

public interface LogClientFactory {

	LogFileDisplayView getLogDisplayView();

}
