package fr.sedoo.commons.client.faq.ui.list;

import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.client.ui.Image;

import fr.sedoo.commons.client.image.CommonBundle;
import fr.sedoo.commons.client.util.ElementUtil;
import fr.sedoo.commons.client.util.UuidUtil;
import fr.sedoo.commons.shared.domain.faq.QuestionAndAnswer;

public class FaqRenderer {
	private final static String QUESTION_PREFIX = "QUESTION_";
	private final static String OPENED_IMG_PREFIX = "OPENEDIMG_";
	private final static String CLOSED_IMG_PREFIX = "CLOSEDIMG_";
	private final static String ANSWER_PREFIX = "ANSWER_";

	public static void render(Context context, QuestionAndAnswer value, SafeHtmlBuilder sb) {
		if (value == null) {
			return;
		}

		String uuid = UuidUtil.uuid();
		String questionId = QUESTION_PREFIX + uuid;
		String openedImageId = OPENED_IMG_PREFIX + uuid;
		String closedImageId = CLOSED_IMG_PREFIX + uuid;
		String answerId = ANSWER_PREFIX + uuid;
		Image closedDot = new Image(CommonBundle.INSTANCE.menuDot());
		closedDot.getElement().setId(closedImageId);
		closedDot.getElement().getStyle().setProperty("cursor", "pointer");
		Image openedDot = new Image(CommonBundle.INSTANCE.verticalMenuDot());
		openedDot.getElement().setId(openedImageId);
		ElementUtil.hide(openedDot);
		openedDot.getElement().getStyle().setProperty("cursor", "pointer");
		sb.appendHtmlConstant("<div style=\"width:100%;border-bottom: 1px solid #EEEEEE;text-align:justify;cursor:default;\" class=\"localCellList\">");
		sb.appendHtmlConstant("<div>" + closedDot.toString()+openedDot.toString() + "<span style=\"margin-left:5px;cursor:pointer;font-weight:bold\" id=\"" + questionId + "\">" + value.getQuestion() + "</span></div>");
		sb.appendHtmlConstant("<div  id=\"" + answerId + "\" style=\"display:none\">");
		sb.appendHtmlConstant(value.getAnswer());
		sb.appendHtmlConstant("</div>");
		sb.appendHtmlConstant("</div>");
	}

	public static final String getAnwerIdFromSrcId(String questionId) {
		String[] split = questionId.split("_");
		return ANSWER_PREFIX + split[1];
	}
	
	public static final String getOpenIdFromSrcId(String questionId) {
		String[] split = questionId.split("_");
		return OPENED_IMG_PREFIX + split[1];
	}
	
	public static final String getCloseIdFromSrcId(String questionId) {
		String[] split = questionId.split("_");
		return CLOSED_IMG_PREFIX + split[1];
	}

	public static boolean isSrcId(String id) {
		if ((id.startsWith(CLOSED_IMG_PREFIX)) || (id.startsWith(OPENED_IMG_PREFIX)) || (id.startsWith(QUESTION_PREFIX))) {
			return true;
		} else {
			return false;
		}
	}

}
