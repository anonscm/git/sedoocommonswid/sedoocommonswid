package fr.sedoo.commons.server.logfile.dao;

import java.io.File;
import java.util.ArrayList;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;

import fr.sedoo.commons.server.file.FileService;
import fr.sedoo.commons.shared.domain.LogFile;

public class LogFileDefaultDAO implements LogFileDAO{

	private String logDirectory;
	
	@Override
	public ArrayList<LogFile> getLogFiles() throws Exception {
		ArrayList<LogFile> result = new ArrayList<LogFile>();
		if (!StringUtils.isEmpty(getLogDirectory()))
		{
			File directory = new File(logDirectory);
			File[] listFiles = directory.listFiles();
			for (int i = 0; i < listFiles.length; i++) 
			{
				File current = listFiles[i];
				if (current.isFile())
				{
					LogFile logFile = new LogFile();
					logFile.setName(current.getName());
					logFile.setUuid(FileService.addFile(current));
					result.add(logFile);
				}
			}
		}
		return result;
	}

	public String getLogDirectory() {
		return logDirectory;
	}

	public void setLogDirectory(String logDirectory) {
		this.logDirectory = logDirectory;
	}
	
	
	
	
}
