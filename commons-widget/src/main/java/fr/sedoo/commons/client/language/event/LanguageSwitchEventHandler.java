package fr.sedoo.commons.client.language.event;

import com.google.gwt.event.shared.EventHandler;

public interface LanguageSwitchEventHandler extends EventHandler {
    void onNotification(LanguageSwitchEvent event);
}

