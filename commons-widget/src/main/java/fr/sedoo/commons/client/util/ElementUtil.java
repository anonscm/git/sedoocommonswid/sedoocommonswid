package fr.sedoo.commons.client.util;

import com.google.gwt.dom.client.Element;
import com.google.gwt.user.client.ui.Widget;

public class ElementUtil {

	protected ElementUtil() {
	}

	public static void hide(Widget widget) {
		if (widget != null) {
			hide(widget.getElement());
		}
	}

	public static void hide(Element element) {
		if (element != null) {
			element.getStyle().setProperty("display", "none");
		}
	}

	public static void show(Widget widget) {
		if (widget != null) {
			show(widget.getElement());
		}
	}

	public static void show(Element element) {
		if (element != null) {
			element.getStyle().setProperty("display", "");
		}
	}

	public static boolean isVisible(Element element) {
		return (element.getStyle().getProperty("display").compareToIgnoreCase("none") != 0);
	}

	public static boolean isVisible(Widget widget) {
		return isVisible(widget.getElement());
	}
}
