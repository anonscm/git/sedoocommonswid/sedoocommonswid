package fr.sedoo.commons.client.style;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.CssResource.NotStrict;
import com.google.gwt.resources.client.ImageResource;

public interface SedooStyleBundle extends ClientBundle {

	@NotStrict
	@Source("sedooStyle.css")
	SedooStyleCss sedooStyle();

	@Source("menuDot.png")
	ImageResource menuDot();

	public static final SedooStyleBundle INSTANCE = GWT.create(SedooStyleBundle.class);

}