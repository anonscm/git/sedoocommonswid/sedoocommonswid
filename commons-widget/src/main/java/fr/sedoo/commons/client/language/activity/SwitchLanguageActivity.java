package fr.sedoo.commons.client.language.activity;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.i18n.client.LocaleInfo;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

import fr.sedoo.commons.client.event.ActionStartEvent;
import fr.sedoo.commons.client.language.bundle.SwitchLanguageMessages;
import fr.sedoo.commons.client.language.place.LanguageSwitchPlace;
import fr.sedoo.commons.client.language.ui.LanguageSwitchingView;
import fr.sedoo.commons.client.mvp.BasicClientFactory;
import fr.sedoo.commons.client.mvp.activity.DefaultActivity;
import fr.sedoo.commons.client.news.activity.MessageManagementEventConstant;

public class SwitchLanguageActivity extends DefaultActivity {

	private String locale;

	public SwitchLanguageActivity(LanguageSwitchPlace place, BasicClientFactory clientFactory) {
		super(clientFactory);
		setLocale(place.getLocale());
		// broadcastActivityTitle(Message.INSTANCE.languageSwitchingTitle());
	}

	@Override
	public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {

		final ActionStartEvent startEvent = new ActionStartEvent(SwitchLanguageMessages.INSTANCE.languageSwitchingProgressMessage(), MessageManagementEventConstant.LANGUAGE_SWITCHING_EVENT, true);
		clientFactory.getEventBus().fireEvent(startEvent);

		final LanguageSwitchingView switchingView = clientFactory.getLanguageSwitchingView();
		containerWidget.setWidget(switchingView.asWidget());

		Timer t = new Timer() {
			public void run() {
				Window.Location.assign( // or replace()
						Window.Location.createUrlBuilder().setParameter(LocaleInfo.getLocaleQueryParam(), getLocale()).buildString());
			}
		};

		t.schedule(2000);

	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}
}
