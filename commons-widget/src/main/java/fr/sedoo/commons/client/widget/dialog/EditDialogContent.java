package fr.sedoo.commons.client.widget.dialog;

import java.util.List;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DialogBox;

import fr.sedoo.commons.client.crud.widget.CrudConfirmCallBack;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.widget.DialogBoxTools;
import fr.sedoo.commons.client.widget.editing.ValidationAlert;
import fr.sedoo.commons.shared.domain.HasIdentifier;


public abstract class EditDialogContent extends Composite implements OkCancelContent{

	private DialogBox dialog;
	private CrudConfirmCallBack confirmCallback;
	
	public EditDialogContent(CrudConfirmCallBack confirmCallback) 
	{
		super();
		this.confirmCallback = confirmCallback;
	}

	public abstract HasIdentifier flush();
	
	@Override
	public void setDialogBox(DialogBox dialog) 
	{
		this.dialog = dialog;
	}
	
	public DialogBox getDialog() {
		return dialog;
	}
	
	@Override
	public void cancelClicked()
	{
		if (getDialog() != null)
		{
			getDialog().hide();
		}
	}
	
	@Override
	public void okClicked()
	{
		HasIdentifier hasIdentifier = flush();
		List<ValidationAlert> alerts = hasIdentifier.validate();
		if (alerts.isEmpty())
		{
			if (getDialog() != null)
			{
				getDialog().hide();
			}
			confirmCallback.confirm(true, hasIdentifier);
		}
		else
		{
			DialogBoxTools.popUpScrollable(CommonMessages.INSTANCE.error(), ValidationAlert.toHTML(alerts), DialogBoxTools.HTML_MODE);
		}
	}
	
}
