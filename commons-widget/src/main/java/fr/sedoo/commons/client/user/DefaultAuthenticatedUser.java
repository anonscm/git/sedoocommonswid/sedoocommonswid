package fr.sedoo.commons.client.user;

import java.io.Serializable;
import java.util.ArrayList;

public class DefaultAuthenticatedUser implements AuthenticatedUser, Serializable {

	private String name;
	private boolean admin;
	private ArrayList<String> roles;

	@Override
	public boolean isAdmin() {
		return admin;
	}

	@Override
	public String getName() {
		return name;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

	public void setName(String name) {
		this.name = name;
	}

	public DefaultAuthenticatedUser() {

	}

	public DefaultAuthenticatedUser(AuthenticatedUser authenticatedUser) {
		setName(authenticatedUser.getName());
		setAdmin(authenticatedUser.isAdmin());
	}

	@Override
	public ArrayList<String> getRoles() {
		return roles;
	}

	public void setRoles(ArrayList<String> roles) {
		this.roles = roles;
	}

}
