package fr.sedoo.commons.client.widget;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DecoratorPanel;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasAlignment;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.reveregroup.gwt.imagepreloader.FitImage;
import com.reveregroup.gwt.imagepreloader.FitImageLoadEvent;
import com.reveregroup.gwt.imagepreloader.FitImageLoadHandler;

import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.widget.dialog.DialogBoxContent;


public class DialogBoxTools 
{
	private final static int INITIAL_Z_ORDER=9999;
	private static int currentZOrder = INITIAL_Z_ORDER;
	public static final String HTML_MODE = "HTML_MODE";
	public static final String TEXT_MODE = "TEXT_MODE";
	
	public static void modalAlert(final String header, final String content) {
        final DialogBox box = new DialogBox();
        box.getElement().getStyle().setProperty("zIndex", ""+getHigherZIndex());
        final VerticalPanel panel = new VerticalPanel();
        box.setText(header);
        panel.add(new Label(content));
        final Button buttonClose = new Button("Close",new ClickHandler() {
            @Override
            public void onClick(final ClickEvent event) {
                box.hide();
            }
        });
        // few empty labels to make widget larger
        final Label emptyLabel = new Label("");
        emptyLabel.setSize("auto","25px");
        panel.add(emptyLabel);
        panel.add(emptyLabel);
        buttonClose.setWidth("90px");
        panel.add(buttonClose);
        panel.setCellHorizontalAlignment(buttonClose, HasAlignment.ALIGN_RIGHT);
        box.add(panel);
        box.center();
        box.show();
    }
	
public static DialogBox modalConfirm(final String header, final String content, final ConfirmCallBack callback) {
		
		
        final DialogBox box = new DialogBox();
        box.getElement().getStyle().setProperty("zIndex", ""+getHigherZIndex());
        final VerticalPanel panel = new VerticalPanel();
        box.setText(header);
        panel.add(new Label(content));
        final Button buttonYes = new Button(CommonMessages.INSTANCE.yes(),new ClickHandler() {
            @Override
            public void onClick(final ClickEvent event) {
                box.hide();
                callback.confirm(true);
            }
        });
        
        final Button buttonNo = new Button(CommonMessages.INSTANCE.no(),new ClickHandler() {
            @Override
            public void onClick(final ClickEvent event) {
                box.hide();
                callback.confirm(false);
            }
        });
        // few empty labels to make widget larger
        final Label emptyLabel = new Label("");
        emptyLabel.setSize("auto","25px");
        panel.add(emptyLabel);
        panel.add(emptyLabel);
        panel.setWidth("100%");
        buttonYes.setWidth("90px");
        buttonNo.setWidth("90px");
        HorizontalPanel horizontalPanel = new HorizontalPanel();
        horizontalPanel.setSpacing(5);
        horizontalPanel.setHorizontalAlignment(HasAlignment.ALIGN_CENTER);
        horizontalPanel.add(buttonYes);
        horizontalPanel.add(buttonNo);
        panel.add(horizontalPanel);
        box.setGlassEnabled(true);
	    box.setAutoHideEnabled(false);
        panel.setCellHorizontalAlignment(horizontalPanel, HasAlignment.ALIGN_CENTER);
        box.add(panel);
        return box;
    }
	

public static void popUp(String title, DialogBoxContent content) 
{
	popUp(title, content, null, null);
}

public static void popUp(String title, DialogBoxContent content, String width, String height) 
{
	final DialogBox dialogBox = new DialogBox();
	if ((width != null) && (height != null))
	{
		((Composite)content).setSize(width, height);
	}
    dialogBox.setText(title);
    dialogBox.getElement().getStyle().setProperty("zIndex", ""+getHigherZIndex());
    dialogBox.setWidget(content);
    content.setDialogBox(dialogBox);
    dialogBox.setGlassEnabled(true);
    dialogBox.setAutoHideEnabled(true);
    dialogBox.center();
    dialogBox.show();
}


public static void popUpScrollable(String title, String text, String mode) 
{
	String formattedText = text;
	if (mode.compareTo(TEXT_MODE)==0)
	{
		formattedText = SafeHtmlUtils.htmlEscape(text);
	}
	final DialogBox dialogBox = new DialogBox();
    dialogBox.setText(title);
    dialogBox.getElement().getStyle().setProperty("zIndex", ""+getHigherZIndex());
    
    VerticalPanel dialogContents = new VerticalPanel();
    dialogContents.setSpacing(4);
    HTML details = new HTML(formattedText);
    ScrollPanel scrollPanel = new ScrollPanel(details);
      scrollPanel.setSize("800px", "300px");
      DecoratorPanel decoratorPanel = new DecoratorPanel();
      decoratorPanel.add(scrollPanel);
      
      Button closeButton = new Button(
	            " Ok ", new ClickHandler() {
	              public void onClick(ClickEvent event) {
	                dialogBox.hide();
	              }
	            });
      
      dialogContents.add(decoratorPanel);
      dialogContents.add(closeButton);
      dialogContents.setCellHorizontalAlignment(closeButton, HasHorizontalAlignment.ALIGN_CENTER);
      
      dialogBox.setWidget(dialogContents);
    dialogBox.setGlassEnabled(true);
    dialogBox.setAutoHideEnabled(true);
    dialogBox.center();
    dialogBox.show();
	
	
}

/**
 * Renvoie un Z-index faisant que l'�l�ment s'affiche au dessus des �l�ments d�j� affich�s 
 */
public static int getHigherZIndex()
{
	currentZOrder++;
	return currentZOrder;
}
	
public static void popUpImage(String title, String url)
{
	final DialogBox dialogBox = new DialogBox();
    dialogBox.setText(title);
    dialogBox.getElement().getStyle().setProperty("zIndex", ""+getHigherZIndex());
    
    VerticalPanel dialogContents = new VerticalPanel();
    dialogContents.setSpacing(4);
    
      
      DecoratorPanel decoratorPanel = new DecoratorPanel();
      VerticalPanel content = new VerticalPanel();
      
      final HorizontalPanel loadingPanel = new HorizontalPanel();
      loadingPanel.setSpacing(5);
      loadingPanel.add(new Label(CommonMessages.INSTANCE.loading()));
      content.add(loadingPanel);
      final FitImage image = new FitImage();
      image.setVisible(false);
      content.add(image);
      loadingPanel.setVisible(true);
      
      image.addFitImageLoadHandler(new FitImageLoadHandler() {
          public void imageLoaded(FitImageLoadEvent event) {
              if (event.isLoadFailed())
              {
            	  dialogBox.hide();
            	  DialogBoxTools.modalAlert(CommonMessages.INSTANCE.error(), CommonMessages.INSTANCE.anErrorHasOccurred());
              }
              else
              {
            	  loadingPanel.setVisible(false);
            	  image.setVisible(true);
            	  dialogBox.center();
              }
          }
      });
      image.setUrl(url);
      image.setMaxSize(800, 400);
      decoratorPanel.add(content);
      
      Button closeButton = new Button(
	            " Ok ", new ClickHandler() {
	              public void onClick(ClickEvent event) {
	                dialogBox.hide();
	              }
	            });
      
      
      dialogContents.add(decoratorPanel);
      dialogContents.add(closeButton);
      dialogContents.setCellHorizontalAlignment(closeButton, HasHorizontalAlignment.ALIGN_CENTER);
      
      dialogBox.setWidget(dialogContents);
    dialogBox.setGlassEnabled(true);
    dialogBox.setAutoHideEnabled(true);
    dialogBox.center();
    dialogBox.show();
}
	
	

	
}