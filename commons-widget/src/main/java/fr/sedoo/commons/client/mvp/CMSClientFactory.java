package fr.sedoo.commons.client.mvp;

import java.util.HashMap;

public interface CMSClientFactory {
	
	HashMap<String, String> getScreenNames();

}
