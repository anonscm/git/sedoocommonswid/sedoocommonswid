package fr.sedoo.commons.client.news.widget;

import org.gwtbootstrap3.client.ui.Alert;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import fr.sedoo.commons.client.DefaultClientApplication;
import fr.sedoo.commons.client.back.BackButton;
import fr.sedoo.commons.client.back.BackPresenter;
import fr.sedoo.commons.client.component.AbstractView;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.news.bundle.NewsMessages;
import fr.sedoo.commons.client.print.Print;
import fr.sedoo.commons.client.print.PrintStyleProvider;
import fr.sedoo.commons.client.style.SedooStyleBundle;
import fr.sedoo.commons.client.util.DateUtil;
import fr.sedoo.commons.client.util.ElementUtil;
import fr.sedoo.commons.client.util.LocaleUtil;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.shared.domain.New;
import fr.sedoo.commons.shared.domain.message.TitledMessage;

public class NewDisplayViewImpl extends AbstractView implements NewDisplayView {

	private static WelcomeViewImplUiBinder uiBinder = GWT.create(WelcomeViewImplUiBinder.class);

	interface WelcomeViewImplUiBinder extends UiBinder<Widget, NewDisplayViewImpl> {
	}

	@UiField
	DivElement contentContainer;

	@UiField
	Label title;

	@UiField
	Label addedOrModified;

	@UiField
	BackButton backButton;

	@UiField
	Alert deleted;

	@UiField
	HorizontalPanel footerPanel;

	@UiField
	VerticalPanel topContainer;

	private PrintStyleProvider printStyleProvider;

	public NewDisplayViewImpl(PrintStyleProvider printStyleProvider) {
		super();
		this.printStyleProvider = printStyleProvider;
		SedooStyleBundle.INSTANCE.sedooStyle().ensureInjected();
		initWidget(uiBinder.createAndBindUi(this));
		applyCommonStyle();
		addedOrModified.setStyleName(SedooStyleBundle.INSTANCE.sedooStyle().newsAddedOrModified());
		backButton.addStyleName("btn-primary");
	}

	@Override
	public void display(New item) {
		if (item instanceof TitledMessage) {
			if (StringUtil.isEmpty(((TitledMessage) item).getTitle())) {
				ElementUtil.show(deleted);
			} else {
				contentContainer.setInnerHTML(((TitledMessage) item).getContent());
				title.setText(((TitledMessage) item).getTitle());
				String language = LocaleUtil.getClientLocaleLanguage(DefaultClientApplication.getClientFactory().getDefaultLanguage());
				addedOrModified.setText(NewsMessages.INSTANCE.addedOrModified() + " : " + DateUtil.getDateByLocale(item.getDate(), language) + " " + CommonMessages.INSTANCE.by() + " "
						+ StringUtil.trimToEmpty(((TitledMessage) item).getAuthor()));
				ElementUtil.show(footerPanel);
			}
		}
	}

	@Override
	public void reset() {
		contentContainer.setInnerHTML("");
		title.setText("");
		addedOrModified.setText("");
		ElementUtil.hide(deleted);
		ElementUtil.hide(footerPanel);
	}

	@Override
	public void setBackPresenter(BackPresenter presenter) {
		backButton.setPresenter(presenter);
	}

	@UiHandler("printImage")
	void onPrintImageClicked(ClickEvent event) {
		Print.it(getPrintStyleProvider().getPrintStyle(), topContainer);
	}

	public PrintStyleProvider getPrintStyleProvider() {
		return printStyleProvider;
	}

}
