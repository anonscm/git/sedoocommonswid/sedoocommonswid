package fr.sedoo.commons.client.widget;

import com.google.gwt.user.client.ui.IsWidget;

public interface PreferredHeightWidget extends IsWidget {

	double getPreferredHeight();
}
