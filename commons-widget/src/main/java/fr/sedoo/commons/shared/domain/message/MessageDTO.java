package fr.sedoo.commons.shared.domain.message;

import java.util.HashMap;

import com.google.gwt.user.client.rpc.IsSerializable;

public class MessageDTO implements IsSerializable {

	HashMap<String, TitledMessage> contents;
	String fileName;
	String illustrationImageType;
	String uuid;
	String url;

	public MessageDTO() {

	}

	public void setContents(HashMap<String, TitledMessage> contents) {
		this.contents = contents;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public void setIllustrationImageType(String illustrationImageType) {
		this.illustrationImageType = illustrationImageType;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public MessageDTO(HashMap<String, TitledMessage> contents, String fileName, String illustrationImageType, String uuid, String url) {
		super();
		this.contents = contents;
		this.fileName = fileName;
		this.illustrationImageType = illustrationImageType;
		this.uuid = uuid;
		this.url = url;
	}

	public HashMap<String, TitledMessage> getContents() {
		return contents;
	}

	public String getFileName() {
		return fileName;
	}

	public String getIllustrationImageType() {
		return illustrationImageType;
	}

	public String getUrl() {
		return url;
	}

	public String getUuid() {
		return uuid;
	}

}
