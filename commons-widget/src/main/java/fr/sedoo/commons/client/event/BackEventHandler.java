package fr.sedoo.commons.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface BackEventHandler extends EventHandler {
    void onNotification(BackEvent event);
}
