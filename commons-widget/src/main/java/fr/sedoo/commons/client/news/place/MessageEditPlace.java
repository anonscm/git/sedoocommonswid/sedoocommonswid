package fr.sedoo.commons.client.news.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

import fr.sedoo.commons.client.mvp.place.AuthenticatedPlace;

public class MessageEditPlace extends Place implements AuthenticatedPlace
{
	private String uuid="";

	public static MessageEditPlace instance;
	
	public static class Tokenizer implements PlaceTokenizer<MessageEditPlace>
	{
		@Override
		public String getToken(MessageEditPlace place)
		{
			return place.getUuid();
		}

		@Override
		public MessageEditPlace getPlace(String token)
		{
			if (instance == null)
			{
				instance = new MessageEditPlace();
			}
			instance.setUuid(token);
			return instance;
		}
	}

	public MessageEditPlace()
	{
		
	}
	
	public MessageEditPlace(String uuid)
	{
		this.uuid = uuid;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	
	
}
