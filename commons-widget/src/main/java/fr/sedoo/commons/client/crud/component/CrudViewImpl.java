package fr.sedoo.commons.client.crud.component;

import fr.sedoo.commons.client.component.AbstractView;
import fr.sedoo.commons.client.crud.widget.CrudPresenter;
import fr.sedoo.commons.client.crud.widget.CrudTable;
import fr.sedoo.commons.shared.domain.HasIdentifier;

public abstract class CrudViewImpl extends AbstractView implements CrudView {
	
	public void setCrudPresenter(CrudPresenter presenter)
	{
		getCrudTable().setPresenter(presenter);
	}
	
	public abstract CrudTable getCrudTable();
	
	@Override
	public void broadcastDeletion(HasIdentifier deletedItem) 
	{
		getCrudTable().removeRow(deletedItem);
	}
	
	@Override
	public void broadcastCreation(HasIdentifier addedItem) {
		getCrudTable().addRow(addedItem);
		
	}
	
	@Override
	public void broadcastEdition(HasIdentifier editedItem) {
		getCrudTable().updateRow(editedItem);
		
	}

}
