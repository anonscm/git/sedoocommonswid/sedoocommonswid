package fr.sedoo.commons.client.language.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Widget;

import fr.sedoo.commons.client.component.AbstractView;


public class LanguageSwitchingViewImpl extends AbstractView implements LanguageSwitchingView {

	private static LanguageSwitchingViewImplUiBinder uiBinder = GWT
			.create(LanguageSwitchingViewImplUiBinder.class);

	interface LanguageSwitchingViewImplUiBinder extends UiBinder<Widget, LanguageSwitchingViewImpl> {
	}

	public LanguageSwitchingViewImpl() {
		initWidget(uiBinder.createAndBindUi(this));
		applyCommonStyle();
	}

}
