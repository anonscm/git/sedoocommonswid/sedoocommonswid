package fr.sedoo.commons.client.util;

public class StringUtil {

	public static final String DEFAULT_TRUNCATION_SUFFIX = "[...]";

	public static String trimToEmpty(String value)
	{
		if (value == null)
		{
			return "";
		}
		else
		{
			return value.trim();
		}
	}
	
	/*
	 * Retourne la chaine tronquée à la longueur indiquée
	 * Si le suffixe n'est pas null il est ajouté à la fin de la chaine
	 */
	public static String truncate(String value, int length, String suffix)
	{
		if (value == null)
		{ 
			return "";
		}
		if (value.length()<=length)
		{
			return value;
		}
		else
		{
			String aux = "";
			if (suffix != null)
			{
				aux = suffix;
			}
			return value.substring(0, length)+aux;
		}
	}
	
	public static boolean isEmpty(String value)
	{
		if (value == null)
		{
			return true;
		}
		else
		{
			if (value.trim().length()==0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}
	
	public static boolean isNotEmpty(String value)
	{
		return !(isEmpty(value));
	}
	
}
