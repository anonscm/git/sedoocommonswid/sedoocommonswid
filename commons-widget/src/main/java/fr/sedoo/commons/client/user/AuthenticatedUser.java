package fr.sedoo.commons.client.user;

import java.util.ArrayList;

public interface AuthenticatedUser {

	boolean isAdmin();

	String getName();

	ArrayList<String> getRoles();
}
