package fr.sedoo.commons.client.widget.editing.impl;

import java.util.ArrayList;
import java.util.List;

import fr.sedoo.commons.client.widget.editing.ValidationAlert;
import fr.sedoo.commons.client.widget.editing.api.Edited;

public class DefaultEdited implements Edited {

	@Override
	public List<ValidationAlert> validate() 
	{
		return new ArrayList<ValidationAlert>();
	}

}
