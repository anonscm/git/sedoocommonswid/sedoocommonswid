package fr.sedoo.commons.client.crud.component;

import com.google.gwt.user.client.ui.IsWidget;

import fr.sedoo.commons.client.crud.widget.CrudPresenter;
import fr.sedoo.commons.shared.domain.HasIdentifier;

public interface CrudView extends IsWidget {

	void setCrudPresenter(CrudPresenter presenter);
	void broadcastDeletion(HasIdentifier hasIdentifier);
	void broadcastCreation(HasIdentifier hasIdentifier);
	void broadcastEdition(HasIdentifier hasIdentifier);
}
