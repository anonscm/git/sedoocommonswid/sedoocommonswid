package fr.sedoo.commons.client.news.activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

import fr.sedoo.commons.client.event.ActionEventConstants;
import fr.sedoo.commons.client.event.ActionStartEvent;
import fr.sedoo.commons.client.event.ActivityStartEvent;
import fr.sedoo.commons.client.event.BackEvent;
import fr.sedoo.commons.client.event.NotificationEvent;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.mvp.AuthenticatedClientFactory;
import fr.sedoo.commons.client.mvp.BasicClientFactory;
import fr.sedoo.commons.client.mvp.activity.AdministrationActivity;
import fr.sedoo.commons.client.news.event.MessageChangeEvent;
import fr.sedoo.commons.client.news.mvp.MessageClientFactory;
import fr.sedoo.commons.client.news.place.MessageEditPlace;
import fr.sedoo.commons.client.news.service.MessageService;
import fr.sedoo.commons.client.news.service.MessageServiceAsync;
import fr.sedoo.commons.client.news.ui.MessageEditView;
import fr.sedoo.commons.client.service.DefaultAbstractCallBack;
import fr.sedoo.commons.client.storedfile.service.StoredImagesService;
import fr.sedoo.commons.client.storedfile.service.StoredImagesServiceAsync;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.client.util.UuidUtil;
import fr.sedoo.commons.client.widget.uploader.FileUploadUtil;
import fr.sedoo.commons.shared.domain.Message;
import fr.sedoo.commons.shared.domain.message.MessageDTO;
import fr.sedoo.commons.shared.domain.message.TitledMessage;
import fr.sedoo.commons.shared.domain.misc.ImageUrl;

public abstract class MessageEditActivity extends AdministrationActivity implements MessageEditView.Presenter {

	private MessageEditView editView;
	private String uuid = "";
	private MessageEditPlace place;
	private boolean isNewMessage = true;

	private static final MessageServiceAsync MESSAGE_SERVICE = GWT.create(MessageService.class);
	private static final StoredImagesServiceAsync STORED_IMAGE_SERVICE = GWT.create(StoredImagesService.class);

	public MessageEditActivity(MessageEditPlace place, AuthenticatedClientFactory clientFactory) {
		super(clientFactory, place);
		this.place = place;
		uuid = place.getUuid();
		if (StringUtil.isEmpty(uuid)) {
			isNewMessage = true;
			uuid = UuidUtil.uuid();
		} else {
			isNewMessage = false;
		}
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	/**
	 * Invoked by the ActivityManager to start a new Activity
	 */
	@Override
	public void start(final AcceptsOneWidget containerWidget, EventBus eventBus) {
		if (isValidUser() == false) {
			goToLoginPlace();
			return;
		}
		editView = ((MessageClientFactory) clientFactory).getMessageEditView();
		editView.activityStart();
		editView.setPresenter(this);
		editView.reset();
		editView.setUuid(uuid);
		eventBus.fireEvent(new ActivityStartEvent(this));
		if (isNewMessage) {
			// Creation
			editView.setContent(new HashMap<String, TitledMessage>(), clientFactory.getLanguages(), "", Message.NO_IMAGE, "");
			containerWidget.setWidget(editView.asWidget());
		} else {
			ActionStartEvent e = new ActionStartEvent(CommonMessages.INSTANCE.loading(), ActionEventConstants.BASIC_LOADING_EVENT, true);
			eventBus.fireEvent(e);

			MESSAGE_SERVICE.getMessageContentByUuid(uuid, new DefaultAbstractCallBack<MessageDTO>(e, eventBus) {
				@Override
				public void onSuccess(MessageDTO result) {
					super.onSuccess(result);
					editView.setContent(result.getContents(), clientFactory.getLanguages(), result.getUrl(), result.getIllustrationImageType(), result.getFileName());
					containerWidget.setWidget(editView.asWidget());

					EventBus eventBus = clientFactory.getEventBus();
					ActionStartEvent e = new ActionStartEvent(CommonMessages.INSTANCE.loading(), ActionEventConstants.BASIC_LOADING_EVENT, true);
					eventBus.fireEvent(e);

					STORED_IMAGE_SERVICE.getStoredImagesByContainerId(uuid, new DefaultAbstractCallBack<ArrayList<ImageUrl>>(e, eventBus) {

						@Override
						public void onSuccess(ArrayList<ImageUrl> result) {
							super.onSuccess(result);
							Iterator<ImageUrl> imageIterator = result.iterator();
							while (imageIterator.hasNext()) {
								ImageUrl imageUrl = (ImageUrl) imageIterator.next();
								imageUrl.setLink(FileUploadUtil.getImageUrlFromId(imageUrl.getId()));

							}
							editView.setImages(result);
						}

					});
				}
			});
		}
	}

	@Override
	public void save(final HashMap<String, TitledMessage> content, String externaImagelUrl, String illustrationImageType, String fileName, boolean isTmpFile) {
		EventBus eventBus = clientFactory.getEventBus();
		ActionStartEvent e = new ActionStartEvent(CommonMessages.INSTANCE.saving(), ActionEventConstants.BASIC_SAVING_EVENT, true);
		eventBus.fireEvent(e);
		String author = ((AuthenticatedClientFactory) clientFactory).getUserManager().getUser().getName();
		MESSAGE_SERVICE.saveMessage(uuid, author, content, externaImagelUrl, illustrationImageType, fileName, isTmpFile, new DefaultAbstractCallBack<String>(e, eventBus) {
			@Override
			public void onSuccess(String uuid) {
				super.onSuccess(uuid);
				isNewMessage = false;
				setUuid(uuid);
				clientFactory.getEventBus().fireEvent(new NotificationEvent(CommonMessages.INSTANCE.savedElement()));
				editView.brodcastSaved(uuid);
				clientFactory.getEventBus().fireEvent(new MessageChangeEvent());
			}
		});
	}

	@Override
	public void back() {
		clientFactory.getEventBus().fireEvent(new BackEvent());
	}

	public MessageEditPlace getPlace() {
		return place;
	}

	@Override
	public BasicClientFactory getClientFactory() {
		return clientFactory;
	}

	@Override
	public void onStop() {
		editView.activityStop();
		super.onStop();
	}

	@Override
	public void deleteImageById(final String id) {
		EventBus eventBus = clientFactory.getEventBus();
		ActionStartEvent e = new ActionStartEvent(CommonMessages.INSTANCE.deleting(), ActionEventConstants.BASIC_DELETING_EVENT, true);
		eventBus.fireEvent(e);
		STORED_IMAGE_SERVICE.deleteStoredImageById(new Long(id), new DefaultAbstractCallBack<Void>(e, eventBus) {

			@Override
			public void onSuccess(Void result) {
				super.onSuccess(result);
				clientFactory.getEventBus().fireEvent(new NotificationEvent(CommonMessages.INSTANCE.deletedElement()));
				editView.broadcastDeletion(id);
			}

		});
	}
}