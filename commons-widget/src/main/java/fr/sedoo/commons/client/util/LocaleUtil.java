package fr.sedoo.commons.client.util;

import com.google.gwt.i18n.client.LocaleInfo;
import com.google.gwt.user.client.ui.Image;

import fr.sedoo.commons.client.image.CommonBundle;
import fr.sedoo.commons.client.message.LanguageMessages;
import fr.sedoo.commons.client.mvp.BasicClientFactory;

public class LocaleUtil {

	public static final String DEFAULT = "DEFAULT";
	public static final String FRENCH = "FR";
	public static final String ENGLISH = "EN";
	public static final String SPANISH = "ES";
	public static final String PORTUGUESE = "PT";

	/**
	 * Retourne la langue correspondant à la locale du client : FR, EN, ... La
	 * langue est en majuscules la valeur DEFAULT correspond à l'absence
	 * explicite de locale
	 * 
	 */
	public static String getClientLocaleLanguage() {
		String locale = LocaleInfo.getCurrentLocale().getLocaleName().toUpperCase();
		if (locale.compareTo(DEFAULT) == 0) {
			return DEFAULT;
		} else {
			return locale.substring(0, 2);
		}
	}

	public static String getCurrentLanguage(BasicClientFactory factory) {
		return getClientLocaleLanguage(factory.getDefaultLanguage());
	}

	/**
	 * Retourne la langue correspondant à la locale du client : FR, EN, ... La
	 * langue est en majuscules Si aucune valeur n'est indiquée la valeur
	 * defaultLanguage est retournée.
	 * 
	 */
	public static String getClientLocaleLanguage(String defaultLanguage) {
		String localeLanguage = getClientLocaleLanguage();
		if (localeLanguage.compareTo(DEFAULT) == 0) {
			return defaultLanguage;
		} else {
			return localeLanguage;
		}
	}

	public static Image getLanguageFlag(String language) {
		if (language.compareTo(ENGLISH) == 0) {
			Image image = new Image(CommonBundle.INSTANCE.english());
			image.setTitle(LanguageMessages.INSTANCE.english());
			return image;
		} else if (language.compareTo(FRENCH) == 0) {
			Image image = new Image(CommonBundle.INSTANCE.french());
			image.setTitle(LanguageMessages.INSTANCE.french());
			return image;
		} else {
			return null;
		}
	}
}
