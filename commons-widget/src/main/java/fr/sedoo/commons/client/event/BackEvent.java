package fr.sedoo.commons.client.event;

import com.google.gwt.event.shared.GwtEvent;

public class BackEvent extends GwtEvent<BackEventHandler>{

	
    public static final Type<BackEventHandler> TYPE = new Type<BackEventHandler>();

    public BackEvent()
    {
    }
    
	@Override
    protected void dispatch(BackEventHandler handler) {
            handler.onNotification(this);
    }

    @Override
    public com.google.gwt.event.shared.GwtEvent.Type<BackEventHandler> getAssociatedType() {
            return TYPE;
    }
    
}