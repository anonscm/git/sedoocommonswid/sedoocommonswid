package fr.sedoo.commons.client.component;

import com.google.gwt.user.client.ui.ResizeComposite;

import fr.sedoo.commons.client.image.CommonBundle;

public class AbstractStyledResizeComposite extends ResizeComposite{
	
	public AbstractStyledResizeComposite() {
		CommonBundle.INSTANCE.css().ensureInjected();
	}
	
	protected  void applyCommonStyle()
	{
		getElement().getStyle().setProperty("backgroundColor", "#ffffff");
	}

}
