package fr.sedoo.commons.client.print;

public interface PrintStyleProvider {
	
	String getPrintStyle();

}
