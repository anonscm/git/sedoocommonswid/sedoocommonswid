package fr.sedoo.commons.client.crud.widget;

import fr.sedoo.commons.shared.domain.HasIdentifier;


public class CreateConfirmCallBack extends CrudConfirmCallBack {

	private CrudPresenter presenter;

	public CreateConfirmCallBack(CrudPresenter presenter) 
	{
		this.presenter = presenter;
	}

	@Override
	public void confirm(boolean choice, HasIdentifier hasIdentifier) 
	{
		if (choice == true)
		{
			presenter.create(hasIdentifier);
		}
		
	}


}
