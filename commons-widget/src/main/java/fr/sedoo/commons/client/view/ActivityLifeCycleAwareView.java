package fr.sedoo.commons.client.view;

public interface ActivityLifeCycleAwareView {

	void activityStart();

	void activityStop();
}
