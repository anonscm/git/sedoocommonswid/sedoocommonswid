package fr.sedoo.commons.server.storedfile.service;

import java.util.ArrayList;
import java.util.Iterator;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import fr.sedoo.commons.client.storedfile.service.StoredImagesService;
import fr.sedoo.commons.client.util.ServiceException;
import fr.sedoo.commons.server.DefaultServerApplication;
import fr.sedoo.commons.server.storedfile.dao.StoredFileDAO;
import fr.sedoo.commons.shared.domain.StoredFile;
import fr.sedoo.commons.shared.domain.misc.ImageUrl;

public class StoredImagesServiceImpl extends RemoteServiceServlet implements StoredImagesService {

	private StoredFileDAO storedFileDAO;

	private void initDao() {
		if (storedFileDAO == null) {
			storedFileDAO = (StoredFileDAO) DefaultServerApplication.getSpringBeanFactory().getBeanByName(StoredFileDAO.BEAN_NAME);
		}
	}

	@Override
	public ArrayList<ImageUrl> getStoredImagesByContainerId(String containerId) throws ServiceException {
		// TODO Auto-generated method stub
		initDao();
		ArrayList<ImageUrl> result = new ArrayList<ImageUrl>();
		ArrayList<StoredFile> tmp = storedFileDAO.findFilesByAttacherUuid(containerId, false);
		Iterator<StoredFile> iterator = tmp.iterator();
		while (iterator.hasNext()) {
			StoredFile storedFile = (StoredFile) iterator.next();
			ImageUrl url = new ImageUrl();
			url.setId(storedFile.getId());
			result.add(url);
		}
		return result;
	}

	@Override
	public void deleteStoredImageById(Long id) throws ServiceException {
		initDao();
		storedFileDAO.deleteStoredFileById(id);
	}

	@Override
	public void deleteStoredImageByContainerId(String containerId) {
		initDao();
		storedFileDAO.deleteStoredFileByAttacherUuid(containerId);
	}
}
