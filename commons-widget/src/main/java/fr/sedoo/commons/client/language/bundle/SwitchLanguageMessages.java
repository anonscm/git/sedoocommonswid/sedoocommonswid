package fr.sedoo.commons.client.language.bundle;

import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.LocalizableResource.DefaultLocale;
import com.google.gwt.i18n.client.LocalizableResource.Generate;
import com.google.gwt.i18n.client.Messages;

@Generate(
format = { "com.google.gwt.i18n.rebind.format.PropertiesFormat" }, 
fileName = "Message", 
locales = {"en, fr"})
@DefaultLocale("en")
public interface SwitchLanguageMessages extends Messages {
	
	public static final SwitchLanguageMessages INSTANCE = GWT.create(SwitchLanguageMessages.class);
	
	@Key("languageSwitchingMessage")
	public String languageSwitchingMessage();
	
	@Key("languageSwitchingProgressMessage")
	public String languageSwitchingProgressMessage();
	
}
