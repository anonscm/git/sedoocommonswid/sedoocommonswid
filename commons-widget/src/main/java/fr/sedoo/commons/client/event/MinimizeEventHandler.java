package fr.sedoo.commons.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface MinimizeEventHandler extends EventHandler {
    void onNotification(MinimizeEvent event);
}
