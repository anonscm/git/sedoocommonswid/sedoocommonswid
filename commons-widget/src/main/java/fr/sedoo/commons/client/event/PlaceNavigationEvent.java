package fr.sedoo.commons.client.event;

import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.place.shared.Place;

public class PlaceNavigationEvent extends GwtEvent<PlaceNavigationEventHandler>{

    public static final Type<PlaceNavigationEventHandler> TYPE = new Type<PlaceNavigationEventHandler>();
    
    private Place place;
    
    public PlaceNavigationEvent(Place place)
    {
           this.place = place;
    }

    @Override
    protected void dispatch(PlaceNavigationEventHandler handler) {
            handler.onNotification(this);
    }

    @Override
    public com.google.gwt.event.shared.GwtEvent.Type<PlaceNavigationEventHandler> getAssociatedType() {
            return TYPE;
    }

	public Place getPlace() {
		return place;
	}

}