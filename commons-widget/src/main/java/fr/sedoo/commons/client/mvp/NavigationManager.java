package fr.sedoo.commons.client.mvp;

import com.google.gwt.place.shared.PlaceController;

import fr.sedoo.commons.client.event.PlaceNavigationEvent;
import fr.sedoo.commons.client.event.PlaceNavigationEventHandler;

public class NavigationManager implements PlaceNavigationEventHandler{

	private PlaceController controller;

	public NavigationManager(PlaceController controller) {
		this.controller = controller;
	}
	
	@Override
	public void onNotification(PlaceNavigationEvent event) {
		controller.goTo(event.getPlace());
	}
}
