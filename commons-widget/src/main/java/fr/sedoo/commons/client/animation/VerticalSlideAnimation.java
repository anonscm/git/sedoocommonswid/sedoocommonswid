package fr.sedoo.commons.client.animation;

import com.google.gwt.animation.client.Animation;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.Widget;

import fr.sedoo.commons.client.util.ElementUtil;

public class VerticalSlideAnimation extends Animation {
	private final Element element;
	private boolean opening;

	public VerticalSlideAnimation(Widget widget) {
		element = widget.getElement();
	}

	public VerticalSlideAnimation(Element element) {
		this.element = element;
	}

	@Override
	protected void onComplete() {
		if (!opening) {
			ElementUtil.hide(element);
			element.getStyle().setOpacity(0);
		} else {
			element.getStyle().setOpacity(1);
		}
		element.getStyle().setProperty("height", "auto");
	}

	@Override
	protected void onStart() {
		super.onStart();
		opening = !ElementUtil.isVisible(element);

		if (opening) {
			element.getStyle().setHeight(0, Unit.PX);
			ElementUtil.show(element);
			element.getStyle().setOpacity(0);
		} else {
			element.getStyle().setOpacity(0);
		}

	}

	@Override
	protected void onUpdate(double progress) {
		int scrollHeight = element.getScrollHeight();
		int height = (int) (progress * scrollHeight);
		if (!opening) {
			height = scrollHeight - height;
		}
		height = Math.max(height, 1);
		element.getStyle().setHeight(height, Unit.PX);
	}
}
