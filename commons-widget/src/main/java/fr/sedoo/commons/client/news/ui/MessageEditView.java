package fr.sedoo.commons.client.news.ui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.google.gwt.user.client.ui.IsWidget;

import fr.sedoo.commons.client.mvp.BasicClientFactory;
import fr.sedoo.commons.client.view.ActivityLifeCycleAwareView;
import fr.sedoo.commons.client.widget.uploader.FileUploadPresenter;
import fr.sedoo.commons.shared.domain.message.TitledMessage;
import fr.sedoo.commons.shared.domain.misc.ImageUrl;

public interface MessageEditView extends IsWidget, ActivityLifeCycleAwareView {

	void setContent(HashMap<String, TitledMessage> messages, List<String> languages, String externaImagelUrl, String illustrationImageType, String fileName);

	void reset();

	void setPresenter(Presenter presenter);

	void brodcastSaved(String uuid);

	public interface Presenter extends FileUploadPresenter {
		void save(HashMap<String, TitledMessage> result, String externaImagelUrl, String illustrationImageType, String fileName, boolean isTmpFile);

		void back();

		BasicClientFactory getClientFactory();
	}

	void setImages(ArrayList<ImageUrl> result);

	void setUuid(String uuid);

	void broadcastDeletion(String id);
}
