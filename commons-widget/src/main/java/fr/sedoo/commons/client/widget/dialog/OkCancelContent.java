package fr.sedoo.commons.client.widget.dialog;


public interface OkCancelContent extends DialogBoxContent{
	
	public void okClicked();
	
	public void cancelClicked();

}
