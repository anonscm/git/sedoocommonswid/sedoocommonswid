package fr.sedoo.commons.client.news.activity;

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

import fr.sedoo.commons.client.back.BackPresenter;
import fr.sedoo.commons.client.back.BackUtils;
import fr.sedoo.commons.client.event.ActionEventConstants;
import fr.sedoo.commons.client.event.ActionStartEvent;
import fr.sedoo.commons.client.event.ActivityStartEvent;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.news.mvp.NewsClientFactory;
import fr.sedoo.commons.client.news.place.NewDisplayPlace;
import fr.sedoo.commons.client.news.service.MessageService;
import fr.sedoo.commons.client.news.service.MessageServiceAsync;
import fr.sedoo.commons.client.news.widget.NewDisplayView;
import fr.sedoo.commons.client.service.DefaultAbstractCallBack;
import fr.sedoo.commons.client.util.LocaleUtil;
import fr.sedoo.commons.shared.domain.message.TitledMessage;

public class NewDisplayActivity extends AbstractActivity implements BackPresenter {

	private String uuid;
	private NewsClientFactory clientFactory;

	public final static MessageServiceAsync MESSAGE_SERVICE = GWT.create(MessageService.class);
	private NewDisplayPlace place;

	public NewDisplayActivity(NewDisplayPlace place, NewsClientFactory clientFactory) {
		this.place = place;
		this.clientFactory = clientFactory;
		uuid = place.getUuid();
	}

	public NewDisplayPlace getPlace() {
		return place;
	}

	private NewDisplayView newDisplayView;

	/**
	 * Invoked by the ActivityManager to start a new Activity
	 */
	@Override
	public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {

		newDisplayView = clientFactory.getNewDisplayView();
		newDisplayView.setBackPresenter(this);
		containerWidget.setWidget(newDisplayView.asWidget());
		newDisplayView.reset();
		eventBus.fireEvent(new ActivityStartEvent(this));
		ActionStartEvent e = new ActionStartEvent(CommonMessages.INSTANCE.loading(), ActionEventConstants.NEWS_LOADING_EVENT, true);
		eventBus.fireEvent(e);

		MESSAGE_SERVICE.getConsultMessageContentByUuid(uuid, LocaleUtil.getClientLocaleLanguage(clientFactory.getDefaultLanguage()), clientFactory.getLanguages(),
				new DefaultAbstractCallBack<TitledMessage>(e, eventBus) {
					@Override
					public void onSuccess(TitledMessage result) {
						super.onSuccess(result);
						newDisplayView.display(result);
					}
				});
	}

	@Override
	public void back() {
		BackUtils.goBack(clientFactory);
	}

}