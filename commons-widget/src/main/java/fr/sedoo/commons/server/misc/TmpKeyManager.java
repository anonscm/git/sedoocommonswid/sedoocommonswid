package fr.sedoo.commons.server.misc;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

public class TmpKeyManager<T> {

	private  HashMap<String, T> map = new HashMap<String, T>();


	/**
	 * We delete entries uploaded more than two hours ago.
	 */
	private  void deleteOldEntries() {
		String currentPrefix = getCurrentHourPrefix();
		String previousPrefix = getPreviousHourPrefix();
		ArrayList<String> keyToDelete = new ArrayList<String>();
		Iterator<Entry<String, T>> mapIterator = map.entrySet().iterator();
		while (mapIterator.hasNext()) {
			Map.Entry<java.lang.String, T> entry = (Map.Entry<java.lang.String, T>) mapIterator.next();
			if (!((entry.getKey().contains(currentPrefix)) || (entry.getKey().contains(previousPrefix)))) {
				keyToDelete.add(entry.getKey());
			}
		}

		Iterator<String> iterator = keyToDelete.iterator();
		while (iterator.hasNext()) {
			String key = iterator.next();
			map.remove(key);
		}
	}

	private static String getCurrentHourPrefix() {
		Calendar calendar = GregorianCalendar.getInstance();
		int hour = calendar.get(Calendar.HOUR_OF_DAY);
		return hour + "_";
	}

	private static String getPreviousHourPrefix() {
		Calendar calendar = GregorianCalendar.getInstance();
		calendar.add(Calendar.HOUR_OF_DAY, -1);
		int hour = calendar.get(Calendar.HOUR_OF_DAY);
		return hour + "_";
	}

	public  T getItemFromKey(String key) {
		deleteOldEntries();
		return map.get(key);
	}
	
	public  String add(T item)
	{
		deleteOldEntries();
		String newKey = getCurrentHourPrefix()+UUID.randomUUID();
		map.put(newKey, item);
		return newKey;
	}
}
