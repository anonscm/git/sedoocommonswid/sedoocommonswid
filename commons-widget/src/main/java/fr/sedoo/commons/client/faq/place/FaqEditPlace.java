package fr.sedoo.commons.client.faq.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

import fr.sedoo.commons.client.mvp.place.AuthenticatedPlace;

public class FaqEditPlace extends Place implements AuthenticatedPlace {
	private String uuid = "";
	private Integer order = 0;

	public static FaqEditPlace instance;

	public static class Tokenizer implements PlaceTokenizer<FaqEditPlace> {
		@Override
		public String getToken(FaqEditPlace place) {
			return place.getUuid();
		}

		@Override
		public FaqEditPlace getPlace(String token) {
			if (instance == null) {
				instance = new FaqEditPlace();
			}
			instance.setUuid(token);
			return instance;
		}
	}

	public FaqEditPlace() {

	}

	public FaqEditPlace(Integer order) {
		this.order = order;
	}

	public FaqEditPlace(String uuid) {
		this.uuid = uuid;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public Integer getOrder() {
		return order;
	}

}
