package fr.sedoo.commons.shared.domain;

import java.util.List;

import com.google.gwt.user.client.rpc.IsSerializable;
import com.google.gwt.view.client.ProvidesKey;

import fr.sedoo.commons.client.widget.editing.ValidationAlert;

public abstract class AbstractDTO implements IsSerializable {
	public abstract String getHash();

	public abstract List<ValidationAlert> validate();

	public static final ProvidesKey<HasIdentifier> KEY_PROVIDER = new ProvidesKey<HasIdentifier>() {
		@Override
		public Object getKey(HasIdentifier item) {
			return item == null ? null : item.getIdentifier();
		}
	};

}
