package fr.sedoo.commons.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface PlaceNavigationEventHandler extends EventHandler {
    void onNotification(PlaceNavigationEvent event);
}
