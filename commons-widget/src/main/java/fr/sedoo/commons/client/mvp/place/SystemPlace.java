package fr.sedoo.commons.client.mvp.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

public class SystemPlace extends Place
{
	public static SystemPlace instance;
	
	public SystemPlace()
	{
	}

	public static class Tokenizer implements PlaceTokenizer<SystemPlace>
	{
		
		public SystemPlace getPlace(String token) {
			if (instance == null)
			{
				instance = new SystemPlace();
			}
			return instance;
		}

		public String getToken(SystemPlace place) {
			return "";
		}

		
	}
}