package fr.sedoo.commons.client.user;


public interface UserManager {

	AuthenticatedUser getUser();
	void setUser(AuthenticatedUser user);
	void clearCurrentUser();

}
