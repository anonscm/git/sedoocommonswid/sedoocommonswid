package fr.sedoo.commons.client.news.bundle;

import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.LocalizableResource.DefaultLocale;
import com.google.gwt.i18n.client.LocalizableResource.Generate;
import com.google.gwt.i18n.client.Messages;

@Generate(format = { "com.google.gwt.i18n.rebind.format.PropertiesFormat" }, fileName = "Message", locales = { "en, fr" })
@DefaultLocale("en")
public interface NewsMessages extends Messages {

	public static final NewsMessages INSTANCE = GWT.create(NewsMessages.class);

	@Key("author")
	public String author();

	@Key("title")
	public String title();

	@Key("illustrationImage")
	public String illustrationImage();

	@Key("noFileDownloaded")
	public String noFileDownloaded();

	@Key("chooseFile")
	public String chooseFile();

	@Key("dropFile")
	public String dropFile();

	@Key("content")
	public String content();

	@Key("header")
	public String header();

	@Key("editDialogTitle")
	public String editDialogTitle();

	@Key("createDialogTitle")
	public String createDialogTitle();

	@Key("addItemText")
	public String addItemText();

	@Key("addedOrModified")
	public String addedOrModified();

	@Key("deleted")
	public String deleted();

	@Key("url")
	public String url();

	@Key("file")
	public String file();

	@Key("illustrationImageTooltip")
	public String illustrationImageTooltip();

	@Key("newsArchiveTitle")
	public String newsArchiveTitle();

	@Key("newsDisplayTitle")
	public String newsDisplayTitle();

	@Key("frontpage")
	public String frontPage();
}
