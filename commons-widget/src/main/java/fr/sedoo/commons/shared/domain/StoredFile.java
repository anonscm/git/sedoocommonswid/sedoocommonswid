package fr.sedoo.commons.shared.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

@Entity
@Table(name = "STORED_FILE")
public class StoredFile {
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "NAME")
	private String name;

	@Lob
	@Column(name = "CONTENT")
	@Type(type = "org.hibernate.type.PrimitiveByteArrayBlobType")
	protected byte[] content;

	@Column(name = "ATTACHER_UUID")
	private String attacherUuid;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public byte[] getContent() {
		return content;
	}

	public void setContent(byte[] content) {
		this.content = content;
	}

	public String getAttacherUuid() {
		return attacherUuid;
	}

	public void setAttacherUuid(String attacherUuid) {
		this.attacherUuid = attacherUuid;
	}

}
