package fr.sedoo.commons.client.widget.api;

import com.google.gwt.user.client.ui.IsWidget;

public interface HtmlTextEditor extends IsWidget{
	
	String getHtml();
	void setHtml(String html);

}
