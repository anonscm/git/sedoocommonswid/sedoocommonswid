package fr.sedoo.commons.client.widget.table.multiline;

import com.google.gwt.cell.client.TextCell;
import com.google.gwt.text.shared.SafeHtmlRenderer;
import com.google.gwt.user.cellview.client.Column;

public abstract class MultilineTextColumn<T> extends Column<T, String> {

	/**
	 * Construct a new TextColumn.
	 */
	public MultilineTextColumn() {
		super(new TextCell(MultilineRenderer.getInstance()));
	}

	public MultilineTextColumn(SafeHtmlRenderer<String> renderer) {
		super(new TextCell(renderer));
	}
}
