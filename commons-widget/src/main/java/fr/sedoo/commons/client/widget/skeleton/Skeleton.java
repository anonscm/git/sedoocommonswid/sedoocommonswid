package fr.sedoo.commons.client.widget.skeleton;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.DockLayoutPanel;

public abstract class Skeleton extends DockLayoutPanel
{
	protected EventBus eventBus;
	
	public Skeleton(EventBus eventBus)
	{
		super(Unit.EM);
		this.eventBus = eventBus;
	}
	
}
