package fr.sedoo.commons.client.event;

import com.google.gwt.event.shared.GwtEvent;

import fr.sedoo.commons.client.user.AuthenticatedUser;

public class UserLoginEvent extends GwtEvent<UserLoginEventHandler>{

	public static final Type<UserLoginEventHandler> TYPE = new Type<UserLoginEventHandler>();
	private AuthenticatedUser user;
	

	public UserLoginEvent(AuthenticatedUser user)
	{
		this.user = user;
		
	}

	@Override
	protected void dispatch(UserLoginEventHandler handler) {
		handler.onNotification(this);
	}

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<UserLoginEventHandler> getAssociatedType() {
		return TYPE;
	}

	public AuthenticatedUser getUser() {
		return user;
	}
}
