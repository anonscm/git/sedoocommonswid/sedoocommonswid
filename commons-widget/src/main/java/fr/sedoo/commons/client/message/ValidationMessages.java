package fr.sedoo.commons.client.message;

import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.LocalizableResource.DefaultLocale;
import com.google.gwt.i18n.client.LocalizableResource.Generate;
import com.google.gwt.i18n.client.Messages;

@Generate(format = { "com.google.gwt.i18n.rebind.format.PropertiesFormat" }, fileName = "Message", locales = { "fr", "en", "default" })
@DefaultLocale("en")
public interface ValidationMessages extends Messages {

	public static final ValidationMessages INSTANCE = GWT.create(ValidationMessages.class);

	@Key("numericalData")
	public String numericalData();

	@Key("mandatoryData")
	public String mandatoryData();

	@Key("emailData")
	public String emailData();

	@Key("dateUnconsistency")
	public String dateUnconsistency();

	@Key("atLeastOneElementNeeded")
	public String atLeastOneElementNeeded();

	@Key("dateData")
	public String dateData();
}
