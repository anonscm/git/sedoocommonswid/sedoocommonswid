package fr.sedoo.commons.client.user;


public class UserManagerImpl implements UserManager{

	AuthenticatedUser user = null;
	
	@Override
	public AuthenticatedUser getUser() 
	{
		return user;
	}
	
	public void setUser(AuthenticatedUser user) {
		this.user = user;
	}

	@Override
	public void clearCurrentUser() {
		setUser(null);
	}

}
