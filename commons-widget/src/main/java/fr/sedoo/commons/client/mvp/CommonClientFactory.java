package fr.sedoo.commons.client.mvp;

import com.google.gwt.place.shared.PlaceController;

public interface CommonClientFactory extends BasicClientFactory {

	PlaceController getPlaceController();
	NavigationManager getNavigationManager();
	
}
