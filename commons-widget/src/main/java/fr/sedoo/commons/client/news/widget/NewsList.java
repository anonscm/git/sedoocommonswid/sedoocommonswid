package fr.sedoo.commons.client.news.widget;

import java.util.ArrayList;

import com.google.gwt.user.cellview.client.CellList;

import fr.sedoo.commons.client.widget.celllist.CellListResources;
import fr.sedoo.commons.shared.domain.New;

public class NewsList extends CellList<New>{

	NewsCell newsCell;
	
	public NewsList() 
	{
		super(new NewsCell(),CellListResources.INSTANCE);
		newsCell = (NewsCell) getCell();
		setKeyboardSelectionPolicy(KeyboardSelectionPolicy.DISABLED);
	}

	public void setPresenter(NewsListPresenter presenter) 
	{
		newsCell.setPresenter(presenter);
	}
	
	public void reset()
	{
		setRowData(new ArrayList<New>());
	}
	
	
	
	

}
