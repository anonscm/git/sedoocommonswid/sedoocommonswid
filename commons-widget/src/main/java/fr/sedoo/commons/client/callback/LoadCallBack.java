package fr.sedoo.commons.client.callback;

public interface LoadCallBack<E> {

	void postLoadProcess(E result);

}
