package fr.sedoo.commons.client.faq.ui;

import java.util.ArrayList;

import com.google.gwt.user.client.ui.IsWidget;

import fr.sedoo.commons.shared.domain.faq.FaqTableItem;

public interface FaqManageView extends IsWidget {

	void setQuestions(ArrayList<FaqTableItem> questions);

	void setPresenter(Presenter presenter);

	void broadcastDeletion(String uuid);

	public interface Presenter {
		void edit(String uuid);

		void create();

		void delete(String uuid);

		void swap(String firstId, String secondId);
	}

	void broadcastSwap(String firstId, String secondId);

}
