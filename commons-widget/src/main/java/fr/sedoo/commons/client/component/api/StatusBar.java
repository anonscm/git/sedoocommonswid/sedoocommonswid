package fr.sedoo.commons.client.component.api;

import fr.sedoo.commons.client.event.ActionEndEventHandler;
import fr.sedoo.commons.client.event.ActionStartEventHandler;
import fr.sedoo.commons.client.event.MaximizeEventHandler;
import fr.sedoo.commons.client.event.MinimizeEventHandler;
import fr.sedoo.commons.client.event.ParameterLoadedEventHandler;
import fr.sedoo.commons.client.widget.PreferredHeightWidget;

public interface StatusBar extends MinimizeEventHandler, MaximizeEventHandler, ParameterLoadedEventHandler,ActionEndEventHandler, ActionStartEventHandler, PreferredHeightWidget 
{

}