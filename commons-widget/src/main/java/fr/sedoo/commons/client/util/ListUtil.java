package fr.sedoo.commons.client.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ListUtil {

	protected ListUtil() {
	}

	public static final int FIRST_INDEX = 0;
	public static final int SINGLETON_SIZE = 1;

	public static boolean isEmpty(List list) {
		if (list == null) {
			return true;
		} else {
			return list.isEmpty();
		}
	}

	public static boolean isNotEmpty(List list) {
		return (!(isEmpty(list)));
	}

	public static List<String> fromSeparatedString(String src, String separator) {
		List<String> result = new ArrayList<String>();
		if (src != null) {
			String[] split = src.split(separator);
			for (int i = 0; i < split.length; i++) {
				if (StringUtil.isNotEmpty(split[i])) {
					result.add(split[i]);
				}
			}
		}
		return result;
	}

	public static String toSeparatedString(List<String> src, String separator) {
		StringBuffer sb = new StringBuffer();
		if (src != null) {
			Iterator<String> iterator = src.iterator();
			while (iterator.hasNext()) {
				sb.append(iterator.next());
				if (iterator.hasNext()) {
					sb.append(separator);
				}
			}
		}
		return sb.toString();
	}

}
