package fr.sedoo.commons.client.faq.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import fr.sedoo.commons.client.util.ServiceException;
import fr.sedoo.commons.shared.domain.faq.FaqTableItem;
import fr.sedoo.commons.shared.domain.faq.QuestionAndAnswer;

@RemoteServiceRelativePath("faq")
public interface FaqService extends RemoteService {
	ArrayList<QuestionAndAnswer> getAllQuestionsAndAnswers(String preferredLanguage, List<String> alternateLanguages) throws ServiceException;

	void deleteFaqByUuid(String uuid) throws ServiceException;

	HashMap<String, QuestionAndAnswer> getFaqContentByUuid(String uuid) throws ServiceException;

	String saveFaq(String uuid, HashMap<String, QuestionAndAnswer> content) throws ServiceException;

	QuestionAndAnswer getConsultFaqContentByUuid(String uuid, String preferredLanguage, List<String> alternateLanguages) throws ServiceException;

	ArrayList<FaqTableItem> getQuestions(String preferredLanguage, List<String> alternateLanguages) throws ServiceException;

	void swap(String firstId, String secondId);
}
