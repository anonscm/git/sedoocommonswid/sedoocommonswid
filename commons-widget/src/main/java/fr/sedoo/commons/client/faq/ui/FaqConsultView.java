package fr.sedoo.commons.client.faq.ui;

import java.util.List;

import com.google.gwt.user.client.ui.IsWidget;

import fr.sedoo.commons.shared.domain.faq.QuestionAndAnswer;

public interface FaqConsultView extends IsWidget {

	void setContent(List<QuestionAndAnswer> questions);
	void reset();
}
