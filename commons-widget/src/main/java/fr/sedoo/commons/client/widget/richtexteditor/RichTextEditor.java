package fr.sedoo.commons.client.widget.richtexteditor;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.RichTextArea;
import com.google.gwt.user.client.ui.VerticalPanel;

public class RichTextEditor extends Composite {
	RichTextArea content = null;
	private VerticalPanel panel;
	private HorizontalPanel toolbarPanel;

	public RichTextEditor() {
		content = new RichTextArea();
		content.setSize("100%", "14em");
		content.getElement().getStyle().setBackgroundColor("white");
		ToolBar toolbar = GWT.create(DefaultToolbar.class);
		toolbar.setRichTextArea(content);

		panel = new VerticalPanel();
		toolbarPanel = new HorizontalPanel();
		toolbarPanel.setWidth("100%");
		toolbarPanel.getElement().getStyle().setBackgroundColor("#DDDDDD");
		toolbarPanel.add(toolbar);
		panel.setBorderWidth(1);
		panel.add(toolbarPanel);
		panel.add(content);
		initWidget(panel);
	}

	public String getHTML() {
		return content.getHTML();
	}

	public void setHTML(String input) {
		content.setHTML(input);
	}

	public void addToolBar(Composite toolbar) {
		toolbarPanel.add(toolbar);
	}

	public RichTextArea getRichTextArea() {
		return content;
	}

}
