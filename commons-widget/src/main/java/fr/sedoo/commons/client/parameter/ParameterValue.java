package fr.sedoo.commons.client.parameter;

import fr.sedoo.commons.client.util.StringUtil;

public class ParameterValue {

	private String value;

	public ParameterValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return value;
	}

	public String asString() {
		return StringUtil.trimToEmpty(value);
	}

	public Integer asInteger() {
		return new Integer(value);
	}
}
