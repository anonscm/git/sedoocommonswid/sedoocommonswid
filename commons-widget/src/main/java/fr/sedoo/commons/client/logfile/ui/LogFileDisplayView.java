package fr.sedoo.commons.client.logfile.ui;

import java.util.ArrayList;

import com.google.gwt.user.client.ui.IsWidget;

import fr.sedoo.commons.client.back.BackPresenter;
import fr.sedoo.commons.shared.domain.LogFile;

public interface LogFileDisplayView extends IsWidget {

	void setLogFiles(ArrayList<LogFile> files);

	void reset();

	void setBackPresenter(BackPresenter presenter);

	void setPresenter(Presenter presenter);

	public interface Presenter {
		void downloadLog(String id);
	}

}
