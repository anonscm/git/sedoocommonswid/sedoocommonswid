package fr.sedoo.commons.client.faq.bundle;

import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.LocalizableResource.DefaultLocale;
import com.google.gwt.i18n.client.LocalizableResource.Generate;
import com.google.gwt.i18n.client.Messages;

@Generate(
format = { "com.google.gwt.i18n.rebind.format.PropertiesFormat" }, 
fileName = "Message", 
locales = {"en, fr"})
@DefaultLocale("en")
public interface FaqMessages extends Messages {
	
	public static final FaqMessages INSTANCE = GWT.create(FaqMessages.class);
	
	@Key("questions")
	public String questions();
	
	@Key("question")
	public String question();
	
	@Key("answer")
	public String answer();
	
	@Key("faqManagementViewTitle")
	public String faqManagementViewTitle();
	
	@Key("faqEditionViewTitle")
	public String faqEditionViewTitle();
	
	@Key("faqConsultationViewTitle")
	public String faqConsultationViewTitle();
	
	@Key("addNewQuestion")
	public String addNewQuestion();
	
}
