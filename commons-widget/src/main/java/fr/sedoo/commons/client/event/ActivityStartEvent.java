package fr.sedoo.commons.client.event;

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.event.shared.GwtEvent;

import fr.sedoo.commons.client.mvp.activity.DefaultActivity;

public class ActivityStartEvent extends GwtEvent<ActivityStartEventHandler>{

	public final static String ADMINISTRATOR_ACTIVITY = "ADMINISTRATOR";
	public final static String AUTHENTICATED_ACTIVITY = "MEMBER";
	public final static String DEFAULT_USER_ACTIVITY = "DEFAULT_USER";
	
	private String type = DEFAULT_USER_ACTIVITY;
	private AbstractActivity activity;
	
	public ActivityStartEvent(AbstractActivity activity)
	{
		this.activity = activity;
	}
	
	public ActivityStartEvent(String type)
	{
		this.type = type;
	}
	
	public static final Type<ActivityStartEventHandler> TYPE = new Type<ActivityStartEventHandler>();
	

	@Override
	protected void dispatch(ActivityStartEventHandler handler) {
		handler.onNotification(this);
	}

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<ActivityStartEventHandler> getAssociatedType() {
		return TYPE;
	}

	public String getType() {
		return type;
	}
	
	public AbstractActivity getActivity() {
		return activity;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public void setActivity(DefaultActivity activity) {
		this.activity = activity;
	}
	

}
