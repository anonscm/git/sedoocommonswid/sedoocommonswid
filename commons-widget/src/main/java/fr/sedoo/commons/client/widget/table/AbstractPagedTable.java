package fr.sedoo.commons.client.widget.table;

import java.util.ArrayList;

import fr.sedoo.commons.shared.domain.HasIdentifier;


public abstract class AbstractPagedTable extends AbstractTable {
	public final static int DEFAULT_POSITION = 1;

	private final SedooSimplePager pager;
	private int PAGE_SIZE;

	public AbstractPagedTable(int pageSize) {
		super();
		PAGE_SIZE = pageSize;
		setHeaderPanelVisible(true);
		pager = new SedooSimplePager(pageSize);
		pager.setDisplay(itemTable);
		setPagerInPanel(pager);
		getDataProvider().addDataDisplay(itemTable);
		
	}

	public void reset() {
		pager.setPageSize(PAGE_SIZE);
		pager.setPageStart(0);
		itemTable.setRowData(0, new ArrayList<HasIdentifier>());
		itemTable.setRowCount(0);
		setHeaderPanelVisible(false);
		checkEmptyPanelVisibility();
	}

}
