package fr.sedoo.commons.client.widget.uploader;

import java.util.ArrayList;

import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.widget.table.ImageTable;
import fr.sedoo.commons.shared.domain.HasIdentifier;
import fr.sedoo.commons.shared.domain.misc.ImageUrl;

public class ImagePanelContent extends HorizontalPanel implements UploadListener {

	private ImageTable imageTable = new ImageTable();
	FileUploader uploader;

	public ImagePanelContent() {
		super();
		setSpacing(5);
		setHeight("100%");
		setHorizontalAlignment(HasHorizontalAlignment.ALIGN_LEFT);

		VerticalPanel left = new VerticalPanel();

		Label tableTitle = new Label(CommonMessages.INSTANCE.images());
		tableTitle.setStyleName("title2");
		//imageTable.setWidth("100%");
		imageTable.init(new ArrayList<HasIdentifier>());
		left.add(tableTitle);
		//left.add(imageTable);

		ScrollPanel scroll = new ScrollPanel(imageTable);
		scroll.setHeight("300px");
		scroll.setWidth("300px");
//		scroll.setAlwaysShowScrollBars(true);
		//scroll.add(left);
		left.add(scroll);
		add(left);
		VerticalPanel right = new VerticalPanel();
		Label uploaderTitle = new Label(CommonMessages.INSTANCE.addImage());
		uploaderTitle.setStyleName("title2");
		uploader = new FileUploader();
		uploader.addUploadListener(this);
		right.add(uploaderTitle);
		right.add(uploader);

		add(left);
		add(right);
		setCellWidth(left,"320px");
		setCellWidth(right,"200px");
	}

	public void setUuid(String uuid) {
		uploader.setUuid(uuid);
	}

	public void reset() {
		imageTable.reset();
	}

	public void setPresenter(FileUploadPresenter presenter) {
		imageTable.setPresenter(presenter);

	}

	public void setImages(ArrayList<ImageUrl> images) {
		imageTable.init(images);
	}

	public void broadcastDeletion(String id) {
		imageTable.removeRow(id);

	}

	@Override
	public void fileUploaded(Long id) {
		ImageUrl url = new ImageUrl();
		url.setId(id);
		url.setLink(FileUploadUtil.getImageUrlFromId(id));
		imageTable.addElement(url);
	}

}
