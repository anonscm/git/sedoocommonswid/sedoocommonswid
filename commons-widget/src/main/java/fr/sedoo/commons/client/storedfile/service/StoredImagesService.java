package fr.sedoo.commons.client.storedfile.service;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import fr.sedoo.commons.client.util.ServiceException;
import fr.sedoo.commons.shared.domain.misc.ImageUrl;

@RemoteServiceRelativePath("storedimages")
public interface StoredImagesService extends RemoteService {
	ArrayList<ImageUrl> getStoredImagesByContainerId(String containerId) throws ServiceException;

	void deleteStoredImageById(Long id) throws ServiceException;

	void deleteStoredImageByContainerId(String containerId);
}
