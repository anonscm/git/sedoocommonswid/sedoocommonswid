package fr.sedoo.commons.client.faq.ui;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Widget;

import fr.sedoo.commons.client.component.AbstractView;
import fr.sedoo.commons.client.faq.ui.list.FaqList;
import fr.sedoo.commons.shared.domain.faq.QuestionAndAnswer;

public class FaqConsultViewImpl extends AbstractView implements FaqConsultView {

	@UiField
	FaqList faqList;

	private static WelcomeViewImplUiBinder uiBinder = GWT.create(WelcomeViewImplUiBinder.class);

	interface WelcomeViewImplUiBinder extends UiBinder<Widget, FaqConsultViewImpl> {
	}

	public FaqConsultViewImpl() {
		super();
		// GWT.<GlobalBundle>create(GlobalBundle.class).css().ensureInjected();
		initWidget(uiBinder.createAndBindUi(this));
		applyCommonStyle();
	}

	@Override
	public void setContent(List<QuestionAndAnswer> questions) {
		faqList.setRowData(questions);
	}

	@Override
	public void reset() {
		faqList.setRowData(new ArrayList<QuestionAndAnswer>());
	}

}
