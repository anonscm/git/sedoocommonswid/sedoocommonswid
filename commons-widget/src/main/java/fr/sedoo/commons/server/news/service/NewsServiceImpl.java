package fr.sedoo.commons.server.news.service;

import java.util.ArrayList;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import fr.sedoo.commons.client.news.service.NewsService;
import fr.sedoo.commons.client.util.ServiceException;
import fr.sedoo.commons.server.DefaultServerApplication;
import fr.sedoo.commons.server.news.dao.MessageDAO;
import fr.sedoo.commons.shared.domain.Message;
import fr.sedoo.commons.shared.domain.New;

public class NewsServiceImpl extends RemoteServiceServlet implements NewsService {

	private MessageDAO dao;
	private final static int LIST_SIZE = 10;

	private void initDao() {
		if (dao == null) {
			dao = (MessageDAO) DefaultServerApplication.getSpringBeanFactory().getBeanByName(MessageDAO.BEAN_NAME);
		}
	}

	@Override
	public ArrayList<New> getLatest() throws ServiceException {
		initDao();
		ArrayList<Message> found = dao.findAll();
		ArrayList<New> result = new ArrayList<New>();
		if (found.size() > LIST_SIZE) {
			result.addAll(found.subList(0, LIST_SIZE));
		} else {
			result.addAll(found);
		}
		return result;
	}

}
