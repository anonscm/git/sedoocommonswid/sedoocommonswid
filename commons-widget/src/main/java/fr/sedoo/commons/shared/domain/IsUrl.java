package fr.sedoo.commons.shared.domain;

public interface IsUrl {

	String getLink();

	String getLabel();

}
