package fr.sedoo.commons.client.faq.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

public class FaqConsultPlace  extends Place 
{

	public static FaqConsultPlace instance;
	
	public static class Tokenizer implements PlaceTokenizer<FaqConsultPlace>
	{
		@Override
		public String getToken(FaqConsultPlace place)
		{
			return "";
		}

		@Override
		public FaqConsultPlace getPlace(String token)
		{
			if (instance == null)
			{
				instance = new FaqConsultPlace();
			}
			return instance;
		}
	}

	public FaqConsultPlace()
	{
		
	}
	
	
	
}

