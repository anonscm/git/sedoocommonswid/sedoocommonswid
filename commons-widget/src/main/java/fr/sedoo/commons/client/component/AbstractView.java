package fr.sedoo.commons.client.component;

import java.util.Iterator;
import java.util.List;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.resources.client.CssResource;

public class AbstractView extends AbstractStyledResizeComposite {

	public AbstractView() {

		super();
		CssResourceProvider provider = GWT.create(DefaultCssResourceProvider.class);
		if (provider != null) {
			List<CssResource> cssResources = provider.getCssResources();
			Iterator<CssResource> iterator = cssResources.iterator();
			while (iterator.hasNext()) {
				CssResource cssResource = iterator.next();
				cssResource.ensureInjected();
			}
		}
	}
}
