package fr.sedoo.commons.client.mvp;

import com.google.gwt.user.client.ui.IsWidget;

import fr.sedoo.commons.client.mvp.place.LoginPlace;
import fr.sedoo.commons.client.user.UserManager;

/**
 * Une BasicClientFactory est une fabrique à vue gérant les aspects relatifs à
 * l'authentification : - Gestionnaire d'utilisateur - Place de connection
 * 
 * @author francois
 * 
 */

public interface AuthenticatedClientFactory extends BasicClientFactory {

	UserManager getUserManager();

	LoginPlace getLoginPlace();

	IsWidget getUnAuthorizedUserView();
}
