package fr.sedoo.commons.client.news.widget;

import java.util.ArrayList;
import java.util.Iterator;

import org.gwtbootstrap3.client.ui.Alert;
import org.gwtbootstrap3.client.ui.Carousel;
import org.gwtbootstrap3.client.ui.CarouselCaption;
import org.gwtbootstrap3.client.ui.CarouselIndicator;
import org.gwtbootstrap3.client.ui.CarouselIndicators;
import org.gwtbootstrap3.client.ui.CarouselInner;
import org.gwtbootstrap3.client.ui.CarouselSlide;
import org.gwtbootstrap3.client.ui.Heading;
import org.gwtbootstrap3.client.ui.Image;
import org.gwtbootstrap3.client.ui.Paragraph;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.SpanElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.place.shared.PlaceChangeEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

import fr.sedoo.commons.client.DefaultClientApplication;
import fr.sedoo.commons.client.image.CommonBundle;
import fr.sedoo.commons.client.news.place.NewDisplayPlace;
import fr.sedoo.commons.client.news.ui.MessageEditViewImpl;
import fr.sedoo.commons.client.style.SedooStyleBundle;
import fr.sedoo.commons.client.util.ElementUtil;
import fr.sedoo.commons.client.util.ListUtil;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.shared.domain.New;
import fr.sedoo.commons.shared.domain.message.TitledMessage;

public class CarrouselBlock extends Composite {

	private static CarrouselBlockUiBinder uiBinder = GWT.create(CarrouselBlockUiBinder.class);

	private static final int DEFAULT_CAROUSEL_HEIGHT = 300;

	interface CarrouselBlockUiBinder extends UiBinder<Widget, CarrouselBlock> {
	}

	ArrayList<Image> carouselImages = new ArrayList<Image>();

	@UiField
	NewsBlock newsBlock;

	@UiField
	Alert noNewsAlert;

	@UiField
	CarouselIndicators carouselIndicators;

	@UiField
	CarouselInner carouselInner;

	@UiField
	SpanElement carouselContainer;

	@UiField
	DivElement carouselWidthIndicator;

	@UiField
	Carousel carousel;

	private int currentCarouselHeight = DEFAULT_CAROUSEL_HEIGHT;

	public CarrouselBlock() {
		super();
		initWidget(uiBinder.createAndBindUi(this));
	}

	public void setNews(ArrayList<? extends New> frontPageNews, ArrayList<? extends New> otherNews) {
		ElementUtil.hide(newsBlock);
		ElementUtil.hide(carouselContainer);
		boolean noNews = false;
		if ((ListUtil.isEmpty(frontPageNews)) && (ListUtil.isEmpty(otherNews))) {
			ElementUtil.show(noNewsAlert);
			noNews = true;
		} else {
			ElementUtil.hide(noNewsAlert);
		}
		if (ListUtil.isNotEmpty(frontPageNews)) {
			ElementUtil.show(carouselContainer);
			onResize();
			carouselIndicators.clear();
			carouselInner.clear();
			carouselImages.clear();
			boolean first = true;
			int index = 0;
			Iterator<? extends New> iterator = frontPageNews.iterator();
			while (iterator.hasNext()) {
				New current = (New) iterator.next();
				if (current instanceof TitledMessage) {
					final TitledMessage aux = (TitledMessage) current;
					CarouselSlide slide = new CarouselSlide();
					CarouselIndicator indicator = new CarouselIndicator();
					indicator.setDataSlideTo("" + index);
					indicator.setTarget("#carousel-basic");
					if (index == 0) {
						indicator.setActive(true);
					}
					index++;

					carouselIndicators.add(indicator);

					if (first) {
						slide.setActive(true);
						first = false;
					}
					org.gwtbootstrap3.client.ui.Image img = new org.gwtbootstrap3.client.ui.Image();
					img.setWidth("100%");
					img.getElement().getStyle().setProperty("maxHeight", currentCarouselHeight + "px");
					if (StringUtil.isNotEmpty(aux.getUrl())) {
						img.setUrl(StringUtil.trimToEmpty(aux.getUrl()));
					} else {
						img.setUrl(GWT.getModuleBaseURL() + MessageEditViewImpl.getStoredFileServicePath() + "ILLUSTRATION_OF_" + aux.getUuid());
					}
					carouselImages.add(img);
					slide.add(img);
					img.setStyleName(SedooStyleBundle.INSTANCE.sedooStyle().carouselImage());
					Heading heading = new Heading(3);
					heading.addStyleName(CommonBundle.INSTANCE.commonStyle().clickable());
					heading.addDomHandler(new ClickHandler() {

						@Override
						public void onClick(ClickEvent event) {

							NewDisplayPlace place = new NewDisplayPlace(aux.getUuid());
							DefaultClientApplication.getClientFactory().getEventBus().fireEvent(new PlaceChangeEvent(place));
						}

					}, ClickEvent.getType());
					heading.setText(aux.getTitle());
					CarouselCaption caption = new CarouselCaption();

					Paragraph paragraph = new Paragraph(truncate(removeHtmlTags(aux.getContent()), 100));

					caption.add(heading);
					caption.add(paragraph);
					slide.add(caption);
					carouselInner.add(slide);
				}
			}

		}
		if (ListUtil.isNotEmpty(otherNews)) {
			ElementUtil.show(newsBlock);
			newsBlock.setNews(otherNews);
		} else {
			if (noNews == false) {
				// On affiche tout de même la liste afin d'avoir le lien vers
				// les archives
				newsBlock.setNews(new ArrayList<New>());
				ElementUtil.show(newsBlock);
			}
		}
	}

	public void onResize() {
		currentCarouselHeight = carouselWidthIndicator.getClientWidth() / 2;
		resizeCarousel();

	}

	private void resizeCarousel() {
		Iterator<org.gwtbootstrap3.client.ui.Image> iterator = carouselImages.iterator();
		while (iterator.hasNext()) {
			org.gwtbootstrap3.client.ui.Image image = (org.gwtbootstrap3.client.ui.Image) iterator.next();
			image.getElement().getStyle().setProperty("maxHeight", currentCarouselHeight + "px");
			image.setHeight(currentCarouselHeight + "px");
		}
		carousel.setHeight(currentCarouselHeight + "px");

	}

	private String removeHtmlTags(String html) {
		String regex = "(<([^>]+)>)";
		html = html.replaceAll(regex, "");
		html = html.replaceAll("&nbsp;", " ");
		return html;
	}

	private String truncate(String src, int length) {
		String aux = src;
		if (aux.length() > length) {
			aux = aux.substring(0, length) + " [...]";
		}
		return aux;
	}

	public void reset() {
		newsBlock.reset();
		ElementUtil.hide(noNewsAlert);
		ElementUtil.hide(newsBlock);
		ElementUtil.hide(carouselContainer);
	}

	public void setPresenter(NewsListPresenter presenter) {
		newsBlock.setPresenter(presenter);
	}
}
