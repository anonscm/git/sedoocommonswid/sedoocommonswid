package fr.sedoo.commons.client.widget.breadcrumb;

import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.mvp.place.AboutPlace;
import fr.sedoo.commons.client.mvp.place.SystemPlace;
import fr.sedoo.commons.client.mvp.place.WelcomePlace;

public class CommonShortcutFactory {

	static public Shortcut getWelcomeShortcut()
	{
		return new Shortcut(CommonMessages.INSTANCE.home(), new WelcomePlace());
	}
	
	public static Shortcut getAboutShortcut() {
		return new Shortcut(CommonMessages.INSTANCE.about(), new AboutPlace());
	}

	public static Shortcut getSystemShortcut() {
		return new Shortcut(CommonMessages.INSTANCE.systemInformation(), new SystemPlace());
	}

}
