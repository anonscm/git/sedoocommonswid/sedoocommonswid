package fr.sedoo.commons.client.ckeditor;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.TextResource;

public interface CKEditorJSBundle extends ClientBundle {

	public static final CKEditorJSBundle INSTANCE = GWT.create(CKEditorJSBundle.class);

	@Source("ckeditor.js")
	TextResource ckEditorJS();

}