package fr.sedoo.commons.server.file;

import java.io.File;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.apache.commons.lang.StringUtils;

import fr.sedoo.commons.server.misc.TmpKeyManager;

@Path("/file")
public class FileService {
	
	static TmpKeyManager<String> keys = new TmpKeyManager<String>();

	@GET
	@Path("/isAlive")
	public Response getVersion() {
		String answer = "Yes";
		return Response.status(200).entity(answer).build();
	}

	@GET
	@Path("/getByKey/{key}")
	@Produces("text/plain")
	public Response getTmpImage(@PathParam("key") String key) {
		try {
			
			String fileName = keys.getItemFromKey(key);
			if (StringUtils.isEmpty(fileName))
			{
				return Response.status(404).build();
			}
			else
			{
				File file = new File(fileName);
				ResponseBuilder response = Response.ok((Object) file);
				response.header("Content-Disposition","attachment; filename=\""+file.getName()+"\"");
				return response.build(); 	
			}
		} catch (Exception e) {
			return Response.status(404).build();
		}
	}

	public static String addFile(File current) 
	{
		return keys.add(current.getPath());
	}

	

}
