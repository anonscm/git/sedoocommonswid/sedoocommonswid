package fr.sedoo.commons.client.widget;

public interface ConfirmCallBack {
	
	public void confirm(boolean choice);

}
