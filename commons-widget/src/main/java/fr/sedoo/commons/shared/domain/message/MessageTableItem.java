package fr.sedoo.commons.shared.domain.message;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import fr.sedoo.commons.client.widget.editing.ValidationAlert;
import fr.sedoo.commons.shared.domain.HasIdentifier;

public class MessageTableItem implements HasIdentifier, Serializable
{
	private String uuid;
	private String title;
	private String author;
	private Date date;
	
	public MessageTableItem() {
	}
	
	public MessageTableItem(String uuid, String title, String author, Date date) {
		setUuid(uuid);
		setTitle(title);
		setAuthor(author);
		setDate(date);
	}
	
	@Override
	public String getIdentifier() 
	{
		return getUuid();
	}
	@Override
	public List<ValidationAlert> validate() {
		return new ArrayList<ValidationAlert>();
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
}
