package fr.sedoo.commons.client.widget.map.impl;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.RequiresResize;
import com.google.gwt.user.client.ui.Widget;

import fr.sedoo.commons.client.image.CommonBundle;
import fr.sedoo.commons.client.widget.editing.impl.DefaultEditor;
import fr.sedoo.commons.shared.domain.GeographicBoundingBoxDTO;

/**
 * Widget affichant une carte et des zones de texte Nord, Est, Sud, Ouest <br>
 * - la carte permet le dessin/effacement d'une unique zone rectangulaire et le
 * déplacement de la carte<br>
 * - Les zones sont synchronisées avec le rectangle<br>
 * - Un mode "display" transforme les zones de saisie en zones d'affichage
 * 
 * 
 * @author andre
 * 
 */
public class MultipleAreaSelectorWidget extends DefaultEditor implements RequiresResize {

	private static MapWidgetUiBinder uiBinder = GWT.create(MapWidgetUiBinder.class);

	@UiField(provided = true)
	MultipleAreaTable multipleAreaTable;

	@UiField(provided = true)
	AreaSelectorWidget areaSelector;

	interface MapWidgetUiBinder extends UiBinder<Widget, MultipleAreaSelectorWidget> {
	}

	public MultipleAreaSelectorWidget() {
		this(AreaSelectorWidget.DEFAULT_MAP_LAYER);
	}

	public MultipleAreaSelectorWidget(String mapLayer) {
		CommonBundle.INSTANCE.css().ensureInjected();
		areaSelector = new AreaSelectorWidget(mapLayer);
		multipleAreaTable = new MultipleAreaTable(areaSelector);
		initWidget(uiBinder.createAndBindUi(this));
		init();
	}

	protected void init() {
		areaSelector.activateDrawRectangularAreaControl();
		reset();
	}

	public ArrayList<GeographicBoundingBoxDTO> getGeographicBoundingBoxDTOList() {
		return multipleAreaTable.getBoxes();
	}

	public void setGeographicBoundingBoxDTOList(ArrayList<GeographicBoundingBoxDTO> boxes) {
		multipleAreaTable.setBoxes(boxes);
		areaSelector.zoomAll();
	}

	public void reset() {
		multipleAreaTable.setBoxes(new ArrayList<GeographicBoundingBoxDTO>());
		areaSelector.reset();
	}

	@Override
	public void enableDisplayMode() {
		super.enableDisplayMode();
		areaSelector.enableDisplayMode();
		multipleAreaTable.enableDisplayColumn();
		multipleAreaTable.hideToolBar();

	}

	@Override
	public void enableEditMode() {
		super.enableEditMode();
		areaSelector.enableEditMode();
		multipleAreaTable.enableEditColumn();
		multipleAreaTable.showToolBar();
	}

	@Override
	public void onResize() {
		if (areaSelector != null) {
			areaSelector.onResize();
		}
	}

}
