package fr.sedoo.commons.server.storedfile.dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fr.sedoo.commons.client.util.ListUtil;
import fr.sedoo.commons.shared.domain.StoredFile;

@Repository
public class StoredFileDAOJPAImpl implements StoredFileDAO {

	private static Logger logger = LoggerFactory.getLogger(StoredFileDAOJPAImpl.class);

	protected EntityManager em;

	public EntityManager getEntityManager() {
		return em;
	}

	@PersistenceContext
	public void setEntityManager(EntityManager entityManager) {
		this.em = entityManager;
	}

	@Transactional
	public StoredFile save(StoredFile storedFile) {
		logger.debug("Debut sauvegarde");
		if (storedFile.getId() == null) {
			logger.debug("Debut Persit");
			getEntityManager().persist(storedFile);

			return storedFile;
		} else {
			logger.debug("Debut Merge");
			getEntityManager().merge(storedFile);
			return storedFile;
		}
	}

	@Override
	@Transactional
	public void deleteStoredFileByAttacherUuid(String attacherUuid) {
		List<StoredFile> list = em.createQuery("select storedFile from StoredFile storedFile where storedFile.attacherUuid='" + attacherUuid + "'").getResultList();
		for (StoredFile storedFile : list) {
			em.remove(storedFile);
		}
	}

	@Override
	@Transactional
	public StoredFile findByAttacherUuid(String attacherUuid) {
		List<StoredFile> list = em.createQuery("select storedFile from StoredFile storedFile where storedFile.attacherUuid='" + attacherUuid + "'").getResultList();
		if ((list == null) || list.isEmpty()) {
			return new StoredFile();
		} else {
			return list.get(ListUtil.FIRST_INDEX);
		}
	}

	@Override
	@Transactional
	public ArrayList<StoredFile> findFilesByAttacherUuid(String attacherUuid, boolean full) {
		ArrayList<StoredFile> result = new ArrayList<StoredFile>();
		List<StoredFile> tmp = em.createQuery("select storedFile from StoredFile storedFile where storedFile.attacherUuid='" + attacherUuid + "'").getResultList();
		if ((tmp == null) || tmp.isEmpty()) {
			return result;
		} else {
			if (full) {
				result.addAll(tmp);
				return result;
			} else {
				Iterator<StoredFile> iterator = tmp.iterator();
				while (iterator.hasNext()) {
					StoredFile storedFile = iterator.next();
					result.add(storedFile);
				}
				return result;
			}
		}
	}

	@Override
	@Transactional
	public void deleteStoredFileById(Long id) {
		StoredFile aux = getEntityManager().find(StoredFile.class, id);
		if (aux != null) {
			getEntityManager().remove(aux);
		}

	}

	@Override
	@Transactional
	public StoredFile findById(Long id) {
		return getEntityManager().find(StoredFile.class, id);
	}

}