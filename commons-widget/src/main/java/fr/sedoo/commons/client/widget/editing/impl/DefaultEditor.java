package fr.sedoo.commons.client.widget.editing.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.UIObject;

import fr.sedoo.commons.client.widget.editing.api.Editor;

public class DefaultEditor extends Composite implements Editor {
	
	protected List<UIObject> editWidgets = new ArrayList<UIObject>();
	protected List<UIObject> displayWidgets = new ArrayList<UIObject>();
	
	@Override
	public void enableEditMode() {
		changeVisibility(displayWidgets, false);
		changeVisibility(editWidgets, true);
	}
	
	@Override
	public void enableDisplayMode() {
		changeVisibility(displayWidgets, true);
		changeVisibility(editWidgets, false);
	}
	
	
	private void changeVisibility(List<UIObject> widgets, boolean value)
	{
		Iterator<UIObject> iterator = widgets.iterator();
		while (iterator.hasNext()) {
			iterator.next().setVisible(value);
		}
	}

}
