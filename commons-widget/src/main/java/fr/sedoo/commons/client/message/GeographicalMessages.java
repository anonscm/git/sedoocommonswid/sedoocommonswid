package fr.sedoo.commons.client.message;

import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.LocalizableResource.DefaultLocale;
import com.google.gwt.i18n.client.LocalizableResource.Generate;
import com.google.gwt.i18n.client.Messages;

@Generate(format = { "com.google.gwt.i18n.rebind.format.PropertiesFormat" }, fileName = "Message", locales = { "fr", "en", "default" })
@DefaultLocale("en")
public interface GeographicalMessages extends Messages {

	public static final GeographicalMessages INSTANCE = GWT.create(GeographicalMessages.class);

	@Key("northLatitude")
	public String mapSelectorNorthLatitude();

	@Key("southLatitude")
	public String mapSelectorSouthLatitude();

	@Key("eastLongitude")
	public String mapSelectorEastLongitude();

	@Key("westLongitude")
	public String mapSelectorWestLongitude();

	@Key("drawRectangularAreaTooltip")
	public String drawRectangularAreaTooltip();

	@Key("eraseRectangularAreaTooltip")
	public String eraseRectangularAreaTooltip();

	@Key("dragPanButtonTooltip")
	public String dragPanButtonTooltip();

	@Key("addAnArea")
	public String addAnArea();

}
