package fr.sedoo.commons.client.mvp.activity;

import com.google.gwt.activity.shared.AbstractActivity;

import fr.sedoo.commons.client.mvp.BasicClientFactory;

public abstract class DefaultActivity extends AbstractActivity {
	
	protected BasicClientFactory clientFactory;
	
	public DefaultActivity(BasicClientFactory clientFactory) {
		this.clientFactory = clientFactory;
	}

	
//	protected void broadcastActivityTitle(String title)
//	{
//		clientFactory.getEventBus().fireEvent(new ActivityChangeEvent(title));
//	}
	
	

}
