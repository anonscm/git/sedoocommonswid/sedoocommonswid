package fr.sedoo.commons.client.news.style;

import com.google.gwt.resources.client.CssResource;

public interface NewsStyleCss extends CssResource {

	@ClassName("blockHeader")
	String blockHeader();
	
	@ClassName("newsTitle")
	String newsTitle();
	
	@ClassName("newsImageContainer")
	String newsImageContainer();
	
	@ClassName("newsImage")
	String newsImage();
	
	@ClassName("newsContent")
	String newsContent();
	
	@ClassName("newsBlock")
	String newsBlock();
	
	@ClassName("newsAddedOrModified")
	String newsAddedOrModified();
	
	@ClassName("dropFiles")
	String dropFiles();

	@ClassName("dropFilesOver")
	String dropFilesOver();

	@ClassName("dropFilesDisabled")
	String dropFilesDisabled();
	
}
