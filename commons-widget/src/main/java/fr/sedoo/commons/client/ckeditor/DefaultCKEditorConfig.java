package fr.sedoo.commons.client.ckeditor;

import com.google.gwt.core.client.GWT;

public class DefaultCKEditorConfig extends CKConfig {

	public DefaultCKEditorConfig(String containerId, String language) {
		super(PRESET_TOOLBAR.FULL);
		setToolbar(getResifToolbar());
		setLanguage(language);
		setWidth("100%");
		setHeight("100%");
		setFileBrowserBrowseUrl(GWT.getModuleBaseURL() + getStoredImage() + containerId);
	}

	/*
	 * Return the part of the path to access to the stored file REST service.
	 */
	public static String getStoredImage() {
		return "rest/image/byContainerId/";
	}

	private Toolbar getResifToolbar() {
		ToolbarLine formatLine = new ToolbarLine();
		formatLine.add(TOOLBAR_OPTIONS.Bold);
		formatLine.add(TOOLBAR_OPTIONS.Italic);
		formatLine.add(TOOLBAR_OPTIONS.Underline);
		formatLine.add(TOOLBAR_OPTIONS.Strike);
		formatLine.add(TOOLBAR_OPTIONS.Subscript);
		formatLine.add(TOOLBAR_OPTIONS.Superscript);
		formatLine.addBlockSeparator();
		formatLine.add(TOOLBAR_OPTIONS.RemoveFormat);

		ToolbarLine cutAndPastLine = new ToolbarLine();
		cutAndPastLine.add(TOOLBAR_OPTIONS.Cut);
		cutAndPastLine.add(TOOLBAR_OPTIONS.Copy);
		cutAndPastLine.add(TOOLBAR_OPTIONS.Paste);
		cutAndPastLine.add(TOOLBAR_OPTIONS.PasteText);
		cutAndPastLine.add(TOOLBAR_OPTIONS.PasteFromWord);
		cutAndPastLine.addBlockSeparator();
		cutAndPastLine.add(TOOLBAR_OPTIONS.Undo);
		cutAndPastLine.add(TOOLBAR_OPTIONS.Redo);

		ToolbarLine miscLine = new ToolbarLine();
		miscLine.add(TOOLBAR_OPTIONS.Source);
		miscLine.add(TOOLBAR_OPTIONS.ShowBlocks);
		miscLine.add(TOOLBAR_OPTIONS.Maximize);
		miscLine.add(TOOLBAR_OPTIONS.Preview);

		ToolbarLine justifyLine = new ToolbarLine();
		justifyLine.add(TOOLBAR_OPTIONS.JustifyLeft);
		justifyLine.add(TOOLBAR_OPTIONS.JustifyCenter);
		justifyLine.add(TOOLBAR_OPTIONS.JustifyRight);
		justifyLine.add(TOOLBAR_OPTIONS.JustifyBlock);

		ToolbarLine embeddedLine = new ToolbarLine();
		embeddedLine.add(TOOLBAR_OPTIONS.Link);
		embeddedLine.add(TOOLBAR_OPTIONS.Unlink);
		cutAndPastLine.addBlockSeparator();
		embeddedLine.add(TOOLBAR_OPTIONS.Image);
		embeddedLine.add(TOOLBAR_OPTIONS.Youtube);
		embeddedLine.add(TOOLBAR_OPTIONS.Table);
		embeddedLine.add(TOOLBAR_OPTIONS.Blockquote);
		embeddedLine.add(TOOLBAR_OPTIONS.HorizontalRule);

		ToolbarLine listLine = new ToolbarLine();
		listLine.add(TOOLBAR_OPTIONS.BulletedList);
		listLine.add(TOOLBAR_OPTIONS.NumberedList);
		cutAndPastLine.addBlockSeparator();
		listLine.add(TOOLBAR_OPTIONS.Indent);
		listLine.add(TOOLBAR_OPTIONS.Outdent);

		ToolbarLine styleLine = new ToolbarLine();
		styleLine.add(TOOLBAR_OPTIONS.Styles);

		Toolbar toolbar = new Toolbar();
		toolbar.add(formatLine);
		toolbar.add(justifyLine);
		toolbar.add(cutAndPastLine);
		toolbar.add(listLine);
		toolbar.add(embeddedLine);
		toolbar.add(styleLine);
		toolbar.add(miscLine);

		return toolbar;
	}

}
