package fr.sedoo.commons.client.mvp;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.SimpleEventBus;
import com.google.gwt.place.shared.PlaceController;

import fr.sedoo.commons.client.event.PlaceNavigationEvent;
import fr.sedoo.commons.client.language.ui.LanguageSwitchingView;
import fr.sedoo.commons.client.mvp.place.LoginPlace;
import fr.sedoo.commons.client.user.UserManager;
import fr.sedoo.commons.client.user.UserManagerImpl;
import fr.sedoo.commons.client.util.LocaleUtil;

public abstract class CommonClientFactoryImpl implements CommonClientFactory, AuthenticatedClientFactory {

	private static final EventBus EVENT_BUS = new SimpleEventBus();
	private static final PlaceController PLACE_CONTROLLER = new AsynchronousPlaceController(EVENT_BUS);
	private static final UserManager USER_MANAGER = new UserManagerImpl();
	private static final NavigationManager NAVIGATION_MANAGER = new NavigationManager(PLACE_CONTROLLER);
	private static LanguageSwitchingView languageSwitchingView = null;

	public CommonClientFactoryImpl() {
		EVENT_BUS.addHandler(PlaceNavigationEvent.TYPE, NAVIGATION_MANAGER);
	}

	@Override
	public EventBus getEventBus() {
		return EVENT_BUS;
	}

	@Override
	public PlaceController getPlaceController() {
		return PLACE_CONTROLLER;
	}

	@Override
	public NavigationManager getNavigationManager() {
		return NAVIGATION_MANAGER;
	}

	@Override
	public UserManager getUserManager() {
		return USER_MANAGER;
	}

	@Override
	public abstract LoginPlace getLoginPlace();

	/**
	 * Dans l'implementation commune, la langue par défaut est l'anglais.
	 */
	@Override
	public String getDefaultLanguage() {
		return LocaleUtil.ENGLISH;
	}

	/**
	 * Dans l'implementation commune, l'unique langue supportée est l'anglais.
	 */
	@Override
	public List<String> getLanguages() {
		ArrayList<String> languages = new ArrayList<String>();
		languages.add(LocaleUtil.ENGLISH);
		return languages;
	}

}
