package fr.sedoo.commons.client.widget.celllist;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.cellview.client.CellList;
import com.google.gwt.user.cellview.client.CellList.Resources;

public interface CellListResources extends Resources {

        public CellListResources INSTANCE =
                GWT.create(CellListResources.class);

        /**
         * The styles used in this widget.
         */
        @Source("cellList.css")
        @Override
        CellList.Style cellListStyle();
}