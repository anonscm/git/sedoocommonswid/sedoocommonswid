package fr.sedoo.commons.client.mvp.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

public class AboutPlace extends Place
{
	public static AboutPlace instance;
	
	public AboutPlace()
	{
	}

	public static class Tokenizer implements PlaceTokenizer<AboutPlace>
	{
		
		public AboutPlace getPlace(String token) {
			if (instance == null)
			{
				instance = new AboutPlace();
			}
			return instance;
		}

		public String getToken(AboutPlace place) {
			return "";
		}

		
	}
}