package fr.sedoo.commons.client.crud.widget;

import fr.sedoo.commons.shared.domain.HasIdentifier;

public abstract class CrudConfirmCallBack {

	public abstract void confirm(boolean choice, HasIdentifier hasIdentifier);

}
