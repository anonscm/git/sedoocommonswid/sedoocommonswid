package fr.sedoo.commons.client.logfile.ui.table;

import com.google.gwt.cell.client.Cell;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.ui.Image;

import fr.sedoo.commons.client.image.CommonBundle;
import fr.sedoo.commons.client.logfile.bundle.LogFileMessages;
import fr.sedoo.commons.client.logfile.ui.LogFileDisplayView.Presenter;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.widget.table.AbstractListTable;
import fr.sedoo.commons.client.widget.table.ImagedActionCell;
import fr.sedoo.commons.client.widget.table.ImagedActionCell.Delegate;
import fr.sedoo.commons.shared.domain.HasIdentifier;
import fr.sedoo.commons.shared.domain.LogFile;

public class LogFileTable extends AbstractListTable {

	protected Column<HasIdentifier, String> downloadColumn;
	private Presenter presenter;
	private Delegate<HasIdentifier> downloadAction;

	@Override
	public void presenterEdit(HasIdentifier hasIdentifier) {
		// Nothing is done this way
	}

	protected void initColumns() {

		TextColumn<HasIdentifier> nameColumn = new TextColumn<HasIdentifier>() {
			@Override
			public String getValue(HasIdentifier aux) {
				return ((LogFile) aux).getName();
			}
		};
		itemTable.addColumn(nameColumn, LogFileMessages.INSTANCE.fileName());
		itemTable.setColumnWidth(nameColumn, 100.0, Unit.PX);

		Cell<HasIdentifier> downloadActionCell = new ImagedActionCell<HasIdentifier>(getDownloadAction(), new Image(CommonBundle.INSTANCE.download()), CommonMessages.INSTANCE.download());
		downloadColumn = new Column(downloadActionCell) {

			@Override
			public HasIdentifier getValue(Object object) {
				return (HasIdentifier) object;
			}
		};

		itemTable.addColumn(downloadColumn);
		itemTable.setColumnWidth(downloadColumn, 30.0, Unit.PX);

	}

	private Delegate<HasIdentifier> getDownloadAction() {
		if (downloadAction == null) {
			downloadAction = new Delegate<HasIdentifier>() {

				@Override
				public void execute(final HasIdentifier hasId) {
					presenter.downloadLog(hasId.getIdentifier());
				}

			};
		}
		return downloadAction;
	}

	@Override
	public void presenterDelete(HasIdentifier hasId) {
	}

	@Override
	public void addItem() {

	}

	@Override
	public String getAddItemText() {
		return "";
	}

	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
	}

}
