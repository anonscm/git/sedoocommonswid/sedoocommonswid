package fr.sedoo.commons.client.news.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import fr.sedoo.commons.client.util.ServiceException;
import fr.sedoo.commons.shared.domain.message.MessageDTO;
import fr.sedoo.commons.shared.domain.message.MessageTableItem;
import fr.sedoo.commons.shared.domain.message.TitledMessage;

@RemoteServiceRelativePath("messages")
public interface MessageService extends RemoteService {
	ArrayList<TitledMessage> getAllTitledMessage(String preferredLanguage, List<String> alternateLanguages) throws ServiceException;

	ArrayList<TitledMessage> getLastTitledMessage(String preferredLanguage, List<String> alternateLanguages) throws ServiceException;

	ArrayList<TitledMessage> getTitleMessagesByPage(Integer pageNumber, Integer pageSize, String preferredLanguage, List<String> alternateLanguages) throws ServiceException;

	void deleteMessageByUuid(String uuid) throws ServiceException;

	MessageDTO getMessageContentByUuid(String uuid) throws ServiceException;

	Integer getMessageHits() throws ServiceException;

	String saveMessage(String uuid, String author, HashMap<String, TitledMessage> content, String externaImagelUrl, String illustrationImageType, String fileName, boolean isTmpFile)
			throws ServiceException;

	TitledMessage getConsultMessageContentByUuid(String uuid, String preferredLanguage, List<String> alternateLanguages) throws ServiceException;

	ArrayList<MessageTableItem> getTitles(String preferredLanguage, List<String> alternateLanguages) throws ServiceException;

	ArrayList<TitledMessage> getCarouselNews(String preferredLanguage, List<String> alternateLanguages) throws ServiceException;

}
