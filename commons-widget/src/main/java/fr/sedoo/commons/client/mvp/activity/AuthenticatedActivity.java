package fr.sedoo.commons.client.mvp.activity;

import com.google.gwt.place.shared.Place;

import fr.sedoo.commons.client.event.ActivityStartEvent;
import fr.sedoo.commons.client.event.PlaceNavigationEvent;
import fr.sedoo.commons.client.mvp.AuthenticatedClientFactory;
import fr.sedoo.commons.client.mvp.place.LoginPlace;

public abstract class AuthenticatedActivity extends DefaultActivity {

	private Place wishedPlace;

	public AuthenticatedActivity(AuthenticatedClientFactory clientFactory, Place wishedPlace) {
		super(clientFactory);
		this.wishedPlace = wishedPlace;
	}

	protected abstract boolean isValidUser();

	protected boolean isLoggedUser() {
		if (((AuthenticatedClientFactory) clientFactory).getUserManager().getUser() == null) {
			return false;
		} else {
			return true;
		}
	}

	protected void goToLoginPlace() {
		LoginPlace loginPlace = ((AuthenticatedClientFactory) clientFactory).getLoginPlace();
		loginPlace.setWishedPlace(wishedPlace);
		clientFactory.getEventBus().fireEvent(new PlaceNavigationEvent(loginPlace));
	}

	protected void sendActivityStartEvent() {
		clientFactory.getEventBus().fireEvent(new ActivityStartEvent(ActivityStartEvent.AUTHENTICATED_ACTIVITY));
	}
}
