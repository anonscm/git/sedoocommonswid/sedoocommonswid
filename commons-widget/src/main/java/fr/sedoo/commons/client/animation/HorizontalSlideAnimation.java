package fr.sedoo.commons.client.animation;

import com.google.gwt.animation.client.Animation;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.Widget;

public class HorizontalSlideAnimation extends Animation
{
    private final Widget widget;
    private boolean         opening;

    public HorizontalSlideAnimation(Widget widget)
    {
        this.widget = widget;
    }

    @Override
    protected void onComplete()
    {
        if(! opening)
            this.widget.setVisible(false);

        DOM.setStyleAttribute(this.widget.getElement(), "width", "auto");
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        opening = ! this.widget.isVisible();

        if(opening)
        {
            DOM.setStyleAttribute(this.widget.getElement(), "width", "0px");
            this.widget.setVisible(true);
        }

    }

    @Override
    protected void onUpdate(double progress)
    {
        int scrollWidth = DOM.getElementPropertyInt(this.widget.getElement(), "scrollWidth");
        int width = (int) (progress * scrollWidth);
        if( !opening )
        {
        	width = scrollWidth - width;
        }
        width = Math.max(width, 1);
        DOM.setStyleAttribute(this.widget.getElement(), "width", width + "px");
    }
}
