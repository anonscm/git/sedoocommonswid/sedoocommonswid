package fr.sedoo.commons.client.widget.table;


import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;


public interface TableBundle extends ClientBundle {
	    
	@Source("add.png")
	ImageResource add();
	
	@Source("view.gif")
	ImageResource view();
	
	@Source("delete.png")
	ImageResource delete();

	@Source("delete-locked.png")
	ImageResource lockedDelete();

	@Source("edit.png")
	ImageResource edit();

	@Source("edit-locked.png")
	ImageResource lockedEdit();

	    public static final TableBundle INSTANCE = GWT.create(TableBundle.class);
		
	  }