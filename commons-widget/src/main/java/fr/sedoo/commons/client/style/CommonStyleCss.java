package fr.sedoo.commons.client.style;

import com.google.gwt.resources.client.CssResource;

public interface CommonStyleCss extends CssResource {

	@ClassName("clickable")
	String clickable();

	@ClassName("italic")
	String italic();

	@ClassName("rightMargin3px")
	String rightMargin3px();

	@ClassName("leftMargin3px")
	String leftMargin3px();

	@ClassName("rightMargin5px")
	String rightMargin5px();

	@ClassName("leftMargin5px")
	String leftMargin5px();

	@ClassName("inlineDisplay")
	String inlineDisplay();

}
