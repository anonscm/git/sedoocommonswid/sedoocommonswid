package fr.sedoo.commons.client.util;

import org.junit.Assert;
import org.junit.Test;

public class DoubleUtilTest {

	@Test
	public void testCheckDouble() {
		Assert.assertTrue(DoubleUtil.checkDouble("12.5"));
		Assert.assertTrue(DoubleUtil.checkDouble("12555"));
		Assert.assertTrue(DoubleUtil.checkDouble("0.555555"));
		Assert.assertTrue(DoubleUtil.checkDouble("-512.5"));
		Assert.assertFalse(DoubleUtil.checkDouble(".5"));
		Assert.assertFalse(DoubleUtil.checkDouble("12,5"));
		Assert.assertFalse(DoubleUtil.checkDouble("12A5"));
	}

}
