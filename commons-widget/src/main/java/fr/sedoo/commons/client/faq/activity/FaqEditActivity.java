package fr.sedoo.commons.client.faq.activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

import fr.sedoo.commons.client.event.ActionEventConstants;
import fr.sedoo.commons.client.event.ActionStartEvent;
import fr.sedoo.commons.client.event.ActivityStartEvent;
import fr.sedoo.commons.client.event.BackEvent;
import fr.sedoo.commons.client.event.NotificationEvent;
import fr.sedoo.commons.client.event.PlaceNavigationEvent;
import fr.sedoo.commons.client.faq.mvp.FaqClientFactory;
import fr.sedoo.commons.client.faq.place.FaqEditPlace;
import fr.sedoo.commons.client.faq.place.FaqManagePlace;
import fr.sedoo.commons.client.faq.service.FaqService;
import fr.sedoo.commons.client.faq.service.FaqServiceAsync;
import fr.sedoo.commons.client.faq.ui.FaqEditView;
import fr.sedoo.commons.client.message.CommonMessages;
import fr.sedoo.commons.client.mvp.AuthenticatedClientFactory;
import fr.sedoo.commons.client.mvp.activity.AdministrationActivity;
import fr.sedoo.commons.client.service.DefaultAbstractCallBack;
import fr.sedoo.commons.client.storedfile.service.StoredImagesService;
import fr.sedoo.commons.client.storedfile.service.StoredImagesServiceAsync;
import fr.sedoo.commons.client.util.StringUtil;
import fr.sedoo.commons.client.util.UuidUtil;
import fr.sedoo.commons.client.widget.uploader.FileUploadUtil;
import fr.sedoo.commons.shared.domain.faq.QuestionAndAnswer;
import fr.sedoo.commons.shared.domain.misc.ImageUrl;

public abstract class FaqEditActivity extends AdministrationActivity implements FaqEditView.Presenter {

	private FaqEditView editView;
	private String uuid = "";
	private FaqEditPlace place;
	private Integer order;

	private boolean tmp = true;

	private static final FaqServiceAsync FAQ_SERVICE = GWT.create(FaqService.class);
	private static final StoredImagesServiceAsync STORED_IMAGE_SERVICE = GWT.create(StoredImagesService.class);

	public FaqEditActivity(FaqEditPlace place, AuthenticatedClientFactory clientFactory) {
		super(clientFactory, place);
		this.place = place;
		uuid = place.getUuid();
		order = place.getOrder();
	}

	/**
	 * Invoked by the ActivityManager to start a new Activity
	 */
	@Override
	public void start(final AcceptsOneWidget containerWidget, EventBus eventBus) {
		if (isValidUser() == false) {
			goToLoginPlace();
			return;
		}
		editView = ((FaqClientFactory) clientFactory).getFaqEditView();
		editView.activityStart();
		editView.setPresenter(this);
		editView.reset();
		eventBus.fireEvent(new ActivityStartEvent(this));
		if (StringUtil.isEmpty(uuid)) {
			// Si ordre à 0, on revient à la vue de management
			if (order == 0) {
				FaqManagePlace place = new FaqManagePlace();
				clientFactory.getEventBus().fireEvent(new PlaceNavigationEvent(place));
				return;
			}
			// Creation
			tmp = true;
			uuid = UuidUtil.uuid();
			editView.reset();
			editView.setUuid(uuid);
			editView.setContent(new HashMap<String, QuestionAndAnswer>(), clientFactory.getLanguages(), order);
			containerWidget.setWidget(editView.asWidget());
		} else {
			tmp = false;
			editView.reset();
			editView.setUuid(uuid);
			ActionStartEvent e = new ActionStartEvent(CommonMessages.INSTANCE.loading(), ActionEventConstants.BASIC_LOADING_EVENT, true);
			eventBus.fireEvent(e);

			FAQ_SERVICE.getFaqContentByUuid(uuid, new DefaultAbstractCallBack<HashMap<String, QuestionAndAnswer>>(e, eventBus) {
				@Override
				public void onSuccess(HashMap<String, QuestionAndAnswer> result) {
					super.onSuccess(result);
					editView.setContent(result, clientFactory.getLanguages(), result.values().iterator().next().getOrder());
					containerWidget.setWidget(editView.asWidget());

					EventBus eventBus = clientFactory.getEventBus();
					ActionStartEvent e = new ActionStartEvent(CommonMessages.INSTANCE.loading(), ActionEventConstants.BASIC_LOADING_EVENT, true);
					eventBus.fireEvent(e);

					STORED_IMAGE_SERVICE.getStoredImagesByContainerId(uuid, new DefaultAbstractCallBack<ArrayList<ImageUrl>>(e, eventBus) {

						@Override
						public void onSuccess(ArrayList<ImageUrl> result) {
							super.onSuccess(result);
							Iterator<ImageUrl> imageIterator = result.iterator();
							while (imageIterator.hasNext()) {
								ImageUrl imageUrl = (ImageUrl) imageIterator.next();
								imageUrl.setLink(FileUploadUtil.getImageUrlFromId(imageUrl.getId()));

							}
							editView.setImages(result);
						}

					});
				}
			});

		}
	}

	@Override
	public void save(final HashMap<String, QuestionAndAnswer> content) {
		EventBus eventBus = clientFactory.getEventBus();
		ActionStartEvent e = new ActionStartEvent(CommonMessages.INSTANCE.saving(), ActionEventConstants.BASIC_SAVING_EVENT, true);
		eventBus.fireEvent(e);

		FAQ_SERVICE.saveFaq(uuid, content, new DefaultAbstractCallBack<String>(e, eventBus) {
			@Override
			public void onSuccess(String uuid) {
				super.onSuccess(uuid);
				setUuid(uuid);
				clientFactory.getEventBus().fireEvent(new NotificationEvent(CommonMessages.INSTANCE.savedElement()));
				tmp = false;
			}
		});
	}

	@Override
	public void back() {
		clientFactory.getEventBus().fireEvent(new BackEvent());
	}

	public FaqEditPlace getPlace() {
		return place;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	@Override
	public void deleteImageById(final String id) {
		EventBus eventBus = clientFactory.getEventBus();
		ActionStartEvent e = new ActionStartEvent(CommonMessages.INSTANCE.deleting(), ActionEventConstants.BASIC_DELETING_EVENT, true);
		eventBus.fireEvent(e);
		STORED_IMAGE_SERVICE.deleteStoredImageById(new Long(id), new DefaultAbstractCallBack<Void>(e, eventBus) {

			@Override
			public void onSuccess(Void result) {
				super.onSuccess(result);
				clientFactory.getEventBus().fireEvent(new NotificationEvent(CommonMessages.INSTANCE.deletedElement()));
				editView.broadcastDeletion(id);
			}

		});
	}

	@Override
	public void onStop() {
		if (tmp == true) {
			// Si des images on été chargées elle sont supprimées car l'élément
			// n'a pas été sauvé
			EventBus eventBus = clientFactory.getEventBus();
			ActionStartEvent e = new ActionStartEvent(CommonMessages.INSTANCE.deleting(), ActionEventConstants.BASIC_DELETING_EVENT, true);
			eventBus.fireEvent(e);
			STORED_IMAGE_SERVICE.deleteStoredImageByContainerId(uuid, new DefaultAbstractCallBack<Void>(e, eventBus) {

				@Override
				public void onSuccess(Void result) {
					super.onSuccess(result);
				}

			});
		}
		editView.activityStop();
		super.onStop();
	}
}