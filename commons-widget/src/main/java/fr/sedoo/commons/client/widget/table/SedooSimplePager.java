package fr.sedoo.commons.client.widget.table;

import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.cellview.client.SimplePager;
import com.google.gwt.view.client.HasRows;
import com.google.gwt.view.client.Range;

import fr.sedoo.commons.client.message.CommonMessages;

/**
 * Pager permettant de faire en sorte que la dernière page n'affiche pas PAGE_SIZE éléments.
 * @author rolfo
 *
 */
public class SedooSimplePager extends SimplePager {

	private int PAGE_SIZE;

	public SedooSimplePager(int pageSize) {
		super(TextLocation.CENTER, false, true);
		this.PAGE_SIZE = pageSize;
	}

	@Override
	public void setPageStart(int index) {
		if (this.getDisplay() != null) {
			Range range = this.getDisplay().getVisibleRange();
			int pageSize = range.getLength();

			index = adjustIndex(index);
			if (index != range.getStart()) {
				this.getDisplay().setVisibleRange(index, pageSize);
			}
		}
	}

	private int adjustIndex(int index) {
		
		index = Math.max(0, index);
		
		while (index % PAGE_SIZE != 0) {
			index--;
		}

		return index;
	}

	protected String createText() {
		// Default text is 1 based.
		NumberFormat formatter = NumberFormat.getFormat("#,###");
		HasRows display = getDisplay();
		Range range = display.getVisibleRange();
		int pageStart = range.getStart() + 1;
		int pageSize = range.getLength();
		int dataSize = display.getRowCount();
		int endIndex = Math.min(dataSize, pageStart + pageSize - 1);
		endIndex = Math.max(pageStart, endIndex);
		boolean exact = display.isRowCountExact();
		return formatter.format(pageStart)
				+ "-"
				+ formatter.format(endIndex)
				+ (exact ? " " + CommonMessages.INSTANCE.pagerOf() + " " : " "
						+ CommonMessages.INSTANCE.pagerOfOver() + " ")
				+ formatter.format(dataSize);
	}
}
