package fr.sedoo.commons.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface ActionStartEventHandler extends EventHandler {
    void onNotification(ActionStartEvent event);
}
