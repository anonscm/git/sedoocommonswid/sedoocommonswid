package fr.sedoo.commons.client.news.widget;

import com.google.gwt.user.client.ui.IsWidget;

import fr.sedoo.commons.client.back.BackPresenter;
import fr.sedoo.commons.shared.domain.New;

public interface NewDisplayView extends IsWidget{
	
	void display(New item);
	void reset();
	void setBackPresenter(BackPresenter presenter);


}
