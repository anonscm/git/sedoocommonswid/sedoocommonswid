package fr.sedoo.commons.client.widget.editing.api;

import java.util.List;

import fr.sedoo.commons.client.widget.editing.ValidationAlert;

public interface Edited {

	List<ValidationAlert> validate();
}
