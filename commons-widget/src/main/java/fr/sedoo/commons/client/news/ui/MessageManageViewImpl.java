package fr.sedoo.commons.client.news.ui;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

import fr.sedoo.commons.client.component.AbstractView;
import fr.sedoo.commons.client.news.ui.table.MessageTable;
import fr.sedoo.commons.shared.domain.HasIdentifier;
import fr.sedoo.commons.shared.domain.message.MessageTableItem;

public class MessageManageViewImpl extends AbstractView implements MessageManageView
{

	private static MessageManageViewImplUiBinder uiBinder = GWT.create(MessageManageViewImplUiBinder.class);

	interface MessageManageViewImplUiBinder extends UiBinder<Widget, MessageManageViewImpl>
	{
	}

	@UiField
	MessageTable messageTable;
	
	@UiField
	Label title;
	
	@Override
	public void setPresenter(Presenter presenter) {
		messageTable.setPresenter(presenter);
	}
	
	public MessageManageViewImpl() {
		super();
		initWidget(uiBinder.createAndBindUi(this));
		applyCommonStyle();
		messageTable.setAddButtonEnabled(true);
		messageTable.init(new ArrayList<HasIdentifier>());
	}

	@Override
	public void setMessages(ArrayList<MessageTableItem> messages)
	{
		messageTable.init(messages);
	}

	@Override
	public void broadcastDeletion(String uuid) {
		messageTable.broadcastDeletion(uuid);
		
	}

	@Override
	public void setTitleVisible(boolean value) {
		title.setVisible(value);
		
	}
	
}
