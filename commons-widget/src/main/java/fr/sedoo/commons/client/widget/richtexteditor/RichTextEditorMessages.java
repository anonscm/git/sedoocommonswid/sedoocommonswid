package fr.sedoo.commons.client.widget.richtexteditor;

import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.LocalizableResource.DefaultLocale;
import com.google.gwt.i18n.client.LocalizableResource.Generate;
import com.google.gwt.i18n.client.Messages;

@Generate(
format = { "com.google.gwt.i18n.rebind.format.PropertiesFormat" }, 
fileName = "Message", 
locales = {"en, fr"})
@DefaultLocale("en")
public interface RichTextEditorMessages extends Messages {
	
	public static final RichTextEditorMessages INSTANCE = GWT.create(RichTextEditorMessages.class);
	
	@Key("bold")
	String bold();

	@Key("createLink")
	String createLink();

	@Key("hr")
	String hr();

	@Key("indent")
	String indent();

	@Key("insertImage")
	String insertImage();

	@Key("italic")
	String italic();

	@Key("justifyCenter")
	String justifyCenter();

	@Key("justifyLeft")
	String justifyLeft();

	@Key("justifyRight")
	String justifyRight();

	@Key("ol")
	String ol();

	@Key("outdent")
	String outdent();

	@Key("removeFormat")
	String removeFormat();

	@Key("removeLink")
	String removeLink();

	@Key("strikeThrough")
	String strikeThrough();

	@Key("subscript")
	String subscript();

	@Key("superscript")
	String superscript();

	@Key("ul")
	String ul();

	@Key("underline")
	String underline();

}
