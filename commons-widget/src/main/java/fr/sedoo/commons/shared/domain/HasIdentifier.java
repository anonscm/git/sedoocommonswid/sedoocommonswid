package fr.sedoo.commons.shared.domain;

import java.util.List;

import fr.sedoo.commons.client.widget.editing.ValidationAlert;

public interface HasIdentifier {
        public String getIdentifier();

		public List<ValidationAlert> validate();
}
