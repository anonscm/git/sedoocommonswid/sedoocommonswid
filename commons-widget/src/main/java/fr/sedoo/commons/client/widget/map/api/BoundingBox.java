package fr.sedoo.commons.client.widget.map.api;

import fr.sedoo.commons.client.widget.editing.api.Edited;

public interface BoundingBox extends Edited{
	
	String getNorthBoundLatitude();
	String getSouthBoundLatitude();
	String getEastBoundLongitude(); 
	String getWestBoundLongitude();
	void setNorthBoundLatitude(String northBoundLatitude); 
	void setSouthBoundLatitude(String southBoundLatitude); 
	void setEastBoundLongitude(String eastBoundLongitude); 
	void setWestBoundLongitude(String westBoundLongitude); 

}
