package fr.sedoo.commons.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface BreadCrumbChangeEventHandler extends EventHandler {
    void onNotification(BreadCrumbChangeEvent event);
}
